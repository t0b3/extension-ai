<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Text

Text is stored within an `AI11_TextDocument`, i.e. 

```
%AI11_BeginTextDocument
/AI11TextDocument : /ASCII85Decode ,
%[... blob ...]~>
7893 7770 /RulerOrigin ,
;
/AI11UndoFreeTextDocument : /ASCII85Decode ,
%[... blob ...]~>
;
%AI11_EndTextDocument
```

The individiual lines of the blob start with `%`. After Adobe-ASCII-85-decoding the blob, 
the text document is described by the following grammar 
(which is in this form used as unit test for the actual parser):

```
textdocument := object
object       := (flag + value)*
flag         := r"/(\d\d?|[\w\d]+)"
value        := subobject | list | number | 
                text | boolean | datatype 
subobject    := "<<" + object + ">>"
list         := "[" + value* + "]"
number       := int | float
text         := Anything between "(" and ")", possibly multiline; may include
                out-of-charset characters, non-quoting parentheses are 
                escaped with "\". 
                Uids are formatted in standard hex 8-4-4-4-12 representation.
                Other strings start are encoded in UTF-16-BE and start with,
                '\FE\FF' (Big endian Byte-order marker)  
boolean      := "true" | "false"
datatype     := r"/[\w\d]+"
```

which gives rise to a deeply nested, hierarchical data structure that contains 
the actual text information, described below.

(This is very similar to PostScript's dictionary syntax, described on Page 33 of the
PostScript Language Reference Manual, Third Edition.)

The `UndoFreeTextDocument` does not appear relevant for parsing the text.

In the saved files, the keys are usually only numeric values ("/0", "/99") etc,
whose meaning has been reconstructed below. 
However, querying the clipboard for a format called "Adobe Text Engine 2.0"
reveals the underlying names of most of those keys. They are specified in 
brackets below.

## Text instance in the main document

### Simple text

```
/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
0 /StoryIndex ,
2 /TextAntialiasing ,
;
```

`/StoryIndex` references the `Story`. `/FreeUndo` appears to be always 0. 

The actual text position is stored in the `/1 /1 /1 /0 /0` list, see below. `/FrameIndex` is only relevant for Area Type.

### Area Type and Text-on-path

The shape-inside is stored in `/ConfiningPath`. Click-dragging creates a non-live rectangle, 
however arbitrary shapes, including live shapes, may be used as confing objects.

```
/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
1 /StoryIndex ,
/Art :
X=
1 Ap
4 As
<path object>
[<live shape definition>]
X+
; /ConfiningPath ,
2 /TextAntialiasing ,
;
```

If the Area Type consists of multiple connected areas, this section may be repeated (with incremented `/FrameIndex`).

Text-on-path has exactly the same syntax, the distinction between Text on Path and Area Type 
happens in the `/Frame` object (whether `/2 /11` is defined or not).

## Structure of the `/AI11TextDocument`

```
/98: ?
/0 [/DocumentResources]: Dict, definitions
  /1 [/FontSet]: Dict, contains a list of used fonts
    /0 [/Resources]: List[FontDefinition]
    /1 [/DisplayList]: List - could be the index of the default font?
      /0 [Resource]: Index of the font in the FontDefinitions
  /2 [/MojiKumiCodeToClassSet]: Dict
  /3 [/MojiKumiTableSet]: Dict
  /4 [/KinsokuSet]: Dict
  /9 [/ListStyleSet]: Dict, enumeration and itemization type definitions
    /0 [/Resources]: List[ListingStyle]
    /1 [/DisplayList]: List, one entry for each default enumeration style
      /0 [Resource] : Index of the enumeration style in the /Resources
  /5 [/StyleSheetSet]: Dict, Character Styles defined in the document
    /0 [/Resources] List
      /0 [/Resource] Object
        /97 [/UUID]: UUID
        /0 [/Name]: Name in the Window -> Type -> Character Style dialog
        /5: int, 0 (?)
        /6 [/Features]: CharacterStyle
    /1 [/DisplayList]: List
      /0 [/Resource]: Index of the style in the StyleSheetSet
  /6 [/ParagraphSheetSet]: Dict, Paragraph Styles defined in the document (analogous to /5)
  /8 [/TextFrameSet]: Dict
    /0 [/Resources]: List
      /0 [/Resource]: Frame
/1 [/DocumentObjects]: Dict
  /0 [/DocumentSettings]: Dict
    /0 [/HiddenGlyphFont]: Dict
      /0 [/AlternateGlyphFont]: int
      /1 [/WhitespaceCharacterMapping] : List
        /0 [/WhitespaceCharacter]
        /1 [/AlternateCharacter]
    /1 [/NormalStyleSheet] : int
    /2 [/NormalParagraphSheet]: int
    /3 [/SuperscriptSize]: float
    /4 [/SuperscriptPosition]: float
    /5 [/SubscriptSize]: float
    /6 [/SubscriptPosition]: float
    /7 [/SmallCapSize]: float
    /8 [/UseSmartQuotes]: bool
    /9 [/SmartQuoteSets]: List
      /0 [/Language]: int
      /1 [/OpenDoubleQuote]: str
      /2 [/CloseDoubleQuote]: str
      /3 [/OpenSingleQuote]: str
      /4 [/CloseSingleQuote]:str
    /11 [/GreekingSize] : int
    /15 [/LinguisticSettings] : Dict
      /0 [/PreferredProvider] : string ("Hunspell")
    /16 [/UseSmartLists] : bool
    /17 [/DefaultStoryDir] : int
      

  /1 [/TextObjects]: List[Story]
  /2 [/OriginalNormalStyleFeatures]: CharacterStyle
  /3 [/OriginalNormalParagraphFeatures]: ParagraphStyle
```

The data types are further described below. In particular, the following lists are relevant for later index-based lookups:

- `/0 /1 /0`: contains the `FontDefinition`s
- `/0 /5 /0`: contains the `CharacterStyle`s
- `/0 /6 /0`: contains the `ParagraphStyle`s
- `/0 /8 /0`: contains the `Frame`s
- `/0 /9 /0`: contains the `ListingStyle`s

### `Story`

The format of the Story is slightly different from the clipboard, so not all entries could be identified.

```
/0 [/Model]: content
  /0 [/Text]: actual text, characters are at least two bytes long (i.e. UTF-16). 
      Emojis?
      Paragraphs are created with \r, newlines with \u03.
  /5 [/ParagraphRun]: Dict, contains overrides of the paragraph styles
      If multiple styles are used in a text element, their scope is given in
      "number of affected characters".
    /0 [/RunArray]: List
      /0 [/RunData]: Dict 
        /0 [/ParagraphSheet]: int (index in the default paragraph styles) or dict, in which case:
          /97 [/UUID]
          /0 [/Name]: empty
          /5 [/Features]: ParagraphStyle, Overrides of the paragraph style 
          /6 [/Parent]
      /1 [/Length]: Number of affected characters
  /6 [/StyleRun]: Dict, contains overrides of the character styles.
      If multiple styles are used in a text element, their scope is given in
      "number of affected characters".
    /0 [/RunArray]: List
      /0 [/RunData]: Dict
        /0 [/StyleSheet]: int (index in the default character styles) or dict, in which case:
          /97 [/UUID]: UUID
          /0 [/Name]: [string]
          /5 [/Parent]: [int]
          /6 [/Features]: CharacterStyle, Overrides of the character style
      /1 [/Length]: Number of affected characters
  /? [/KernRun]
  /? [/AlternateGlyphRun]
  /10 [/StorySheet]: Dict (small), probably some settings
    /0 [/AntiAlias]
    /2 [/UseFractionalGlyphWidths]
  /? [/HyperlinkRun]
/1 [/View]: layout information
  /0 [/Frames]: List of Dicts
    /0 [/Resource]: index of Frame. Note that flowtexts can have multiple defined.
        This appears to be duplicated in the main file ("/FrameIndex")
  /? [/RenderedData]: Dict
  /2 [/Strikes]: List[Strikes] 
    /99 [/StreamTag]: string. Stream tags in here:
      /PC: /PathSelectGroupCharacter
      /F: /FrameStrike
      /R: /RowColStrike
      /L: /LineStrike
      /S: /Segment
      /G: /GlyphStrike
    /0 [/Transform] : Dict
    /6 [/Children]: List[Strikes]
```

### `CharacterStyle`

Settings accessible only through the Window -> Type -> Character Style dialog are marked with `[*]`.

Undefined attributes are inherited from the [Normal Character Style]. 

```
/0 [/Font]: Font [index in the font list]
/1 [/FontSize]: Font size [pt]
/2 [/FauxBold]: [bool]
/3 [/FauxItalic]: [bool]
/4 [/AutoLeading]: [bool]
/5 [/Leading]: Line spacing [pt]
/6 [/HorizontalScale]: Horizontal Scaling [factor]
/7 [/VerticalScale]: Vertical Scaling [factor]
/8 [/Tracking]: Tracking [percent]
/9 [/BaselineShift]: Baseline shift [pt]
/10 [/CharacterRotation]: Character rotation [degrees, counterclockwise]
/11 [/AutoKern]: 2: Optical Kerning 3: Metrics - Roman Kerning
/12 [/FontCaps]: 1: Small caps, 2: All caps
/13 [/FontBaseline]: 0: normal, 1: Superscript, 2: Subscript
/14 [/FontOTPosition]: OpenType Features: Position. 1: Superscript/Superior, 2: Subscript/Inferior,
            3: Numerator, 4: Denominator
/15 [/StrikethroughPosition]: 0: no strikethrough, 1: Strikethrough
/16 [/UnderlinePosition]: 0: no underline, 1: Underline
/17 [/UnderlineOffset]: [float]
/18 [/Ligatures]: OpenType Features: Standard Ligatures [bool]
/19 [/DiscretionaryLigatures]: OpenType Features: Discretionary Ligures [bool]
/20 [/ContextualLigatures]: OpenType Features: Contextual Alternates [bool]
/21 [/AlternateLigatures]: [bool]
/22 [/OldStyle]: [bool]
/23 [/Fractions]: OpenType Features: Fractions [bool]
/24 [/Ordinals]: OpenType Features: Ordinals [bool]
/25 [/Swash]: OpenType Features: Swash [bool]
/26 [/Titling]: OpenType Features: Titling Alternates [bool]
/27 [/ConnectionForms]: [bool]
/28 [/StylisticAlternates]: OpenType Features: Stylistic Alternates [bool]
/29 [/Ornaments]: [bool]
/30 [/FigureStyle]: OpenType Features: Figure. 0: Default Figure, 1: Tabular Lining, 
            2: Proportional Old Style, 3: Proportional Lining, 4: Tabular Old Style
/31 [/ProportionalMetrics]: [bool]
/32 [/Kana]: [bool]
/33 [/Italics]: [bool]
/34 [/Ruby]: [bool]
/35 [/BaselineDirection]: [*] 1: Standard Vertical Roman Alignment 
/33 [/Tsume]: [float]
/34 [/StyleRunAlignment]: [int]
/38 [/Language]: "Language" [enum]
/39 [/JapaneseAlternateFeature]: [int]
/40 [/EnableWariChu]: [bool]
/41 [/WariChuLineCount]: [int]
/42 [/WariChuLineGap]: [int]
/43 [/WariChuSubLineAmount]: Dict
  /0 [/WariChuSubLineScale] : float
/44 [/WariChuWidowAmount]: [int]
/45 [/WariChuOrphanAmount]: [int]
/46 [/WariChuJustification]: [int]
/47 [/TCYUpDownAdjustment]: [int]
/48 [/TCYLeftRightAdjustment]: [int]
/49 [/LeftAki]: [float]
/50 [/RightAki]: [float]
/51 [/JiDori]: [int]
/52 [/NoBreak]: [bool]
/53 [/FillColor]: Fill Color: Object of type /CAITextPaint
  /99 [/StreamTag]: /SimplePaint
  /0 [/Color]: Object
    /1 [/Values]: List of components. 
       RGB documents: [1 R G B, values between 0 and 1]
       CMYK documents: [1 C M Y K, values between 0 and 1]
    /2 [/Type]: int
/54 [/StrokeColor]: Stroke Color: Object of type /CAITextPaint
/55 [/Blend]: Opacity: Object of type /CAITextBlender
  /0 [/Mode]: Blend mode. 1: Multiply, 2: Screen, 3: Overlay, 
            4: Soft Light, 5: Hard Light, 6: Color Dodge, 
            7: Color Burn, 8: Darken, 9: Lighten,
            10: Difference, 11: Exclusion, 12: Hue, 
            13: Saturation, 14: Color, 15: Luminosity
  /1 [/Opacity]: Opacity [0-1]
  /2 [/Isolated]: 1: Isolate Blending enabled
  /3 [/Knockout]: 1: Knockout group enabled, 
                  2: Knockout group "half enabled" (minus in checkbox)
  /4 [/AlphaIsShape]: 1: "Opacity Mask and define Knockout shape"
  /99 [/StreamTag]: "/CAITextBlender"
/56 [/FillFlag]: [bool]
/57 [/StrokeFlag]: [bool]
/58 [/FillFirst]: [bool]
/59 [/FillOverPrint]: [*] Overprint Fill [bool]
/60 [/StrokeOverPrint]: [*] Overprint Stroke [bool]
/61 [/LineCap]: Stroke caps. 0: Butt, 1: Round, 2: Square 
/62 [/LineJoin]: Stroke linejoin. 0: Miter Join, 1: Round join, 2: Bevel join
/63 [/LineWidth]: Stroke width [px]. Default: 1px (usually /54 is not defined)
/64 [/MiterLimit]: Stroke miterlimit [factor of stroke width]
/65 [/LineDashOffset]: [float]
/66 [/LineDashArray]: Stroke dasharray [Array of px values]
/67 [/Type1EncodingNames]: [Array]
/68 [/Kashidas]: [int]
/69 [/DirOverride]: [int]
/70 [/DigitSet]: [int]
/71 [/DiacVPos]: [int]
/72 [/DiacXOffset]: [float]
/73 [/DiacYOffset]: [float]
/74 [/OverlapSwash]: [bool]
/75 [/JustificationAlternates]: [bool]
/76 [/StretchedAlternates]: [bool]
/77 [/FillVisibleFlag]: [bool], true
/78 [/StrokeVisibleFlag]: [bool], true
/79 [/FillBackgroundColor]: Object of type /CAITextPaint
/80 [/FillBackgroundFlag]: [bool]
/81 [/UnderlineStyle]: [int]
/82 [/DashedUnderlineGapLength]: [float]
/83 [/DashedUnderlineDashLength]: [float]
/84 [/SlashedZero]: [bool]
/85 [/StylisticSets]: OpenType Features: Stylistic Sets 
     [binary mask, 1 = Set 1, 2 = Set 2, 4 = Set 3 etc.]
/86 [/CustomFeature]: Dict
  /99 [/StreamTag]: e.g. "/SimpleCustomFeature"
/87 [/MarkYDistFromBaseline]: [float]
/88 [/AutoMydfb]: [bool]
/89 [/RefFontSize]: Font size [pt]
/90 [/FontSizeRefType]: [int]
/92 [/MagicLineGap]: [float]
/93 [/MagicWordGap]: [float]
```

### `FontDefinition`

```
/99 [/StreamTag] Datatype: /CoolTypeFont
/97 [/UUID]: UUID
/0 [/Identifier]: Dict
  /0 [Name]: PostScript font name, encoded in UTF-16. 
      Also includes variant (e.g. "-Italic")
      as well as the weight for variable font: "_535.000wght". 
  /2 [/Type]: Int, ?
  /4 [/MMAxis]: List[int] ?
  /5 [/VersionString]: Font version
```
# `ListingStyle`

All `[pt]` values here are understood to be relative to the font size.
```
/0: Wrapper Object
  /97 [/UUID]: UUID
  /0 [/Name]: Name, either predefined ("kPredefinedEmptyCircleBulletListStyleTag") 
            or random-custom ("Custom_1680435898608793")
  /5 [/LevelStyle]: List[ListingStyleLevel]
    /0 [/IndentUnits]: int, always 1 (?)
    /1 [/TextIndent]: left indent [pt]
    /2 [/LabelIndent]: first line indent [pt], usually negative
    /3 [/LabelAlignment]: 0
    /5 [/SequenceGenerator]: Object
      /99 [/StreamTag]: Type of sequence generator. Types: 
           /AlphabeticSequenceGenerator, 
           /ArabicNumberSequenceGenerator, 
           /BulletSequenceGenerator
           /RomanNumeralSequenceGenerator
      /0 [/Prefix]: Character(s) before enumeration symbol
      /1 [/Postfix]: Character(s) after enumeration symbol
      /2 [/MinDigits]: int
      /3 [/CaseType]: EnumerationChar. Meaning depends on type of sequence generator
        For /AlphabeticSequenceGenerator: 
          Either 64 (upper-case) or 96 (lower case). 
          This is the character code one before the first enumeration 
          symbol (A = 65, a = 97).
        For /RomanNumeralSequenceGenerator:
          Either 64 (upper-case) or 96 (lower case). 
        For /BulletSequenceGenerator: indicates type of bullet used, 
          encountered values:
            ' "' - U+2022 center dot
            '%\aa' - filled square
            '%\e6' - empty center circle
            '%\ab' - empty center square
        For /ArabicNumberSequenceGenerator: 0

    /6 [Font]: int - Reference to the font list
    /7 [/UseOriginalFont]: bool, if true, 6 is not specified
  /6 [/PredefinedTag] : int
```


### `ParagraphStyle`

All `[pt]` values here are understood to be relative to the font size.

```
/0 [/Justification]: Justification. 0: align left, 1: align right, 2: align center
                                    3: justify, last line aligned left
                                    4: justify, last line aligned right
                                    5: justify, last line center-aligned
                                    6: justify all lines
/1 [/FirstLineIndent]: First-line left indent [pt]
/2 [/StartIndent]: Left indent [pt]
/3 [/EndIndent]: Right indent [pt]
/4 [/SpaceBefore]: Space before paragraph [pt]
/5 [/SpaceAfter]: Space after paragraph [pt]
/6 [/DropCaps]: [int]
/7 [/AutoLeading]: Auto Leading [float]
/8 [/LeadingType]: [int]
/9 [/AutoHyphenate]: hypenation enabled [bool]
/10 [/HyphenatedWordSize]: Hypenate words longer than [int] letters
/11 [/PreHyphen]: Hypenate after first [int] letters
/12 [/PostHyphen]: Hypenate before last [int] letters
/13 [/ConsecutiveHyphens]: Hyphen limit: [int] letters
/14 [/Zone]: Hyphenation zone [pt]
/15 [/HyphenateCapitalized]: [bool]
/16 [/HyphenationPreference]: Float between 0: Better Spacing, 0.5: standard, 1: Fewer Hyphens
/17 [/WordSpacing]: Justification (Word spacing): List[float]: [Minimum, Desired, Maximum]  
/18 [/LetterSpacing]: Justification (Letter spacing): List[float]: [Minimum, Desired, Maximum]  
/19 [/GlyphSpacing]: Justification (Glyph spacing): List[float]: [Minimum, Desired, Maximum]  
/20 [/SingleWordJustification]: Single-word justification: 0: align Left, 1: align right, 
                                                           2: align center, 6: Full justify 
/21 [/Hanging]: Roman Hanging Punctuation [bool]
/22 [/AutoTCY]: [int]
/23 [/KeepTogether]: [bool]
/24 [/BurasagariType]: [int]
/25 [/KinsokuOrder]: [int]
/26 [/Kinsoku]: "/nil"
/27 [/KurikaeshiMojiShori]:[bool]
/28 [/MojiKumiTable]: "/nil"
/29 [/EveryLineComposer]: Composer: true: Adobe Every-Line Composer, 
                                    false: Adobe Single Line Composer
/30 [/TabStops]: Tabs
  /0 [/TabStops]: List of tabs
    /0 [/TabAdvance]: Position [pt]
    /1 [/TabType]: Type. 0: left-justified tab, 1: Center-justified tab, 
          2: Right-justified tab, 3: Decimal-justified tab
    /2 [/TabLeader]: Leader [str]
    /3 [/DecimalCharacter]: for decimal-justified tabs: character to align at [str]
/31 [/DefaultTabWidth]: [float]
/32 [/DefaultStyle]: CharacterStyle, Default character style for this paragraph style.
/33 [/ParagraphDirection]: [int]
/34 [/JustificationMethod]: [int]
/35 [/ComposerEngine]: [int]
/36 [/ListStyle]: Bullets/Enumeration [index in the enumeration and itemization type defs], 
                  no enumeration: "/nil"
/37 [/ListTier]: Bullets/Enumeration level [0-based index]
/38 [/ListSkip]: [bool]
/39 [/ListOffset]: [int]
/40 [/KashidaWidth]: [int]
```

### `Frame`

All `[pt]` values here are understood to be relative to the font size.

The "/TextFrameSet" data inside the clipboard is empty, so there are no original names here.

```
/97 [/UUID]: UUID
/0 [/Position]: Position of the start of the baseline of the first character, 
   in canvas coordinates. In CS, the position is only contained in "/Bezier" 
   (coordinate is repeated four times - start position, handle1, handle2, end position) * 
/1 [/Bezier]: Dict
  /0 [/Points]: List of canvas coordinates, only for text-in-shape
/2 [/Data]: Dict
  /0 [/Type]: 2 = Text on path, 1 = Area type, 0 = normal text
  /1 [/LineOrientation]: Orientation. 2: Vertical (top to bottom), also applies to Text on Path
  /2 [/FrameMatrix]: Transform [a b c d e f]. Used for translate, rotate and shear.
      Scale transforms are performed by changing the font size 
      (and scale_x / scale_y if uneven scaling is desired).
  /3 [/RowCount]: (Area type): Number of rows
  /4 [/ColumnCount]: (Area type): Number of columns
  /5 [/RowMajorOrder]: (Area type): Row/column order: false: Row-major, true: Column-major
  /6 [/TextOnPathTRange]: (Text on path): List[float], specifies the start and end position on the path.
      A value of 3.2 indicates "at t=0.2 on the bezier segment between Node 3 and Node 4".
  /7 [/RowGutter]: (Area type): Row gutter
  /8 [/ColumnGutter]: (Area type): Column gutter
  /9 [/Spacing]: (Area type): Inset spacing [pt]
  /10 [/FirstBaseAlignment]: (Area type): Settings for the first baseline
    /0 [/Flag]: First baseline: 0: Fixed, Unspecified: Ascent, 2: Cap height, 3: Leading, 
                        4: By x, 5: Em Box Height, 6: Legacy
    /1 [/Min]: Minimum width of first baseline [pt]
  /11 [/PathData]: Settings for text on path
    /0 [/Flip]: Flipped [bool], default: false
    /1 [/Effect]: Effect type: 0: Rainbow (default), 1: Skew, 2: Ribbon, 3: 3D Stairstep, 4: Gravity,
    /2 [/Alignment]: Align to: Unspecified: Baseline, 0: Ascender, 1: Descender, 2: Center
    /4 [/Spacing]: Spacing [pt] *
    /18 [/Spacing2]: Spacing [pt] *
  /13 [/VerticalAlignment]: (Area type): Vertical Alignment: Unspecified: Top, 1: Center, 2: Bottom, 3: Justify *

```
* These keys are only written in current versions, not CS, and also not in the clipboard data. 
Therefore, we've assigned them names.

## Implementation in Inkscape SVG

Inkscape generally supports texts, Area Text and Text on path very well.
The majority of features of the text document are supported by SVG2 / CSS Fonts Level 4, 
although not all of it may be supported by Inkscape, or even browsers 
(browsers often lack SVG support of CSS features they support for normal websites). 
Such features have low priority for the importer.

Text elements will consist of multiple `<tspan>`s, which have different styles applied to them. 
This is governed by the character / paragraph style settings for each `Story`. 

The default character and paragraph styles should be implemented as CSS rules that affect all text-like elements, 
if it is possible to specify the property independent of font size (e.g. in percent). Overriden properties are
specified on the `<tspan>`/`<text>`.
The distinction between character and paragraph style doesn't exist in Inkscape and is not required.

Refer to the [`<text>` SVG specification](https://www.w3.org/TR/SVG2/text.html#Introduction) 
and in particular [`shape-inside`](https://www.w3.org/TR/SVG2/text.html#TextShapeInside) for Area Text 
and [`<textPath>`](https://www.w3.org/TR/SVG2/text.html#TextPathElement) for Text-on-path, 
and to the [`CSS Fonts specification`](https://drafts.csswg.org/css-fonts/).

### Known unsupported features

Even SVG2 texts (which are not supported by browsers) don't really have advanced paragraph layouting capabilities. 
That means that we won't be able to support Enumeration / Itemization, 
(anything except [basic](https://www.w3.org/TR/css-text-3/#text-indent-property)) indentation, tab stops etc. 
Furthermore, SVG `shape-inside` [does not support](https://gitlab.com/inkscape/inbox/-/issues/760) vertical alignment in Inkscape.

SVG `shape-inside` flowed texts don't support multi-row / multi-column arrangements. 
A possible very-low-priority workaround would be splitting the `/ConfiningPath` into multiple 
subpaths according to the row/column arrangement 
(keeping the fact in mind that `Inset Spacing` is not applied to rows/columns, 
but `shape-padding` is applied to all subpaths of the `shape inside`).

