<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Hierarchical Data structure

Individual lines typically are prefixed with `%_`, which in case of binary data
is shortened to `%`.

The data structure is hierarchical and deeply nested; containers are opened with `/ContainerType` and terminated with `;`. There are different types of containers:

* [`/Array`](#array)s have children that are separated with `,` - but no keys.
* [`/ArtDictionary`](#artdictionary), [`/Dictionary`](#dictionary) and [`/XMLNode`](#xmlnode) are dictionary-like structures with unique keys, the syntax is 
  ```
  DictionaryContainerName : qualifier
  ((Container | value "/"DataType) "("key")" "," )* ;
  ```
  A [`/Dictionary`](#dictionary) is always a child of another container, while an [`/ArtDictionary`](#artdictionary) is always part of commands (occurs among oneline commands).
  Qualifiers are `/Recorded` or `/NotRecorded` (what do they mean?)
* [`/KnownStyle`](#knownstyle) , [`/ActiveStyle`](#activestyle), [`/SimpleStyle`](#simplestyle),  [`/BlendStyle`](#blendstyle), [`/FillStyle`](#fillstyle), [`/StrokeStyle`](#strokestyle), 
[`/BasicFilter`](#basicfilter),  [`/CompoundFilter`](#compoundfilter), [`/AI11Text`](#ai11text), [`/Document`](#document), [`/GObjRef`](#gobjref) and [`/SVGFilter`](#svgfilter) are objects. At least for [`/CompoundFilter`](#compoundfilter) the "key" might occur more than once. The syntax is
  ```
  ObjectContainerName :
  ((Container | value) /"key")* ;
  ```
  Values may be main-text data (one-line commands or even other [`/ArtDictionary`](#artdictionary)s).
* [`/Art`](#art) only contains main-text data, but no keys.


## Container types
Container types always occur before a `:`.  

### `/Array`


Example:

```
%_/Array :
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
%_/Dictionary :
%_(Page 2) 
[...]
%_; ,
%_; (ArtboardArray) ,
```

### `/Dictionary`

Example:

```
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
```

### `/ArtDictionary`

Example:

```
%_/ArtDictionary :
%_/Dictionary :
%_(ai::Ellipse) /UnicodeString (ai::LiveShape::HandlerName) ,
%_/Dictionary :
%_8196.5 /Real (ai::Ellipse::CenterX) ,
%_8000.5 /Real (ai::Ellipse::CenterY) ,
%_199.404112284301 /Real (ai::Ellipse::Width) ,
%_199.404112284301 /Real (ai::Ellipse::Height) ,
%_1 /Bool (ai::Ellipse::Clockwise) ,
%_1 /Int (ai::Ellipse::InitialQuadrant) ,
%_0 /Real (ai::Ellipse::RotationAngle) ,
%_0 /Real (ai::Ellipse::PieStartAngle) ,
%_0 /Real (ai::Ellipse::PieEndAngle) ,
%_; (ai::LiveShape::Params) ,
%_; (ai::LiveShape) ,
%_;
%_
```

### `/Document`

Child properties:
  * `/Recorded: `[`/Dictionary`](#dictionary)
  * `/NotRecorded: `[`/Dictionary`](#dictionary)

Example (strongly abbreviated):

```
/Document :
    /Dictionary :
        ...
        /Array :
            ...
        ; (ArtboardArray) ,
        16383 /Int (AIDocumentCanvasSize) ,
        ...
        8191 8441 /RealPoint
        (PerspectiveGrid_RightPlaneBottomPoint) ,
    ; /Recorded ,
    /Dictionary : /NotRecorded ,
        ...
        /Binary : /ASCII85Decode ,
            <binary data>
        ; (AI12 Document Profile Data) ,
    ; /NotRecorded ,
;
```

### `/AI11Text`

Occurs within the main-text data as text reference; no prefixed `%_`.

Child properties:
  * `/FreeUndo : int`
  * `/FrameIndex : int` - if a flowtext flows across multiple frames, this indidicates
  the frame number
  * `/StoryIndex : int` - Story index in the `TextDocument`
  * `/ConfiningPath : ` [`/Art`](#art) - For flowtext and text-along-path
  * `/TextAntialiasing : int`

Example:
```
/AI11Text :
    0 /FreeUndo ,
    0 /FrameIndex ,
    0 /StoryIndex ,
      /Art :
        <main text commands, including possibly other /ArtDictionary's (Live shapes)>
      ; /ConfiningPath ,
    2 /TextAntialiasing ,
;
```

### `/Art`

Contains - without key - main-text data. May contain [`/ArtDictionary`](#artdictionary)s (for live shapes)

Example:

```
/Art :
X=
... more commands
X+
;
```

### `/KnownStyle`

Child properties:
  * `/Name : WrappedString`
  * `/Def : Union[`[`/SimpleStyle`](#simplestyle)`, `[`/ActiveStyle`](#activestyle)`]`

Example:

```
/KnownStyle :
([Standard]) /Name ,
/SimpleStyle :
0 O
0 0 0 0 1 1 1 Xa
...
/Paint ;
 /Def ;
```

### `/SimpleStyle`

Child properties:
  * `/Paint` : main-text data 

Example:

```
/SimpleStyle :
0 O
0 0 0 0 1 1 1 Xa
...
/Paint ;
```

### `/ActiveStyle`

Child properties:
  * `/Execution : `[`/CompoundFilter`](#compoundfilter), possible also [`/BasicFilter`](#basicfilter)

Example:

```
/ActiveStyle :
    /CompoundFilter :
        ...
    ;
/Execution ;
```


### `FilterBase` base class

Child properties:
  * `/Filter : Tuple[WrappedString, int, int]`
  * `/Visible : int`
### `/BasicFilter`

inherits from [`FilterBase`](#filterbase-base-class)

Child properties:
  * `/Filter : Tuple[WrappedString, int, int]`, inherited from [`FilterBase`](#filterbase-base-class)
  * `/Visible : int`, inherited from [`FilterBase`](#filterbase-base-class)
  * `/Dict : /Dictionary`, possible entries for `/PluginFileName == "Illustrator.exe"` (for other plugins, different entries might be present; see example below):
    * `StrokeStyle : `[`/StrokeStyle`](#strokestyle),  if present, so are the following entries:
      * `DisplayString : WrappedString` - `Inside` or `Outside`
      * `StrokeOffsetInside : bool` 
    * `BlendStyle : `[`/BlendStyle`](#blendstyle)
    * `FillStyle : `[`/FillStyle`](#fillstyle), if present, so are the following entries:
      * `UseEvenOdd : bool`
  * optional: `/PluginFileName : WrappedString`
  * optional: `/Title : WrappedString`
  * optional: `/FillOrStroke : int`

Example:

```
/BasicFilter :
    (Adobe Stroke Offset) 1 0 /Filter ,
    1 /Visible ,
    (Illustrator.exe) /PluginFileName ,
    (Stroke Offset Live Effect) /Title ,
    2 /FillOrStroke ,
    /Dictionary : /NotRecorded ,
        /StrokeStyle : 0 1 0 0 0 Xy
            0 J 0 j 1 w 10 M []0 d
            0 XR
            0 0 Xd
        /Def ; 
        (StrokeStyle) ,
        (Outside) /String (DisplayString) ,
        0 /Bool (StrokeOffsetInside) ,
    ; /Dict 
;
```

```
/BasicFilter :
  (Adobe Drop Shadow) 1 0 /Filter ,
  1 /Visible ,
  (DropShadow.aip) /PluginFileName ,
  (Drop Shadow) /Title ,
  /Dictionary : /NotRecorded ,
      3 /Real (blur) ,
      50 /Real (dark) ,
      2 /Real (vert) ,
      1 /Int (blnd) ,
      1 /Bool (pair) ,
      0.75 /Real (opac) ,
      1 /Int (csrc) ,
      2 /Real (horz) ,
      /FillStyle : 0 O
        0.745113253593445 0.65221631526947 0.621515214443207 0.813809394836426 k
        0 1 0 0 0 Xy
        0 J 0 j 1 w 10 M []0 d
        0 XR
        0 1 Xd
      /Def ; 
      (sclr) ,
      1 /Bool (usePSLBlur) ,
      16 /Int (Adobe Effect Expand Before Version) ,
  ; 
/Dict ;
```

### `/CompoundFilter`

inherits from [`FilterBase`](#filterbase-base-class)

Child properties:
  * `/Filter : Tuple[WrappedString, int, int]`, inherited from [`FilterBase`](#filterbase-base-class)
  * `/Visible : int`, inherited from [`FilterBase`](#filterbase-base-class)
  * `/Part : `[`FilterBase`](#filterbase-base-class), may be specified multiple times (i.e. `List[`[`FilterBase`](#filterbase-base-class)`]`)

Example:
```
/CompoundFilter :
    (Chain Style Filter) 0 0 /Filter ,
    1 /Visible ,
    /BasicFilter :
        ...
    ; /Part ,
    /BasicFilter :
        ...
    ; /Part ,
;
```

### `/BlendStyle`

Child properties: 
  * `/Def : str` (main-text data, typically style commands)

Example:

```
/BlendStyle : 0 1 0 0 0 Xy
    0 J 0 j 1 w 10 M []0 d
    0 XR
/Def ; 
```

### `/FillStyle`

Child properties:
  * `/Def : ` main-text commands

Example:

```
/FillStyle : 0 O
0 0 0 0 1 1 1 Xa
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
0 1 Xd
/Def ; 
```
### `/StrokeStyle`

Child properties:
  * `/Def : ` main-text commands

Example:

```
/StrokeStyle : 0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
0 0 Xd
/Def ; 
```
### `/GObjRef`
Child properties:
  * `/PluginObject : Tuple[str, str]` 

Example:
```
/GObjRef : (5 pt. Flat) (Adobe Calligraphic Brush Tool) /PluginObject ; (Brush) ,
```

### `/SVGFilter`

Child properties:
  * `/Def`: the filter

Example:

```
/SVGFilter :
    /XMLNode :
        /Dictionary :
            /XMLNode :
                /Dictionary :
                ; (xmlnode-attributes) ,
                2 /Int (xmlnode-nodetype) ,
                /Array :
                ; (xmlnode-children) ,
                (ShadowBlur) /UnicodeString (xmlnode-nodevalue) ,
                (id) /String (xmlnode-nodename) ,
            ; (id) ,
            /XMLNode :
                /Dictionary :
                ; (xmlnode-attributes) ,
                2 /Int (xmlnode-nodetype) ,
                /Array :
                ; (xmlnode-children) ,
                (0) /UnicodeString (xmlnode-nodevalue) ,
                (y) /UnicodeString (xmlnode-nodename) ,
            ; (y) ,
            /XMLNode :
                /Dictionary :
                ; (xmlnode-attributes) ,
                2 /Int (xmlnode-nodetype) ,
                /Array :
                ; (xmlnode-children) ,
                (0) /UnicodeString (xmlnode-nodevalue) ,
                (x) /UnicodeString (xmlnode-nodename) ,
            ; (x) ,
            /XMLNode :
                /Dictionary :
                ; (xmlnode-attributes) ,
                2 /Int (xmlnode-nodetype) ,
                /Array :
                ; (xmlnode-children) ,
                (userSpaceOnUse) /UnicodeString (xmlnode-nodevalue) ,
                (filterUnits) /UnicodeString (xmlnode-nodename) ,
            ; (filterUnits) ,
        ; (xmlnode-attributes) ,
        1 /Int (xmlnode-nodetype) ,
        /Array :
            /XMLNode :
                /Dictionary :
                    /XMLNode :
                        /Dictionary :
                        ; (xmlnode-attributes) ,
                        2 /Int (xmlnode-nodetype) ,
                        /Array :
                        ; (xmlnode-children) ,
                        (feGaussianBlur24) /UnicodeString (xmlnode-nodevalue) ,
                        (id) /UnicodeString (xmlnode-nodename) ,
                    ; (id) ,
                    /XMLNode :
                        /Dictionary :
                        ; (xmlnode-attributes) ,
                        2 /Int (xmlnode-nodetype) ,
                        /Array :
                        ; (xmlnode-children) ,
                        (blur) /UnicodeString (xmlnode-nodevalue) ,
                        (result) /UnicodeString (xmlnode-nodename) ,
                    ; (result) ,
                    /XMLNode :
                        /Dictionary :
                        ; (xmlnode-attributes) ,
                        2 /Int (xmlnode-nodetype) ,
                        /Array :
                        ; (xmlnode-children) ,
                        (SourceAlpha) /UnicodeString (xmlnode-nodevalue) ,
                        (in) /UnicodeString (xmlnode-nodename) ,
                    ; (in) ,
                    /XMLNode :
                        /Dictionary :
                        ; (xmlnode-attributes) ,
                        2 /Int (xmlnode-nodetype) ,
                        /Array :
                        ; (xmlnode-children) ,
                        (3) /UnicodeString (xmlnode-nodevalue) ,
                        (stdDeviation) /UnicodeString (xmlnode-nodename) ,
                    ; (stdDeviation) ,
                ; (xmlnode-attributes) ,
                1 /Int (xmlnode-nodetype) ,
                /Array :
                ; (xmlnode-children) ,
                /String (xmlnode-nodevalue) ,
                (feGaussianBlur) /UnicodeString (xmlnode-nodename) ,
            ; ,
        ; (xmlnode-children) ,
    /String (xmlnode-nodevalue) ,
    (filter) /UnicodeString (xmlnode-nodename) ,
; /Def ;
```

which translates to the following XML (see also [`/XMLNode`](#xmlnode))

```xml
<filter id="ShadowBlur" y="0" x="0" filterUnits="userSpaceOnUse"> 
  <feGaussianBlur id="feGaussianBlur24" result="blur" in="SourceAlpha" stdDeviation="3"/>
</filter>
```


### `/XMLNode`

is a special kind of dictionary with the keys 
 * `xmlnode-attributes : `[`/Dictionary`](#dictionary) of `/XMLNode`s with `xmlnode-nodetype = 2`
 * `xmlnode-children : `[`/Array`](#dictionary) of `/XMLNode`s with `xmlnode-nodetype = 1`
 * `xmlnode-nodetype : int`. `1`: actual xml node, `2`: dummy node for an attribute, `3`: text node (`xmlnode-nodename = #text`),  `9`: root node (`xmlnode-nodename = #document`)
 * `xmlnode-nodename : UnicodeString`
 * `xmlnode-nodevalue : UnicodeString`

### `/XMLUID`

is a wrapper around a single string. 

Example:
```
%_/XMLUID : (Layer_2) ; 
```

### `/Binary`

Binary data is always prefixed with single `%` - even when the surrounding structure is prefixed with `%_`.

The data is Adobe-ASCII-85 encoded. Data ends with `~>`.

Example:
```
/Binary : /ASCII85Decode ,
%87cURD]i,
%"Ebo80~>
;
```

### `/AI11TextDocument`

Subclass of [`Binary`](#binary); remaining data after `~>` is read like a normal object.

Child properties:
 * `/RulerOrigin : Point`

### `/AI11UndoFreeTextDocument`

Subclass of [`Binary`](#binary); remaining data after `~>` is read like a normal object.

No other child properties are known.

## Dictionary value datatypes

These are the datatype specifiers in [`/Dictionary`](#dictionary)s, [`/ArtDictionary`](#artdictionary) and [`/XMLNode`](#xmlnode)s.

###  `/Bool` 

Example:
```
0 /Bool (AI10 flattener outline strokes) ,
1 /Bool (AIPattern_Editor_Dim_Copies_Key) ,
```

### `/Int`

Example:
```
0 /Int (AI10 compound shape mode) ,
5 /Int (AIPattern_Editor_Preview_Cols) ,
```

### `/Real`

Example:
```
0.24 /Real (Raster Art Original Scale) ,
0 /Real (ai::Rectangle::Angle) ,
```

### `/RealPoint`

Example:

```
7231 7651 /RealPoint
 (RulerOrigin) ,
```
### `/RealMatrix`

Example:
```
1 0 0 1 39342.5586 37062.6875 /RealMatrix
 (CAIGradientTformMatrix) ,
```

### `/RealPointRelToROrigin`

Example:
```
0 0 /RealPointRelToROrigin
 (PositionPoint1) ,
```

### `/String`

Example:
```
(2.080288) /String (BBAccumRotation) ,
(27.1.1) /String (kAIFullDocumentVersionStr) ,
(2f56b345-ae09-4763-bcb8-5a769858b3d2) /String (ArtboardUUID) ,
 /String  # empty
```

### `/UnicodeString`  
Example:
```
(0 0 0 0 0  0 0 0 0 0  0 0 0 1 0  0 0 0 1 0) /UnicodeString (xmlnode-nodevalue) ,
(0.05) /UnicodeString (xmlnode-nodevalue) ,
(0689a395-1393-413f-9ee3-c1f8e56062b5) /UnicodeString (AI24 ImageAlphaRawDataUUID) ,
 /UnicodeString # empty
```