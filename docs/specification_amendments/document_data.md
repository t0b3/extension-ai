<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->
# Document data

The document data starts with `%AI9_BeginDocumentData` and ends with `%AI9_EndDocumentData`. 
The data structure was added in AI9, but attributes were added in later versions as well.

It contains:

- Page setup
- some grid settings
- PDF export settings
- Document settings
- `AI12 Document Profile data` - binary, ASCII85 encoded
- ...

The contained data is a [hierarchical data structure](hierarchy.md#document) of type [`/Document`](hierarchy.md#document).

## Page setup

The page setup is stored in an [`/Array`](hierarchy.md#array) with key `ArtboardArray`, which itself is a direct child of [`/Document`](hierarchy.md#document)[`/Recorded`]. Each page is a [`/Dictionary`](hierarchy.md#dictonary).

```
%_/Array :
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
%_/Dictionary :
%_(Page 2) 
[...]
%_; ,
%_; (ArtboardArray) ,
```

* `PositionPoint1` specifies the top-left corner, `PositionPoint2` the bottom-right corner 
in drawing coordinates (`/RealPointRelToROrigin` - relative to (global) ruler origin)
* `IsArtboardSelected`: currently active page
* `DisplayMark`: binary mask. 1 = Show center mark, 2 = Show cross hairs, 4 = show video save areas.
*  `RulerOrigin` specifies the origin of the *artboard ruler* in canvas coordinates.
* `PAR`, ?

AI only has document-wide bleeds (`BleedLeftValue` etc. in the root `/Document` container), specified in px. These should be added to all pages (requires Inkscape 1.3). Margins are only supported through guides (can be added by converting an object (Right click -> Make guides)).