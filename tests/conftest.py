# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Pytest's conftest to configure common fixtures."""

from math import isclose
import math
import os
import sys
import pytest
import unittest.mock
from typing import List, Optional
import subprocess
from collections import OrderedDict
import traceback
from pprint import pprint
from io import StringIO
from inkai.parser.fileparser import FileParser, parse_bytes

from inkai.svg.convert import ai2svg
from inkai.util import to_pretty_xml

HERE = os.path.dirname(__file__) or "."
# This is suggested by https://docs.python-guide.org/writing/structure/.
sys.path.insert(0, os.path.abspath(os.path.join(HERE, "..")))
# from inkai.parser.data_hierarchy import DataHierarchyParser
from inkai.extraction import extract_ai_privatedata
import inkai

from inkex import SvgDocumentElement, load_svg

# from inkai.util import to_pretty_xml
# from inkai.parser.document import AIDocument
# from inkai.parser.lazy import (
#     generate_eol,
#     CollectedSection,
#     UnidentifiedLine,
#     AIElement,
# )
# import inkai.parser.parse as parse
# from inkai.svg.convert import ai2svg
# from inkai.parser.objects.layer import Layer
# from inkai.parser.objects.new import ArtDictionary
# from inkai.svg.interpreter.ai_state.stm import AISTM
# from inkai.svg.interpreter import StatefulInterpreter
# from inkai.svg.svg_builder import SVGBuilder


class ShortFileAccess:
    """Fixture for easy file access.

    a = ShortFileAccess("/")
    a.opt.file -> content
    """

    ignore_files = [".license", ".ai.txt"]

    def __init__(self, path):
        """Create a nice file access for a path."""
        self.__path = path

    def __getattr__(self, name):
        """Return a file content or a folder."""
        return self[name]

    def __getitem__(self, name):
        """Return a file content or a folder."""
        assert not os.path.isfile(self.__path)
        entries = os.listdir(self.__path)
        possible_entries = [
            entry
            for entry in entries
            if not any(entry.endswith(extension) for extension in self.ignore_files)
        ]
        chosen_entries = [entry for entry in possible_entries if name in entry]
        if not chosen_entries:
            raise FileNotFoundError(
                f"{name} does not exist in {self.__path}.\nChoose one in here: {', '.join(possible_entries)}"
            )
        chosen_entry = min(chosen_entries, key=len)
        new_path = os.path.join(self.__path, chosen_entry)
        return self.__class__(new_path)

    def __repr__(self):
        return f"ShortFileAccess({self.__path})"

    def read(self, binary=True):
        """Read the contents of the file."""
        assert os.path.isfile(self.__path)
        with open(self.__path, ("rb" if binary else "r")) as file:
            return file.read()

    @property
    def path(self):
        """The path of the file or directory."""
        return self.__path

    def with_extension(self, extension: str):
        """Return the path with an extension."""
        return self.__path + "." + extension


@pytest.fixture(scope="module")
def data():
    """Access to the data directory."""
    return ShortFileAccess(HERE).data


# @pytest.fixture(scope="module")
# def ai_module():
#     """Cached version of the parser so it does not need to be reconstructed."""
#     return AdobeIllustratorParser()


# @pytest.fixture()
# def ai(ai_module):
#     """Return a parser to check the grammar implementation with."""
#     ai_module.reset()
#     return ai_module


@pytest.fixture()
def mock():
    """Return a unittest mock object."""
    return unittest.mock.MagicMock()


@pytest.fixture()
def cmd():
    """Execute this extension on the command line with the given arguments."""
    cwd = os.path.join(HERE, "..")

    def cmd(arguments: List[str]) -> subprocess.Popen:
        """Execute this extension on the command line with the given arguments."""
        command = [sys.executable, inkai.__name__]
        command.extend(arguments)
        print("cmd: ", "'" + "' '".join(command) + "'")
        return subprocess.Popen(
            command,
            cwd=cwd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )

    return cmd


# ----------------------------------------------------------------------
#
# SVG access
#

EXPECTED_STATUS = [
    [],
    [],
    ["simplified_corel_export.ai"],
    [],
    [
        "20000x10000_origin_at_100_0_CC.ai",
        "20000x10000_origin_at_100_100_CC.ai",
        "20000x10000_origin_topleft_CC.ai",
        "20x40mm_origin_topleft_CC.ai",
        "20x40mm_origin_topleft_CS2.ai",
        "2x4cm_origin_10.2306_0mm_AI10.ai",
        "2x4cm_origin_10.2306_0mm_AI10_v2.ai",
        "2x4cm_origin_10.2306_0mm_CC.ai",
        "2x4cm_origin_topleft_AI10.ai",
        "2x4cm_origin_topleft_AI3.ai",
        "2x4cm_origin_topleft_AI8.ai",
        "2x4cm_origin_topleft_AI9.ai",
        "2x4cm_origin_topleft_CC.ai",
        "525x801_origin_at_100_0_CC.ai",
        "525x801_origin_at_100_0_CS3.ai",
        "525x801_origin_at_100_0_first_page_moved_to_1000_1000.ai",
        "525x801_origin_at_100_0_first_page_moved_to_1000_1000_CS4.ai",
        "525x801_origin_at_100_100_AI10.ai",
        "525x801_origin_at_100_100_AI3.ai",
        "525x801_origin_at_100_100_AI8.ai",
        "525x801_origin_at_100_100_AI9.ai",
        "525x801_origin_at_100_100_CC.ai",
        "525x801_origin_at_100_100_CC_Legacy.ai",
        "525x801_origin_at_100_100_CS.ai",
        "525x801_origin_at_100_100_CS2.ai",
        "525x801_origin_at_100_100_CS3.ai",
        "525x801_origin_at_100_100_CS4.ai",
        "525x801_origin_at_100_100_CS5.ai",
        "525x801_origin_at_100_100_CS6.ai",
        "525x801_origin_topleft_CC.ai",
        "525x801_origin_topleft_CS6.ai",
        "circle.ai",
        "cmyk_rectangle.ai",
        "empty_cs.ai",
        "empty_cs2.ai",
        "flowtext.ai",
        "inkscape_minimized_ai10.ai",
        "inkscape_minimized_ai3.ai",
        "inkscape_minimized_ai7.ai",
        "inkscape_minimized_ai9.ai",
        "inkscape_minimized_ai_cc.ai",
        "inkscape_minimized_ai_cc_current.ai",
        "inkscape_minimized_ai_cs.ai",
        "inkscape_minimized_ai_cs2.ai",
        "inkscape_minimized_ai_cs3.ai",
        "inkscape_minimized_ai_cs4.ai",
        "inkscape_minimized_ai_cs5.ai",
        "inkscape_minimized_ai_cs6.ai",
        "irregular_closed_shape.ai",
        "irregular_line.ai",
        "layers_and_sublayers.ai",
        "layers_and_sublayers_extracted.ai",
        "line_broken_in_middle.ai",
        "page_moved_to_1000_2000_rect200_300.ai",
        "raster_ai8.ai",
        "rectangle-rounded-corners-all-different.ai",
        "rectangle.ai",
        "rectangle_with_rounded_corners.ai",
        "rectangles_origin.ai",
        "rgb_rectangle.ai",
        "simple_cc_legacy.ai",
        "simple_cs.ai",
        "simple_cs2.ai",
        "simple_cs3.ai",
        "simple_cs4.ai",
        "simple_cs5.ai",
        "simple_cs6.ai",
        "simple_raster.ai",
        "simple_v10.ai",
        "simple_v2020.ai",
        "simple_v2020_no_pdf_data.ai",
        "simple_v3.ai",
        "simple_v3_jp.ai",
        "simple_v3_with_newlines.ai",
        "simple_v8.ai",
        "simple_v9.ai",
        "styles.ai",
        "text_transforms.ai",
    ],
]

DUPLICATED_FILES = []
AI_FILES = OrderedDict()  # parameter: path
for dirpath, dirnames, filenames in os.walk(HERE):
    for filename in filenames:
        if filename.endswith(".ai"):
            if filename in AI_FILES:
                DUPLICATED_FILES.append(filename)
            AI_FILES[filename] = os.path.join(dirpath, filename)


@pytest.fixture()
def duplicated_files() -> List[str]:
    """A list of duplicated file names."""
    return DUPLICATED_FILES


NOT_TRIED = object()
FAILED = object()
INVALID_VALUES = [NOT_TRIED, FAILED]


class Step:
    """One step of the progress we make."""

    def __init__(self, done: str, fail: str, func: callable):
        "Create a new step."
        self._message_done = done
        self._message_fail = fail
        self._tried = False
        self._error = None
        self._error_message = ""
        self._func = func

    def _run(self):
        """Run this step once."""
        if self._error or self._tried:
            return
        self._tried = True
        try:
            self._func()
        except:
            ty, err, tb = sys.exc_info()
            self._error = err
            f = StringIO()
            traceback.print_exception(ty, err, tb, file=f)
            self._error_message = f.getvalue()

    def run_or_fail(self):
        """Run this step but raise an error if we fail."""
        self._run()
        if self._error:
            print(self._error_message)
            raise self._error

    def run_or_skip(self):
        """Run this step but skip if we did not succeed before."""
        self._run()
        if self._error:
            print(self._error_message)
            message = f"I skip this test because I {self._message_fail}."
            print("skip", message)
            pytest.skip(message)

    def __repr__(self):
        """repr(self)"""
        if not self._tried:
            return f"have not tried: {self._message_done}"
        if self._error:
            return self._message_fail
        return self._message_done

    def was_tried(self):
        """Whether this step was tried at least once."""
        return self._tried

    def has_error(self):
        """Whether this step had an error."""
        return self._error is not None


class StepwiseFileProcessing:
    """Process AI files in steps."""

    _all_processes = set()
    processing_steps = 6

    def __init__(self, name):
        """Create a stepwise process."""
        self.name = name
        self.path = AI_FILES[name]
        self._steps = [
            Step("found", "", lambda: None),
            Step("extracted", "failed to extract", self._01_extract),
            Step("parsed document in", "failed to parse document", self._02_parse),
            Step(
                "parsed everything in",
                "failed to parse everything in",
                self._03_parse_everything,
            ),
            Step(
                "know all the lines in",
                "found unknown lines in",
                self._04_check_unknown_lines,
            ),
            Step("am 100% happy with", "", lambda: None),  # last step
        ]
        assert len(self._steps) == self.processing_steps
        self._steps[0].run_or_fail()
        self._all_processes.add(self)
        self._svg: Optional[SvgDocumentElement] = None

    def _01_extract(self):
        """The extraction step."""
        with open(self.path, "rb") as file:
            self._extracted_document = extract_ai_privatedata(file.read())

    def _02_parse(self):
        """Parse the document."""
        self._document = parse_bytes(self._extracted_document, FileParser.read_document)

    def _03_parse_everything(self):
        """Make sure the content does not generate an error."""
        self.get_unkown_lines(self._document)

    def _04_check_unknown_lines(self):
        """Make sure the whole content is known."""
        unkown_lines = self.get_unkown_lines(self._document)
        for line in unkown_lines[:10]:
            print(line)
        assert not unkown_lines, f"{len(unkown_lines)} lines are not identified."

    def get_unkown_lines(self, ai_element):
        """Return all the unkown lines from the section recursively."""
        # check that we can actually parse everything even if it is not AIElements
        # getattr(ai_element, "parsed_content", None)
        # print(ai_element)
        # ai_element.tokens
        # result = [ai_element] if isinstance(ai_element, UnidentifiedLine) else []
        # for child in ai_element.children:
        #     result += self.get_unkown_lines(child)
        # return result

    @property
    def steps(self):
        """The number of steps taken."""
        for i, step in enumerate(self._steps):
            if not step.was_tried():
                return i - 1
        return i

    def run(self, index: int):
        """Run a step"""
        for i in range(index):
            self._steps[i].run_or_skip()
        self._steps[index].run_or_fail()

    @property
    def step_name(self):
        """The name of step we are at."""
        return str(self._steps[self.steps])

    def __repr__(self):
        """Return a good representation."""
        return f"I {self.step_name} {self.name} as step {self.steps}."

    @classmethod
    def get_current_status(cls):
        """Return the current status as it is."""
        current_status = []
        for process in cls._all_processes:
            while len(current_status) <= process.steps:
                current_status.append([])
            current_status[process.steps].append(process.name)
        for l in current_status:
            l.sort()  # Sort the filenames so we do not have a problem with git.
        return current_status

    @classmethod
    def get(self, name):
        """Return a StepwiseFileProcessing for the ai file with the name."""
        processes = [process for process in self._all_processes if name in process.name]
        processes.sort(key=lambda process: len(process.name))
        assert (
            len(processes) == 1
            or len(processes) > 1
            and len(processes[0].name) < len(processes[1].name)
        ), f"Name {name} should only match one of {', '.join(process.name for process in processes)}"
        return processes[0]

    @property
    def svg(self) -> SvgDocumentElement:
        """Return the SVG document."""
        if self._svg is not None:
            return self._svg
        self._svg = ai2svg(self.document)
        print(to_pretty_xml(self._svg.tostring()).decode("latin-1"))
        return self._svg

    @classmethod
    def print_current_status_if_it_differs(cls):
        """Test update for convenience."""
        current_status = cls.get_current_status()
        if current_status != EXPECTED_STATUS:
            pprint(current_status)
            return True
        return False

    @property
    def expected_step_to_get_stuck_at(self) -> int:
        """Normally, processes get stuck at one point or succeed.

        Return the step index.
        """
        for i, filenames in enumerate(EXPECTED_STATUS):
            if self.name in filenames:
                return i
        return -1

    @classmethod
    def get_process_fixture_parameter_list(cls):
        """Return a sorted list for testing."""
        if not cls._all_processes:
            for name in AI_FILES:
                cls(name)
        l = list(cls._all_processes)
        l.sort(key=lambda self: self.name)
        return l

    @property
    def document(self):
        """Process all steps so we can return the document."""
        self.run(2)
        return self._document


class ProcessedAttribute:
    """Fast access to generated SVG files and such, generated."""

    def __init__(self, attribute: str):
        self.attribute = attribute

    def __getattr__(self, name) -> object:
        """Return the attibute of the processed ai file."""
        processing = StepwiseFileProcessing.get(name)
        assert processing is not None
        return getattr(processing, self.attribute)

    __getitem__ = __getattr__


@pytest.fixture()
def documents() -> ProcessedAttribute:
    """Return processed documents that are cached accross the test runs.

    To generate a document from inkscape_minimized_ai3.ai,
    use this code:

        document = documents.inkscape_minimized_ai3
    """
    return ProcessedAttribute("document")


@pytest.fixture()
def svgs() -> ProcessedAttribute:
    """Return processed svgs that are cached accross the test runs.

    Like documents.
    """
    return ProcessedAttribute("svg")


@pytest.fixture(params=StepwiseFileProcessing.get_process_fixture_parameter_list())
def process(request) -> StepwiseFileProcessing:
    """Return each stepwise processing operation from .ai to .svg."""
    return request.param


@pytest.fixture()
def last_process() -> StepwiseFileProcessing:
    """Return only one StepwiseFileProcessing."""
    return StepwiseFileProcessing.get_process_fixture_parameter_list()[-1]


@pytest.fixture(
    params=list(range(1, StepwiseFileProcessing.processing_steps)), scope="module"
)
def processing_step(request) -> str:
    """Return each stepwise processing operation from .ai to .svg."""
    return request.param


def assertListAlmostEqual(list1, list2, tol):
    assert len(list1) == len(list2)
    for a, b in zip(list1, list2):
        assert isclose(a, b, abs_tol=tol)


def assertAttributesAlmostEqual(node, expected):
    for k, v in expected.items():
        if isinstance(v, (float, int)):
            assert math.isclose(float(node.get(k)), v)
        else:
            assert node.get(k, None) == v


# class DataObjectForTests:
#     """Shortcut for tests."""

#     def __init__(self, ai):
#         """Create this."""
#         self.ai = ai

#     def parse_string(self, string, parse_all=False):
#         """Parse the string."""
#         if string.startswith("%"):
#             parser = self.ai.data_hierarchy_section
#         else:
#             parser = self.ai.data_object
#             string = string.replace("\n", " ")
#         return parser.parse_string(string, parse_all=parse_all)


# @pytest.fixture(params=["ai.data_object", "DataHierarchyParser"])
# def data_object(request, ai):
#     """Return the parsers for the hierarchical data object."""
#     if request.param == "ai.data_object":
#         return DataObjectForTests(ai)
#     return DataHierarchyParser()


# class RectangleTestData:
#     """Test data for rectangles."""

#     def __init__(self, layer_result: pp.ParseResults):
#         self.layer_result = layer_result

#     @property
#     def layer(self) -> inkex.Layer:
#         """The base element of the file."""
#         assert len(self.layer_result) == 1
#         layer = self.layer_result[0]
#         assert len(layer) >= 1
#         return layer

#     @property
#     def rect(self) -> inkex.ShapeElement:
#         """The rectangle."""
#         return self.layer[0]

#     @property
#     def path_effect(self) -> inkex.PathEffect:
#         """The path effect of the rectangle."""
#         return self.layer[1]  # Assume there is a path effect


# @pytest.fixture()
# def command():
#     """A AIElement subclass to test with."""
#     return generate_eol("attr name Cmd", "example", [int, parse.ps])


# @pytest.fixture()
# def command_module():
#     """The name of this module."""
#     return ShortFileAccess.__module__


# @pytest.fixture()
# def rect(data, _rectangle_cache={}) -> RectangleTestData:
#     """Return a function to generate rectangles from the grammar.

#     This is module scoped to speed up the tests.
#     """

#     def rect(file: str) -> inkex.ShapeElement:
#         """Read out the rectangle."""
#         if file in _rectangle_cache:
#             return _rectangle_cache[file]
#         content = data.rectangle[file].read(binary=False)
#         layer = Layer.from_string(content)
#         return layer.categories[ArtDictionary][-1].live_shape  # type: ignore

#     return rect


# @pytest.fixture()
# def command():
#     """A AIElement subclass to test with."""
#     return generate_eol("attr name Cmd", "example", [int, parse.ps])


# @pytest.fixture()
# def command_module():
#     """The name of this module."""
#     return ShortFileAccess.__module__


# @pytest.fixture()
# def stm():
#     """The state machine for the AI document in its initial state."""
#     return AISTM()


# AISTM.debug = True


# @pytest.fixture()
# def stmi() -> StatefulInterpreter:
#     """The state machine inside of the interpretation adapter."""
#     stmi = StatefulInterpreter()
#     stmi._stm.debug = True
#     return stmi


# @pytest.fixture()
# def svg_builder() -> SVGBuilder:
#     """Create a new SVGBuilder"""
#     return SVGBuilder()


def parse_string(string, method):
    return parse_bytes(string.encode(), method)
