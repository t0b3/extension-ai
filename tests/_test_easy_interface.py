# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests that there are certain functions available when importing inkai."""

import inkai
from inkai.parser.document import AIDocument
import pytest


def test_extract(data):
    """We want to be able to extract document content."""
    content = inkai.extract(data.ai.empty_cs.path)
    lines = content.split("\n", 2)
    assert lines[0].strip() == "%!PS-Adobe-3.0"
    assert lines[1].strip() == "%%Creator: Adobe Illustrator(R) 11.0"


def test_extract_fails(data):
    """Check what happens if we cannot extract."""
    with pytest.raises(inkai.ExtractionError):
        inkai.extract(data.rectangle["90-deg-corners"].path)


def test_parse_document(data):
    """This is how we would like to get the root, the document."""
    document = inkai.parse(data.ai.empty_cs.path)
    assert isinstance(document, AIDocument)


def test_parse_fails(data):
    """Check what happens if we cannot extract."""
    with pytest.raises(inkai.ExtractionError):
        inkai.parse(data.rectangle["90-deg-corners"].path)


def test_conversion_to_svg(data):
    """Check that we can convert AI documents to SVG."""
    svg = inkai.to_svg(data.ai.empty_cs.path)
    assert svg.startswith(b"<")
    assert b"<svg" in svg
