# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Tests for the state machine.

This tests the states and the transitions from one state to another.
"""
from inkai.parser.objects.path import m, PathRender, c
from inkai.parser.objects.image import Raster
from inkai._svg.interpreter.ai_state.stm import AISTM, Transition
from inkai._svg.interpreter.ai_state.adapter import AISTMAdapter
from unittest.mock import MagicMock
from inkai.parser.lazy import AIElement
from typing import Union
from inkai._svg.interpreter.selector import CategorySelector
import pytest
from inkai._svg.interpreter.state import StatefulInterpreter
from inkai._svg.interpreter import ModificationFactory, on_element
from inkai._svg.modification import ModificationChain, NullModification


def test_state_machine_creates_path(stm: AISTM):
    """Check how the state machine creates a path."""
    stm.create_path()
    assert stm.state.is_creating_a_path()
    stm.render_path()
    assert not stm.state.is_creating_a_path()


def test_state_machine_creates_raster_image(stm: AISTM):
    """Check how the state machine creates a raster image."""
    stm.create_raster()
    assert stm.state.is_creating_a_raster_image()
    stm.render_path()
    assert not stm.state.is_creating_a_raster_image()


def test_adapter_mapping_of_ai_element_calls():
    stm = MagicMock()
    interpreter = MagicMock()
    adapter = AISTMAdapter(stm, {m: Transition.create_path})
    adapter.interpret_element(CategorySelector(m), m.example, interpreter)
    interpreter.interpret.assert_not_called()
    stm.do.assert_called_once_with("create_path")


def test_do_a_transition(stm):
    """Check that we can use names to transition."""
    stm.do("create_raster")
    assert stm.state.is_creating_a_raster_image()


def test_invalid_transition_name(stm):
    with pytest.raises(ValueError) as e:
        stm.do("invalid name")
    assert "invalid name" in str(e.errisinstance)


def test_stateful_interpreter_before_and_after(stmi: StatefulInterpreter, mock):
    """This tests that the state is actually changed during interpretation."""
    stmi.before.interpret_element(CategorySelector(m), m.example, mock)
    assert stmi.state.is_creating_a_path()
    stmi.after.interpret_element(CategorySelector(PathRender), PathRender.example, mock)
    assert not stmi.state.is_creating_a_path()


def test_interprete_directly(stmi: StatefulInterpreter):
    """Check the the calls are transferred."""
    stmi.interpret(m.example())
    assert stmi.state.is_creating_a_path()
    stmi.interpret(PathRender.example())
    assert not stmi.state.is_creating_a_path()


class ExampleFactory(ModificationFactory):
    is_in_path = None

    @on_element(AIElement)
    def side_effect(self, element: AIElement):
        """create a side effect"""
        self.is_in_path = self.state.is_creating_a_path()


def test_interpretation_before(stmi: StatefulInterpreter):
    """Check that the factory is inside the path."""
    tf = ExampleFactory()
    stmi.add_factory(tf)
    assert tf.is_in_path is None
    stmi.interpret(m.example())
    assert tf.is_in_path is True


def test_interpretation_after(stmi: StatefulInterpreter):
    """Check that the factory is inside the path."""
    tf = ExampleFactory()
    stmi.add_factory(tf)
    stmi.interpret(m.example())
    tf.is_in_path = None
    stmi.interpret(PathRender.example())
    assert tf.is_in_path is True


def test_interpretation_without_state_change(stmi: StatefulInterpreter):
    """Check that the factory can be used outside the path, too."""
    tf = ExampleFactory()
    stmi.add_factory(tf)
    stmi.interpret(c.example())
    assert tf.is_in_path is False


def test_subscribing_to_the_interpreter_subscribes_to_the_factories(
    stmi: StatefulInterpreter,
):
    modifications = ModificationChain()
    tf1 = ExampleFactory()
    tf2 = ExampleFactory()
    stmi.add_factory(tf1)
    stmi.attach(modifications)
    stmi.add_factory(tf2)
    m1 = NullModification()
    m2 = NullModification()
    m3 = NullModification()
    tf1.notify_observers(m1)
    tf2.notify_observers(m2)
    tf1.notify_observers(m3)
    mods = modifications.modifications
    expected_mods = [m1, m2, m3]
    print(mods, expected_mods)
    assert mods == expected_mods
