# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the creation of path elements."""
from inkai._svg.path import PathElement
from inkai._svg.modification import AddChild


def test_path_element_is_created_with_m():
    """m creates a path element."""
    mods = PathElement().generate_modifications_for(
        "m", is_creating_a_compound_path=False
    )
    mods.assert_matches([AddChild])
    assert mods.child.tag_name == "path"
