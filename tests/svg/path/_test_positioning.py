# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the positioning of paths."""
import pytest
from inkai._svg.coordinates import TestTransform
from inkai._svg.path import NodeTypes, PathCommands
from inkex.paths import Curve, Line, Move, Path as SVGPath, ZoneClose
from inkai._svg.modification import AddChildAttributes

d = TestTransform.from_drawing


@pytest.mark.parametrize(
    "section,expected_commands",
    [
        ("237.5 636.4792 m", [Move(*d(237.5, 636.4792))]),
        ("268 631 m", [Move(*d(268, 631))]),
        ("268 649 L", [Line(*d(268, 649))]),
        ("10 10 m\n1 1 1 1 v", [Move(*d(10, 10)), Curve(*d(10, 10, 1, 1, 1, 1))]),
        (
            "10 10 m\n11 12 13 14 V",
            [Move(*d(10, 10)), Curve(*d(10, 10, 11, 12, 13, 14))],
        ),
        ("15 16 17 18 y", [Curve(*d(15, 16, 17, 18, 17, 18))]),
        (
            "224.065 641.8612 230.0802 636.4792 237.5 636.4792 c",
            [Curve(*d(224.065, 641.8612, 230.0802, 636.4792, 237.5, 636.4792))],
        ),
    ],
)
def test_path_command_list(section, expected_commands):
    """Create a list of path commands from a section."""
    commands: SVGPath = (
        PathCommands(TestTransform())
        .generate_modifications_for(section)
        .factory.commands
    )
    assert commands == expected_commands


@pytest.mark.parametrize(
    "ai_string,sodipodi_nodetypes",
    [
        ("237.5 636.4792 m", "s"),
        ("268 631 m", "s"),
        ("268 649 L", "c"),
        ("225 200 l", "s"),
        ("224.065 641.8612 230.0802 636.4792 237.5 636.4792 c", "s"),
    ],
)
def test_svg_path_attribute_sodipodi_nodetypes(ai_string, sodipodi_nodetypes):
    """This checks the path after some actions have run."""
    note_types: str = (
        NodeTypes().generate_modifications_for(ai_string).factory.nodetypes
    )
    assert note_types == sodipodi_nodetypes


PATH_WITHOUT_PAINTING_OPERATOR = """37.7537 26.5107 m
33.0604 26.233 28.0377 22.8297 32.0115 19.4736 C
35.6989 16.2855 41.2802 20.1701 43.074 24.6533 C
41.9529 26.1165 39.887 26.6369 37.7537 26.5107 c
"""


@pytest.mark.parametrize(
    "paint,closed",
    [
        ("N", True),
        ("f", True),
        ("s", True),
        ("b", True),
        ("n", False),
        ("F", False),
        ("S", False),
        ("B", False),
    ],
)
def test_close_the_path(paint, closed):
    """Check whether the path is closed."""
    add_attr: AddChildAttributes = (
        PathCommands(TestTransform()).generate_modifications_for(
            PATH_WITHOUT_PAINTING_OPERATOR + paint
        )
    ).modifications[-1]
    path = add_attr.attributes["d"]
    assert isinstance(path[-1], ZoneClose) == closed


@pytest.mark.parametrize("closing_command", ["N", "f", "s", "b"])
@pytest.mark.parametrize("nodetype", ["a", "c", "s", "z"])
def test_nodetype_for_closing_is_first_nodetype(closing_command, nodetype):
    """The nodetype of Z is the same as the one of the first node."""
    nodetypes = NodeTypes()
    nodetypes.nodetypes = nodetype + "cc"
    modifications = nodetypes.generate_modifications_for(closing_command)
    modifications.assert_matches(
        [AddChildAttributes({"sodipodi:nodetypes": nodetype + "cc" + nodetype})]
    )
