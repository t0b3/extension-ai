# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Tests for the palette.

See AI Spec page 78+
"""
from inkai.parser.setup.palette import Palette, Pb, PB, Pc, PaletteCell, PaletteCellName
from inkai.parser.document import AIDocument
from inkai.parser.lazy import UnidentifiedLine
import pytest


@pytest.mark.parametrize("topLeftCellIndex,selectedIndex", [(0, 0), (10, 20)])
@pytest.mark.parametrize(
    "create_Pb",
    [
        lambda topLeftCellIndex, selectedIndex: Pb.from_string(
            f"{topLeftCellIndex} {selectedIndex} Pb"
        ),
        lambda topLeftCellIndex, selectedIndex: Palette.from_string(
            f"%AI5_BeginPalette\n{topLeftCellIndex} {selectedIndex} Pb\n%AI5_EndPalette"
        ).beginning,
    ],
)
def test_Pb(topLeftCellIndex, selectedIndex, create_Pb):
    """Check parsing the Pb command."""
    pb: Pb = create_Pb(topLeftCellIndex, selectedIndex)
    assert pb.top_left_cell_index == topLeftCellIndex
    assert pb.selected_index == selectedIndex


@pytest.fixture()
def example_palette() -> Palette:
    return Palette.from_string(Palette.example_string)


@pytest.fixture()
def cells(example_palette):
    return example_palette.cells


@pytest.mark.parametrize(
    "cls, count",
    [
        (PB, 1),
        (Pb, 1),
        (Pc, 8),
    ],
)
def test_complete_palette_cell_contains_elements(cls, count, example_palette):
    """Check that the elements are in there."""
    assert len(example_palette.categories[cls]) == count


def test_example_palette_cells(cells):
    """Check how many cells there are."""
    assert len(cells) == 8


def test_example_palette_cell_0(cells):
    """Check a cell."""
    assert cells[0].color.alpha == 0
    assert cells[0].name == "none"


def test_example_palette_cell_1(cells):
    """Check a cell."""
    assert cells[1].color.gray == [1]
    assert cells[1].name == ""


def test_example_palette_cell_2(cells):
    """Check a cell."""
    assert cells[2].color.gray == [0]
    assert cells[2].name == ""


def test_example_palette_cell_3(cells):
    """Check a cell."""
    assert cells[3].color.cmyk == [0, 0, 0, 0]
    assert cells[3].name == ""


def test_example_palette_cell_4(cells):
    """Check a cell."""
    assert cells[4].color.gray == [0.75]
    assert cells[4].name == ""


def test_example_palette_cell_5(cells):
    """Check a cell. Gradient."""
    assert cells[5].color is None
    pytest.skip("TODO: Implement after gradient MR is merged.")
    assert cells[5].name == "Black & White"


def test_example_palette_cell_6(cells):
    """Check a cell."""
    assert cells[6].color.cmyk == [0.25, 0, 0, 0]
    assert cells[6].name == ""


def test_example_palette_cell_7(cells):
    """Check a cell."""
    assert cells[7].color.cmyk == [1, 0.5, 1, 0]
    assert cells[7].name == ""


def test_palette_from_file(documents):
    """Check the palette."""
    doc: AIDocument = documents.cmyk_rectangle
    assert len(doc.setup.palettes) == 1
    p = doc.setup.palettes[0]
    assert p.beginning.top_left_cell_index == 0
    assert p.beginning.selected_index == 0
    cells = p.cells
    assert cells[0].name == "[Registration]"
    assert cells[0].color.cmyk == [
        0.749721467494965,
        0.679194271564484,
        0.670496642589569,
        0.901457190513611,
    ]


def test_name_can_be_given_as_a_string():
    """A name is recognized."""
    pc = PaletteCell()
    pc.append(PaletteCellName.from_string("([name])"))
    assert pc.name == "[name]"


def test_palette_recognizes_names():
    """We recognize names in lines."""
    p: Palette = Palette.from_string("%AI5_BeginPalette\n(color name)\n%AI5_EndPalette")
    assert len(p.children) == 1
    assert p.children[0].name == "color name"
