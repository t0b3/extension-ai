# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests the parser for the hirarchical data e.g. inside of BeginDocumentData.

This is documented in docs/specification_amendments/document_data.md.
"""
from io import BytesIO
import pytest
from inkai.parser.hierarchy_objects import (
    Art,
    Point,
    PointRelToROrigin,
)
from inkai.parser.fileparser import FileParser
from inkai.parser.operators import Operator
from inkai.parser.streamreader import StreamWrapper, enc
from tests.conftest import parse_string

# from inkai.parser.objects.new import ArtDictionary

# from inkai.parser.tokens import tokenize

EXAMPLE_SECTION = """
%_/Array :
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
%_/Dictionary :
%_(Page 2) /UnicodeString (Name) ,
%_/Binary : /ASCII85Decode ,
%!!n*o5t"%.!Y>>3E,p&@6VU]V9OVBQ#LEGU!#bh@!%@n*@:O@t5u:BOz5t"%.zzzz!!)`D!!*'"!!(J"
%5t"%.zzzzzzzzzzz!!!!+A7]gl!!!#s!!!"@@rQI1!!!%=!!!!LGB@eG
%s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!rr<$!~>
%_; (AI12 Document Profile Data) ,
%_;
%_;
"""


def test_match_the_section_to_its_end():
    """Parse the whole section very fast."""
    p = parse_string(
        EXAMPLE_SECTION, lambda str: FileParser.read_many_objects(str, b"")[0]
    )


XML_NODE = """/XMLNode :
2 /Int (xmlnode-nodetype) ,
(SourceGraphic) /UnicodeString (xmlnode-nodevalue) ,
(in) /UnicodeString (xmlnode-nodename) ,
/Array :
; (xmlnode-children) ,
/Dictionary :
; (xmlnode-attributes) ,
;
"""

BOOL_EXAMPLE_FALSE = """/ArtDictionary :
0 /Bool (AI13PatternEnableGuides) ,
;
"""
BOOL_EXAMPLE_TRUE = """/ArtDictionary : 
1 /Bool (AI13PatternEnableGuides) ,
;
"""
# see https://en.wikipedia.org/wiki/Ascii85#Adobe_version
BINARY_EXAMPLE = """/Binary : /ASCII85Decode ,
%87cURD]i,"Ebo80~>
;
"""

BINARY_EXAMPLE_WITH_NEWLINE = """/Binary : /ASCII85Decode ,
%87cURD]i,
%"Ebo80~>
;
"""


ARRAY_EXAMPLE = """ /Array :
/XMLNode : 1 /Int (xmlnode-nodetype) ,  /String (xmlnode-nodevalue) , (variables) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ;
 ,
/XMLNode : 1 /Int (xmlnode-nodetype) ,  /String (xmlnode-nodevalue) , (v:sampleDataSets) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : /XMLNode : 2 /Int (xmlnode-nodetype) , (&amp;ns_custom;) /String (xmlnode-nodevalue) , (xmlns) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ; (xmlns) , /XMLNode : 2 /Int (xmlnode-nodetype) , (&amp;ns_vars;) /String (xmlnode-nodevalue) , (xmlns:v) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ; (xmlns:v) , ; (xmlnode-attributes) , ;
 , ;
"""

ARTSTYLE_1_CONTENT = """/KnownStyle :
([Default]) /Name ,
/SimpleStyle :
0 O
0 0 0 0 1 1 1 Xa
0 R
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0 0 0 XA
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
1 1 Xd
/Paint ;
 /Def ;
"""

WEIRD_INT_EXAMPLE = """/ArtDictionary :
/Dictionary :
/Dictionary :
/-1923857531 /Int (Transform:random seed) ,
/1923857531 /Int (Transform:another seed) ,
; (5) ,
/Dictionary :
(Adobe Transform) /String (5) ,
; (Adobe_Instance_Data_ID_Map) ,
; (Adobe_AI9_Execution_Instance_Data) ,
;"""

ART_EMBEDDED_IN_DICT = """/ArtDictionary :
/Dictionary :
323 /Real (fpp0X) ,
-362 /Real (fpp0Y) ,
; (VectorScribeDC) ,
X=
1 w
323 -362 m
X+
 (DCNCP) ,
X=
0.42356 0 0.121279 0 0.521569 0.866667 0.901961 XA
1 An
X+
 (DCFP) ,
;
"""


MATCH = object()


@pytest.mark.parametrize(
    "string,expected_value",
    [
        (XML_NODE, ("in", "SourceGraphic")),
        ("/Array : ;", []),
        ("/Array : 1 /Int , /Dictionary : ; , ;", [1, {"_type": "Dictionary"}]),
        ("/Array : /String ;", [""]),
        ("/Array : (hello) /String ;", ["hello"]),
        ("/Array : (hel lo) /String ;", ["hel lo"]),
        ("/Array : (hel \n/lo) /String ;", ["hel \n/lo"]),
        ("/Array : (hello \n) /String ;", ["hello \n"]),
        ("/Array : (hello \n\n\nWorld) /String ;", ["hello \n\n\nWorld"]),
        # At this point, AI deviates from PostScript syntax
        ("/Array : (hello)\n( World) /String ;", ["hello World"]),
        ("/Array : (hel(lo) (World) 123) /String ;", ["hel(lo) (World) 123"]),
        # escaping the newline
        ("/Array : (hel\\\nlo) /String ;", ["hello"]),
        # newline at the end of the string
        ("/Array : (hello\n) /String ;", ["hello\n"]),
        ("/Array : (hello\\n) /String ;", ["hello\n"]),
        # octal data
        (
            "/Array : (sp\\344ter gr\\374n m\\366glich) /String ;",
            ["später grün möglich"],
        ),
        (
            "/Array : (\0533\53\053) /String ;",
            ["+3++"],
        ),
        (r"/Array : (\(hello World\)) /String ;", ["(hello World)"]),
        ("/Array : (27.1.1) /String ;", ["27.1.1"]),
        ("/Array : /UnicodeString ;", [""]),
        (
            "/Array : 197.85549500175 231 110.544897207401 77 0 0 /RealMatrix ;",
            [(197.85549500175, 231.0, 110.544897207401, 77.0, 0.0, 0.0)],
        ),
        (
            "/Array : 197.85549500175 231 \n 110.544897207401 77 0 0 /RealMatrix ;",
            [(197.85549500175, 231.0, 110.544897207401, 77.0, 0.0, 0.0)],
        ),
        ("/Array : 197.85549500175 /Real ;", [197.85549500175]),
        (
            "/Dictionary : /NotRecorded , ;",
            {"Recorded": False, "_type": "Dictionary"},
        ),
        (
            "/Dictionary : /Recorded , ;",
            {"Recorded": True, "_type": "Dictionary"},
        ),
        ("/Dictionary : ;", {"_type": "Dictionary"}),
        # (ARRAY_EXAMPLE, MATCH),
        ("/Array : 0 -500 /RealPoint ;", [Point(0, -500)]),
        ("/Array : 10 -50 /RealPointRelToROrigin ;", [PointRelToROrigin(10, -50)]),
        (
            "/Document : /Dictionary : /NotRecorded , 1 /Bool (SnapWhileScaling) , ; /NotRecorded , ;",
            {
                "_type": "Document",
                "NotRecorded": {
                    "_type": "Dictionary",
                    "Recorded": False,
                    "SnapWhileScaling": True,
                },
                "Recorded": {},
            },
        ),
    ],
)
def test_parse_document_data(string, expected_value):
    """Parse the contents of the document data section."""
    result = parse_string(string, lambda str: FileParser.read_many_objects(str, b"")[0])

    assert result == expected_value or expected_value is MATCH, result


ART_EXAMPLE = """
%_/ArtDictionary :
%_123 /Int (test) ,
%_;
"""

RECT_ART = """
%_/ArtDictionary :
%_/Dictionary :
%_/Dictionary :
%_211 /Real (ai::Rectangle::Height) ,
%_8093.5 /Real (ai::Rectangle::CenterY) ,
%_0 /Real (ai::Rectangle::Angle) ,
%_180 /Real (ai::Rectangle::Width) ,
%_8188 /Real (ai::Rectangle::CenterX) ,
%_0 /Real (ai::Rectangle::CornerRadius::1) ,
%_3 /Int (ai::Rectangle::InitialQuadrant) ,
%_0 /Real (ai::Rectangle::CornerRadius::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::0) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::1) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::1) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::2) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::2) ,
%_0 /Real (ai::Rectangle::CornerRadius::2) ,
%_1 /Bool (ai::Rectangle::Clockwise) ,
%_(Invalid) /UnicodeString (ai::Rectangle::RoundingType::3) ,
%_0 /Real (ai::Rectangle::CornerRadius::3) ,
%_(Invalid) /UnicodeString (ai::Rectangle::CornerType::3) ,
%_; (ai::LiveShape::Params) ,
%_(ai::Rectangle) /UnicodeString (ai::LiveShape::HandlerName) ,
%_; (ai::LiveShape) ,
%_;
%_
"""


@pytest.mark.parametrize(
    "string,expected_value",
    [
        (ART_EXAMPLE, {"test": 123, "_type": "ArtDictionary"}),
        ("/ArtDictionary : ;", {"_type": "ArtDictionary"}),
        (
            "/ArtDictionary : /XMLUID : (Sublayer_1_x5F_1) ; (AI10_ArtUID) , ;",
            {"AI10_ArtUID": "Sublayer_1_x5F_1", "_type": "ArtDictionary"},
        ),
        (
            RECT_ART,
            {
                "_type": "ArtDictionary",
                "ai::LiveShape": {
                    "_type": "Dictionary",
                    "ai::LiveShape::Params": {
                        "_type": "Dictionary",
                        "ai::Rectangle::Height": 211.0,
                        "ai::Rectangle::CenterY": 8093.5,
                        "ai::Rectangle::Angle": 0.0,
                        "ai::Rectangle::Width": 180.0,
                        "ai::Rectangle::CenterX": 8188.0,
                        "ai::Rectangle::CornerRadius::1": 0.0,
                        "ai::Rectangle::InitialQuadrant": 3,
                        "ai::Rectangle::CornerRadius::0": 0.0,
                        "ai::Rectangle::CornerType::0": "Invalid",
                        "ai::Rectangle::RoundingType::0": "Invalid",
                        "ai::Rectangle::CornerType::1": "Invalid",
                        "ai::Rectangle::RoundingType::1": "Invalid",
                        "ai::Rectangle::CornerType::2": "Invalid",
                        "ai::Rectangle::RoundingType::2": "Invalid",
                        "ai::Rectangle::CornerRadius::2": 0.0,
                        "ai::Rectangle::Clockwise": True,
                        "ai::Rectangle::RoundingType::3": "Invalid",
                        "ai::Rectangle::CornerRadius::3": 0.0,
                        "ai::Rectangle::CornerType::3": "Invalid",
                    },
                    "ai::LiveShape::HandlerName": "ai::Rectangle",
                },
            },
        ),
        (
            BOOL_EXAMPLE_TRUE,
            {"AI13PatternEnableGuides": True, "_type": "ArtDictionary"},
        ),
        (
            BOOL_EXAMPLE_FALSE,
            {"AI13PatternEnableGuides": False, "_type": "ArtDictionary"},
        ),
        (
            WEIRD_INT_EXAMPLE,
            {
                "Adobe_AI9_Execution_Instance_Data": {
                    "5": {
                        "Transform:random seed": -1923857531,
                        "Transform:another seed": 1923857531,
                        "_type": "Dictionary",
                    },
                    "Adobe_Instance_Data_ID_Map": {
                        "5": "Adobe " "Transform",
                        "_type": "Dictionary",
                    },
                    "_type": "Dictionary",
                },
                "_type": "ArtDictionary",
            },
        ),
    ],
)
def test_parse_art_dict(string, expected_value):
    """Parse the contents of the art dictionary."""
    result = parse_string(string, lambda str: FileParser.read_many_objects(str, b"")[0])
    assert result == expected_value or expected_value is MATCH


FULL_STYLE_FROM_RECTANGLE_STYLE = """0 O
0 0 0 0 1 1 1 Xa
0 R
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0 0 0 XA
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
1 1 Xd"""
FULL_STYLE_FROM_RECTANGLE = f"""/KnownStyle :
([Default]) /Name ,
/SimpleStyle :
{FULL_STYLE_FROM_RECTANGLE_STYLE} /Paint ;
 /Def ;
"""


@pytest.mark.parametrize(
    "string,expected_value",
    [
        (
            "/KnownStyle : (Abgerundete Ecken 2 Pt.) /Name ;",
            {"Name": "Abgerundete Ecken 2 Pt.", "_type": "KnownStyle", "Def": None},
        ),
        (
            FULL_STYLE_FROM_RECTANGLE,
            {
                "_type": "KnownStyle",
                "Name": "[Default]",
                "Def": {
                    "_type": "SimpleStyle",
                    "Paint": " ".join(FULL_STYLE_FROM_RECTANGLE_STYLE.split("\n")),
                },
            },
        ),
    ],
)
def test_parse_art_style(string, expected_value):
    """Parse the contents of the art dictionary."""
    result = parse_string(string, lambda str: FileParser.read_many_objects(str, b"")[0])
    print(result)
    if result.Def:
        paint = result.Def.Paint
        assert len(paint) == 12
        result.Def.Paint = (
            b" ".join(
                [b" ".join(c.tokens) + b" " + enc(c.command) for c in result.Def.Paint]
            )
        ).decode("latin-1")

    assert result == expected_value


@pytest.mark.parametrize(
    "string,tokens",
    [
        (b"", []),
        (
            b"197.85549500175 231 110.544897207401 77 0 0 /RealMatrix",
            [
                b"197.85549500175",
                b"231",
                b"110.544897207401",
                b"77",
                b"0",
                b"0",
                b"/RealMatrix",
            ],
        ),
        #        ("%_/Document :\n", ["/Document", ":"]), # not for the tokenizer
        #        ("%binary-stuff\n", ["binary-stuff"]), # not for the tokenizer
        (
            b"(U.S. Web Coated \\(SWOP\\) v2) /UnicodeString (/attributes/cm.profile) ,",
            [
                b"U.S. Web Coated (SWOP) v2",
                b"/UnicodeString",
                b"/attributes/cm.profile",
                b",",
            ],
        ),  # line 7345 in document_data.txt
    ],
)
def test_tokenize(string, tokens):
    """Test the tokenization."""
    assert list(StreamWrapper(BytesIO(string)).yield_tokens()) == tokens


def test_can_decode_binary_data(data):
    """See if we can decode the example data."""
    import base64

    b = data.grammar.document_data_binary.read()
    if b.startswith(b"%"):
        b = b[1:]
    b = b.replace(b"\n%", b"\n").replace(b"\r%", b"\r").strip()
    decoded = base64.a85decode(b, foldspaces=True, adobe=True, ignorechars=b" \r\n\t")
    print(decoded[:10])


@pytest.mark.parametrize(
    "document,expected",
    [
        (BINARY_EXAMPLE, b"Hello World!"),
        (BINARY_EXAMPLE_WITH_NEWLINE, b"Hello World!"),
    ],
)
def test_issue_36_decode_binary_data(document, expected):
    """Defer decoding of binary data to speed up the parsing.

    See https://gitlab.com/inkscape/extras/extension-ai/-/issues/36
    """
    binary = parse_string(
        document, lambda str: FileParser.read_many_objects(str, b"")[0]
    )
    assert binary.decode() == expected


LAYER_DEFINITION = """
%_/ArtDictionary :
%_/XMLUID : (Layer_1) ; (AI10_ArtUID) ,
%_;
%_
"""


def test_section():
    """Test the section if it returns the right result."""
    d = parse_string(
        LAYER_DEFINITION, lambda str: FileParser.read_many_objects(str, b"")[0]
    )
    print(d)
    assert "AI10_ArtUID" in d
    assert d["AI10_ArtUID"] == "Layer_1"


TEXT_DOCUMENT = """/AI11TextDocument : /ASCII85Decode ,
%87cURD]i,
%"Ebo80~>
7231 7651 /RulerOrigin ,
;
/AI11UndoFreeTextDocument : /ASCII85Decode ,
%87cURD]i,
%"Ebo80~>
;
"""


def test_begin_text_document_content():
    """Check the content of the text document parser."""
    text_documents = parse_string(
        TEXT_DOCUMENT, lambda str: FileParser.read_many_objects(str, b"")
    )
    print(text_documents)
    assert text_documents == [
        {
            "_type": "AI11TextDocument",
            "Data": b"Hello World!",
            "RulerOrigin": Point(x=7231, y=7651),
        },
        {"_type": "AI11UndoFreeTextDocument", "Data": b"Hello World!"},
    ]


def test_parse_embedded_art():
    """Some Dictionaries have embedded art content"""
    result = parse_string(
        ART_EMBEDDED_IN_DICT, lambda str: FileParser.read_many_objects(str, b"")[0]
    )
    assert isinstance(result["DCNCP"], Art)
    assert len(result["DCNCP"].Data) == 2
    assert isinstance(result["DCNCP"].Data[0], Operator)
