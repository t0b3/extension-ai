# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from io import BytesIO
import pytest
from inkai.parser.fileparser import FileParser
from tests.conftest import parse_string
from inkai.parser.streamreader import StreamWrapper, wsp_regex


def parse_section(data: str):
    return parse_string(data, FileParser.read_many_objects)[0]


@pytest.mark.parametrize(
    "data, wspmatch",
    [
        ("a b", " "),
        ("a  b", "  "),
        ("a \rb", " \r"),
        ("a \nb", " \n"),
        ("a \r\nb", " \r\n"),
        ("a\r\rb", "\r"),
        ("a\r\n\rb", "\r\n"),
        ("a\r b", "\r"),
    ],
)
def test_wsp_behavior(data, wspmatch):
    wrap = StreamWrapper(BytesIO(data.encode()))
    assert wrap.read_token() == b"a"
    assert wrap.read() == data[len(wspmatch) + 1 :].encode()


@pytest.mark.parametrize(
    "str, result",
    [
        ("(This is a string)", "This is a string"),
        (
            "(Strings may contain newlines\n and such.)",
            "Strings may contain newlines\n and such.",
        ),
        (
            "(Strings may contain special characters *!&}^% and\n balanced parentheses ( ) (and so on).)",
            "Strings may contain special characters *!&}^% and\n balanced parentheses ( ) (and so on).",
        ),
        ("()", ""),
        (
            "(These \\\ntwo strings \\\nare the same.)",
            "These two strings are the same.",
        ),
        ("(So does this one.\\n)", "So does this one.\n"),
        ("(\\05353a\\053b\\53)", "++a+b+"),
        ("(()(()()))", "()(()())"),
        (r"(\), \(, (), \w )", "), (, (), w "),
    ],
)
def test_parse_str(str, result):
    actual = parse_string(str[1:], StreamWrapper.read_string).decode("latin-1")
    assert actual == result


def test_parse_section():
    data = """%AI5_BeginGradient: (Fading Sky)
    (Fading Sky) 0 2 Bd
    [
    0.8
    0.4
    0
    <
    00
    >
    1 %_Br
    [
    0.8 0.4 0 0.000031 1 0 6 50 100 %_BS
    %_0.8 0.4 0 0.000031 1 0 6 50 100 Bs
    0.8 0.4 0 0 1 1 6 50 0 %_BS
    %_0.8 0.4 0 0 1 1 6 50 0 Bs
    BD
    %AI5_EndGradient"""
    result = parse_section(data)
    assert len(result.children) == 7
    assert result.arguments == [b"Fading Sky"]


def test_parse_unknown_operator():
    data = """%AI5_BeginGradient: (Fading Sky)
(Fading Sky) 0 2 Bd
%_0.8 0.4 0 0 1 1 6 50 0 Bello
%AI5_EndGradient"""
    with pytest.warns():
        result = parse_section(data)
    assert len(result.children) == 1
    assert result.arguments == [b"Fading Sky"]


def test_parse_xp():
    data = """%_0 Ae
%_(Adobe Path Blends) 1 0 92 XP
%_%444342410100000000008040FFFF7F7F0A000000655A1A41430000000000
%_%0000000000000100000002000000410000000000000000000000FFFFFFFF
%_%FFFFFFFF0000000042000000000000000000803FFFFFFFFFFFFFFFFF0A00
%_%0000
%_/ArtDictionary :
%_(0.076272) /String (BBAccumRotation) ,
%_;"""
    result = parse_string(data, lambda str: FileParser.read_many_objects(str, b""))
    assert len(result) == 3
    assert result[1].command == "XP"
    assert b"0000" in result[1].tokens[4]


@pytest.mark.parametrize("term", ("\r", "\n", "\r\n"))
def test_line_terminators(term):
    data = r"""%AI55J_Tsume: None
%AI3_BeginEncoding: _MyriadPro-Regular MyriadPro-Regular
[/_MyriadPro-Regular/MyriadPro-Regular 0 0 1 TZ
%AI3_EndEncoding AdobeType
%AI55J_Tsume: None
%AI3_BeginEncoding: _TimesNewRomanPSMT TimesNewRomanPSMT
[/_TimesNewRomanPSMT/TimesNewRomanPSMT 0 0 0 TZ
%AI3_EndEncoding TrueType
/Binary : /ASCII85Decode ,
%87cURD]i,
%"Ebo80~>
;"""
    data = data.replace("\n", term)

    result = parse_string(data, lambda str: FileParser.read_many_objects(str, b""))

    assert len(result) == 5
