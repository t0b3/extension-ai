# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the header commends (key-value mappings) at the start of the AI document."""
import pytest
from io import StringIO, BytesIO
from inkai.parser.fileparser import FileParser
from inkai.parser.streamreader import StreamWrapper


def read_header_comments(input: str) -> dict:
    file = StreamWrapper(BytesIO(input.encode("latin-1")))
    file.skip(b"%!PS-Adobe-3.0")
    return FileParser.read_header_comments(file)


SMALL_EXAMPLE = """%!PS-Adobe-3.0 
%%Creator: Adobe Illustrator(R) 24.0
%%EndComments
NEVER REACHED
"""

BIG_EXAMPLE = r"""%!PS-Adobe-3.0 
%%Creator: Adobe Illustrator(R) 24.0
%%AI8_CreatorVersion: 27.1.1
%%For: (Christopher Rogers) ()
%%Title: (cmyk_rectangle.ai)
%%CreationDate: 3/13/2023 7:57 PM
%%Canvassize: 16383
%%BoundingBox: 202 412 384 625
%%HiResBoundingBox: 202.5 412.5 383.5 624.5
%%DocumentProcessColors: Cyan Magenta Yellow Black
%AI5_FileFormat 14.0
%AI12_BuildNumber: 196
%AI3_ColorUsage: Color
%AI7_ImageSettings: 0
%%RGBProcessColor: 0.000136697562994 0.000626976019703 0.000423457007855 ([Registration])
%AI3_Cropmarks: 0 0 595.275573730469 841.889770507813
%AI3_TemplateBox: 296.5 420.5 296.5 420.5
%AI3_TileBox: 0 -0.03021240234375 595.320007324219 841.889770507813
%AI3_DocumentPreview: None
%AI5_ArtSize: 14400 14400
%AI5_RulerUnits: 1
%AI24_LargeCanvasScale: 1
%AI9_ColorModel: 1
%AI5_ArtFlags: 0 0 0 1 0 0 1 0 0
%AI5_TargetResolution: 800
%AI5_NumLayers: 1
%AI17_Begin_Content_if_version_gt:24 4
%AI10_OpenToVie: -646 893 1 0 0 0 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1
%AI17_Alternate_Content
%AI9_OpenToView: -646 893 1 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1
%AI17_End_Versioned_Content
%AI5_OpenViewLayers: 7
%AI17_Begin_Content_if_version_gt:24 4
%AI17_Alternate_Content
%AI17_End_Versioned_Content
%%DocumentFiles:C:\file1.jpg
%%+C:\file2.jpg
%%PageOrigin:-663 -120
%AI7_GridSettings: 72 8 72 8 1 0 0.800000011920929 0.800000011920929 0.800000011920929 0.899999976158142 0.899999976158142 0.899999976158142
%AI9_Flatten: 1
%AI12_CMSettings: 00.MS
%%EndComments
NEVER REACHED
"""


@pytest.mark.parametrize("input", [SMALL_EXAMPLE, BIG_EXAMPLE])
def test_comments_consume_to_their_end(input):
    file = StreamWrapper(BytesIO(input.encode("latin-1")))
    file.skip(b"%!PS-Adobe-3.0")
    comments = FileParser.read_header_comments(file)
    assert file.read() == b"NEVER REACHED\n"


@pytest.mark.parametrize(
    "key,value",
    [
        ("Creator", "Adobe Illustrator(R) 24.0"),
        ("AI8_CreatorVersion", "27.1.1"),
        ("For", "Christopher Rogers "),
        ("Title", "cmyk_rectangle.ai"),
        ("CreationDate", "3/13/2023 7:57 PM"),
        ("Canvassize", "16383"),
        ("BoundingBox", "202 412 384 625"),
        ("HiResBoundingBox", "202.5 412.5 383.5 624.5"),
        ("DocumentProcessColors", "Cyan Magenta Yellow Black"),
        ("AI5_FileFormat", "14.0"),
        ("AI12_BuildNumber", "196"),
        ("AI3_ColorUsage", "Color"),
        ("AI7_ImageSettings", "0"),
        (
            "RGBProcessColor",
            "0.000136697562994 0.000626976019703 0.000423457007855 [Registration]",
        ),
        ("AI3_Cropmarks", "0 0 595.275573730469 841.889770507813"),
        ("AI3_TemplateBox", "296.5 420.5 296.5 420.5"),
        ("AI3_TileBox", "0 -0.03021240234375 595.320007324219 841.889770507813"),
        ("AI3_DocumentPreview", "None"),
        ("AI5_ArtSize", "14400 14400"),
        ("AI5_RulerUnits", "1"),
        ("AI24_LargeCanvasScale", "1"),
        ("AI9_ColorModel", "1"),
        ("AI5_ArtFlags", "0 0 0 1 0 0 1 0 0"),
        ("AI5_TargetResolution", "800"),
        ("AI5_NumLayers", "1"),
        (
            "AI10_OpenToVie",
            "-646 893 1 0 0 0 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1",
        ),
        # ("AI9_OpenToView", "-646 893 1 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1"),
        ("AI5_OpenViewLayers", "7"),
        ("PageOrigin", "-663 -120"),
        (
            "AI7_GridSettings",
            "72 8 72 8 1 0 0.800000011920929 0.800000011920929 0.800000011920929 0.899999976158142 0.899999976158142 0.899999976158142",
        ),
        ("AI9_Flatten", "1"),
        ("AI12_CMSettings", "00.MS"),
        ("DocumentFiles", r"C:\file1.jpg C:\file2.jpg"),
    ],
)
def test_raw_values_can_be_retrieved(key, value):
    """See if we can see the raw values."""
    comments = read_header_comments(BIG_EXAMPLE)
    assert key in comments
    assert " ".join(comments[key]) == value


DocumentNeededResources = """procset Adobe_packedarray 2.0 0
procset Adobe_cmykcolor 1.1 0
procset Adobe_cshow 1.1 0
procset Adobe_customcolor 1.0 0
procset Adobe_typography_AI3 1.0 1
procset Adobe_pattern_AI3 1.0 0
procset Adobe_Illustrator_AI3 1.0 1"""


def test_lines_with_plus(documents):
    """Header comments might have lines with a + to continue them.

    Example:

        %%DocumentNeededResources: procset Adobe_packedarray 2.0 0
        %%+ procset Adobe_cmykcolor 1.1 0
        %%+ procset Adobe_cshow 1.1 0
        %%+ procset Adobe_customcolor 1.0 0
        %%+ procset Adobe_typography_AI3 1.0 1
        %%+ procset Adobe_pattern_AI3 1.0 0
        %%+ procset Adobe_Illustrator_AI3 1.0 1
    """
    print(documents.simple_v3)
    comments = " ".join(documents.simple_v3.header["DocumentNeededResources"])
    assert comments == "  ".join(DocumentNeededResources.splitlines())


def test_unknown_lines_are_children():
    """Make sure that we do not forget lines and not tell people."""
    comments = read_header_comments(
        "%!PS-Adobe-3.0 \n%%Creator: Adobe Illustrator(R) 24.0\nproblematic\n%%EndComments\r\n"
    )
    assert "problematic" in comments["Creator"]
