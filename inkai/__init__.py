# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""inkai allows you to parse Adobe Illustrator files and convert them to SVG.

The parsed object structure allows you to inspect the contents of the AI files.
"""
from __future__ import annotations

# Do not place inkai package imports here!
# They will be imported before __main__ and may result in
# import errors.
__all__ = ["extract", "parse", "to_svg"]
try:
    from .parser.document import AIDocument

    __all__.append("AIDocument")
except ImportError:
    pass


class ExtractionError(ValueError):
    """Could not extract the data."""


def extract(path: str) -> bytes:
    """Extract the AI data from a file given by a path.

    This allows to inspect the AI document as a string.

    If the data could not be extracted, we raise an ExtractionError.
    """
    from .extraction import extract_ai_privatedata

    with open(path, "rb") as f:
        data = extract_ai_privatedata(f.read())
        if data is None:
            raise ExtractionError("Could not extract AI data from document.")
        return data


def parse(path: str) -> AIDocument:
    """Create a parsed AIDocument from the path.

    This allows to inspect the AI document as an object structure.

    If the data could not be extracted, we raise an ExtractionError.
    """
    from .parser.document import AIDocument

    content = extract(path)
    return AIDocument.from_string(content)


def to_svg(path: str) -> bytes:
    """Convert the AI document to an SVG string.

    This allows to view the AI document as an SVG file.

    If the data could not be extracted, we raise an ExtractionError.
    """
    from ._svg.convert import ai2svg

    return ai2svg(parse(path)).tostring()
