# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This holds the state of the interpretation on the SVG side.

The SVGBuilder is the goto object for building the SVG.
The ModificationFactories submit modifications.
These modifications modify the SVG using the SVGBuilder.
"""
from inkex import BaseElement, SvgDocumentElement
from inkex.base import SvgOutputMixin


from lxml import etree
from typing import List, Optional
from ..parser.util import TypeAlias

SVGElement: TypeAlias = etree.ElementBase


class ImpossibleModificationException(ValueError):
    """This modification is not possible because of the state the SVGBuilder is in."""


class SVGBuilder:
    """This object contains commonly used methods to modify the SVG.
    This follows the builder pattern,
    see https://en.wikipedia.org/wiki/Builder_pattern.
    """

    def __init__(self):
        """Create a new SVG state."""
        self._result: SVGElement = SvgOutputMixin.get_template(width=0, height=0)
        self._parents: List[BaseElement] = [self.svg]

    def append_child(self, child: BaseElement):
        """Add the child to the current container (SVG, Layer, Group, ...)."""
        self.current_parent.append(child)

    def append_def(self, definition: BaseElement):
        """Add the child to the current container (SVG, Layer, Group, ...)."""
        self.svg.defs.append(definition)

    def replace_last_child_with(self, child: BaseElement):
        """Replace the last child with another element."""
        self.last_child.replace_with(child)

    def enter(self, container: BaseElement):
        """Set the container as the current container to add elements into."""
        self._parents.append(container)

    def exit(self):
        """Leave the current parent and go one level higher."""
        self.append_child(self._parents.pop())

    def has_last_child(self) -> bool:
        """Whether there is a child."""
        return len(self._parents[-1]) != 0

    @property
    def last_child(self) -> BaseElement:
        """Expose the last child for modification."""
        if not self.has_last_child():
            raise ImpossibleModificationException("No child has been added.")
        return self._parents[-1][-1]

    @property
    def current_parent(self) -> BaseElement:
        """Return the current parent."""
        return self._parents[-1]

    @property
    def svg(self) -> SvgDocumentElement:
        """The SVG root document.
        This is the result of the building process.
        """
        return self._result.getroot()


__all__ = ["SVGBuilder", "SvgDocumentElement", "SVGElement"]
