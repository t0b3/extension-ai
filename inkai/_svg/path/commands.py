# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Draw the path."""
from inkai.parser.objects.path import PathOperator, PathRender
from inkai._svg.coordinates import CoordinateTransform
from inkai._svg.interpreter.modification_factory import ModificationFactory
from inkai._svg.interpreter.mapping import on_element
from inkex import paths
from inkai._svg.modification import AddChildAttributes


from typing import Sequence


class PathCommands(ModificationFactory):
    """Convert AI Path Construction Operators for SVG Path commands.
    See AI Spec page 56.
    See https://gitlab.com/inkscape/extras/extension-ai/-/merge_requests/61#note_1384351623
    See https://inkscape.gitlab.io/extensions/documentation/_modules/inkex/paths.html#Path
    Attributes:
    - operator_mapping - a mapping from AI operator to inkex path command
    Equivalence:
        x y m             = x y m
        x y x2 y2 x3 y3 c = x2 y2 x3 y3 v
        x y m               = x y m
        x1 y1 x3 y3 x3 y3 c = x1 y1 x3 y3 y
    """

    def __init__(self, transform: CoordinateTransform):
        """Create path commands in a transformed space."""
        super().__init__()
        self.transform = transform
        self.operator_mapping = {
            "c": paths.Curve,
            "m": paths.Move,
            "l": paths.Line,
            "y": lambda x1, y1, x3, y3: paths.Curve(x1, y1, x3, y3, x3, y3),
            "v": self.add_v_path_command,
        }

    def reset_modification(self):
        """Initialize the PathCommands."""
        self.commands = paths.Path()
        self._last_point: Sequence[float] = (0.0, 0.0)  # never used

    @property  # type: ignore
    def result(self) -> paths.Path:
        """The collected commands."""
        return self.commands

    @on_element(PathOperator)
    def add_as_path_command(self, operator: PathOperator):
        """Populate the list of path commands."""
        points = self.transform.from_drawing(*operator.xy)
        Command = self.operator_mapping[operator.command.lower()]
        self.commands.append(Command(*points))
        self._last_point = points[-2:]

    def add_v_path_command(self, x2, y2, x3, y3):
        """The v path command uses the last point for bezier calculation."""
        return paths.Curve(self._last_point[0], self._last_point[1], x2, y2, x3, y3)

    @on_element(PathRender)
    def close(self, render: PathRender):
        """Close the path if necessary."""
        if render.close:
            self.commands.append(paths.ZoneClose())
        self.submit_modification(AddChildAttributes(d=self.commands))


__all__ = ["PathCommands"]
