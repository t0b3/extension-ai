# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This module contains the mapping of annotated methods for interpretation."""
from collections import defaultdict
from inkai.parser.lazy import AIElement, Category
from inkai._svg.interfaces import (
    ICategoryMap,
    IElementInterpreter,
    IInterpreter,
    ISelector,
)
from ..interfaces import IInterpreterMapElement, IInterpreter, IAIState
from .interpreters import InvalidInterpreter
from .category_map import CategoryMap

from typing import Callable, Dict, List, NamedTuple
from .ai_state.invalid import InvalidAIState

InterpreterFunction = Callable[[AIElement], None]
InterpreterMap = Dict[Category, List[InterpreterFunction]]


def on_element(category: Category):
    """Tag a function for interpretation for a given category of AIElements.
    Example:
        class MyStrategy(Strategy):
            @Strategy.on(AIElement)
            def check_element(self, element: AIElement):
                "Run a method for all AIElements."
    """

    def interpreter_wrapper(
        function: Callable[[AIElement], None]
    ) -> Callable[[AIElement], None]:
        """Tag a function for interpretation."""
        if not hasattr(function, "_interpret"):
            function._interpret = [category]  # type: ignore
        else:
            function._interpret.append(category)  # type: ignore
        return function

    return interpreter_wrapper


def get_on_element_methods(self: object) -> InterpreterMap:
    """Return the mapping for interpretation."""
    result = defaultdict(list)
    names = set()
    for cls in self.__class__.mro():
        for name, func in cls.__dict__.items():
            if name in names or not hasattr(func, "_interpret"):
                continue
            # print(name, func)
            method = getattr(self, name)
            names.add(name)
            for category in func._interpret:
                result[category].append(method)
    return result


InterpretationState = NamedTuple(
    "InterpretationState", [("interpreter", IInterpreter), ("state", IAIState)]
)


class AnnotatedInterpreterMapElement(IInterpreterMapElement):
    """This is an IInterpreterMapElement that maps annotated methods.

    Example:

        class M(AnnotatedInterpreterMapElement):

            @on_element(AIElement)
            def handle(self, ai_element: AIElement):
                ...
    """

    @property
    def interpreter(self) -> IInterpreter:
        """The current interpreter we work with."""
        return self.interpretation_state.interpreter

    @property
    def state(self) -> IAIState:
        """The current interpreter we work with."""
        return self.interpretation_state.state

    def __init__(self) -> None:
        """Create a new mapping of functions."""
        super().__init__()
        self.interpretation_state = InterpretationState(
            InvalidInterpreter(), InvalidAIState()
        )
        self._category_map = CategoryMap()
        for category, methods in get_on_element_methods(self).items():
            for method in methods:
                self._category_map.add_to_category(
                    category, CallableElementInterpreterAdapter(self, method)
                )

    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Interpret an element."""
        self._category_map.interpret_element(selector, ai_element, interpreter)

    def as_category_map(self) -> ICategoryMap:
        """The subset of functionality that is mapping to categories."""
        return self._category_map


class CallableElementInterpreterAdapter(IElementInterpreter):
    """Make a function taking an AIElement into a element interpreter.

    This uses the state pattern to set and restore the state of the interpretation.
    """

    def __init__(
        self, factory: AnnotatedInterpreterMapElement, function: InterpreterFunction
    ) -> None:
        """Create a new adapter for a callable."""
        super().__init__()
        self._function = function
        self._factory = factory

    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Give interpretation to the function."""
        state = self._factory.interpretation_state
        self._factory.interpretation_state = InterpretationState(
            interpreter, selector.ai_state
        )
        try:
            self._function(ai_element)
        finally:
            self._factory.interpretation_state = state

    def __repr__(self) -> str:
        """repr(self)"""
        return f"{self.__class__.__name__}({self._factory}, {self._function})"


__all__ = [
    "InterpreterFunction",
    "InterpreterMap",
    "on_element",
    "get_on_element_methods",
]
