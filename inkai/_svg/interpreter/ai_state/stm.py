# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tracks the state of interpretation and what we are allowed to do through it."""
from __future__ import annotations
from ...interfaces import IAIState
from statemachine import StateMachine, State
from inkai.parser.util import TypeAlias
from enum import Enum, unique
from typing import Dict
from inkai.parser.lazy import Category
from inkai.parser.objects.path import m, PathRender, CompoundPath
from inkai.parser.objects.image import Raster


class AIStateAdapter(IAIState):
    """This is an extra class for the state because the metaclasses mismatch."""

    def __init__(self, stm: AISTM):
        """Show the state for the AISTM."""
        self._stm = stm

    def is_creating_a_raster_image(self) -> bool:
        """Whether we create a raster image at the moment."""
        return self._stm.in_raster.is_active

    def is_creating_a_path(self) -> bool:
        """Whether we create a path at the moment."""
        return (
            self._stm.in_path.is_active or self._stm.in_path_in_compound_path.is_active
        )

    def is_creating_a_compound_path(self) -> bool:
        """Whether we are in a compound path."""
        return (
            self._stm.in_compound_path.is_active
            or self._stm.in_path_in_compound_path.is_active
        )


@unique
class Transition(str, Enum):
    """The named transitions."""

    create_path = "create_path"
    create_raster = "create_raster"
    render_path = "render_path"
    start_compound_path = "start_compound_path"
    close_compound_path = "close_compound_path"


TransitionMap = Dict[Category, Transition]


class AISTM(StateMachine):
    """A state machine for the sub-states of the AI parsing.

    This contains the actual state machine and
    the mapping of changes from AI element to state change.

    This is one place for the definitions.
    The implementation is spread across other modules, too.
    """

    debug = False

    unknown_element = State(initial=True)
    in_path = State()
    in_compound_path = State()
    in_path_in_compound_path = State()
    in_raster = State()

    create_path = unknown_element.to(in_path) | in_compound_path.to(
        in_path_in_compound_path
    )
    create_raster = unknown_element.to(in_raster)
    render_path = (
        in_path.to(unknown_element)
        | in_raster.to(unknown_element)
        | in_path_in_compound_path.to(in_compound_path)
    )
    start_compound_path = unknown_element.to(in_compound_path)
    close_compound_path = in_compound_path.to(unknown_element)

    transitions_before: TransitionMap = {
        m: Transition.create_path,
        Raster: Transition.create_raster,
        CompoundPath: Transition.start_compound_path,
    }

    transitions_after: TransitionMap = {
        PathRender: Transition.render_path,
        CompoundPath: Transition.close_compound_path,
    }

    def do(self, transition: Transition) -> None:
        """Make a transition."""
        if self.debug:
            msg = f"{self.current_state.name} --[{transition.name}]--> "
        self.send(Transition(transition))
        if self.debug:
            print(msg + self.current_state.name)

    @property
    def state(self) -> IAIState:
        """The state just a bit friendlier."""
        return AIStateAdapter(self)


transitions = {event.name for event in AISTM.events}
attributes = {attr for attr in Transition.__dict__ if not attr.startswith("_")}
assert (
    attributes == transitions
), f"{attributes} == {transitions} - the transition names must match! {attributes ^ transitions}"
assert all(
    getattr(Transition, attr) == attr for attr in attributes
), "The names must match!"
del transitions, attributes

__all__ = ["AISTM", "Transition"]
