# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This module contains an IAIState that is static."""
from .invalid import InvalidAIState
from typing import Dict, Any


class StaticState(InvalidAIState):
    """This is a static state and you can set method results."""

    def __init__(self, method_results: Dict[str, Any]):
        """Create a static state with certain results."""
        for name, value in method_results.items():
            if name not in dir(self):
                raise ValueError(
                    f"{self.__class__.__name__} got an unexpected argument {name}."
                )
            setattr(self, name, lambda value=value: value)


__all__ = ["StaticState"]
