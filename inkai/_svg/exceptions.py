# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Exception classes live here."""
from statemachine.exceptions import TransitionNotAllowed
from statemachine.event import Event
from statemachine import State
from ..parser.lazy import AIElement


class IncompleteStatemachine(TransitionNotAllowed):
    """This error happens when the state machine is not complete,

    The transition is not allowed but the document creates one.
    """

    def __init__(self, ai_element: AIElement, event: Event, state: State):
        """Create a new exception."""
        super().__init__(f"{event} from {ai_element}", state)
        self.event = repr(event)
        self.state = state
        self.ai_element = ai_element


__all__ = ["IncompleteStatemachine"]
