# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is a module for all the live shape conversions"""

from .rectangle import LiveRectangle
from ..interpreter.modification_factory import CombinedFactory
from inkai._svg.coordinates import CoordinateTransform


class LiveShapes(CombinedFactory):
    """Combine all the path conversions together."""

    def __init__(self, transform: CoordinateTransform):
        """Create path commands in a transformed space."""
        self.rectangle = LiveRectangle(transform)
        super().__init__(self.rectangle)


__all__ = ["LiveRectangle", "LiveShapes"]
