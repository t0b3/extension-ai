# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later
from __future__ import annotations

from io import BytesIO
import re
from typing import List, Tuple, Callable, Any, Dict, Union, Optional, cast
from dataclasses import dataclass
from lxml import etree
import warnings

from .streamreader import StreamWrapper, dec
from .hierarchy_objects import (
    AI11TextDocument,
    AI11UndoFreeTextDocument,
    Art,
    Binary,
    HierarchyObject,
    Mask,
    Point,
    PointRelToROrigin,
    SimpleStyle,
)
from .consts import (
    Data,
    Encoding,
    operatorlist,
    ObjectContainers,
    Section,
    Operator,
    operator_factory,
    Setup,
    GradientFallback,
    AIDocument,
    Header,
)


class FileParser:
    @staticmethod
    def read_document(stream: StreamWrapper):
        stream.skip(b"%!PS-Adobe-3.0")

        return AIDocument(
            FileParser.read_header_comments(stream),
            FileParser.read_prolog(stream),
            FileParser.read_setup(stream),
            FileParser.read_document_trailer(stream),
        )

    @staticmethod
    def read_header_comments(stream: StreamWrapper):
        parsed = FileParser.read_many_objects(
            stream, [b"%%EndComments", b"%%BeginProlog"]
        )
        result = Header()
        for entry in parsed:
            # Unpack sections in the header
            if isinstance(entry, tuple):
                result[entry[0]] = entry[1]
            elif isinstance(entry, Section):
                for e in entry.children:
                    if isinstance(e, tuple):
                        result[e[0]] = e[1]
            elif isinstance(entry, Data):
                # This belongs to the previous header comment
                result[list(result.keys())[-1]].append(entry)
            else:
                for e in entry:
                    if isinstance(e, tuple):
                        result[e[0]] = e[1]
        return result

    @staticmethod
    def read_prolog(stream: StreamWrapper) -> Section:
        stream.skip_optional(b"%%BeginProlog")
        return FileParser.read_section(stream, b"%%BeginProlog")

    @staticmethod
    def read_setup(stream: StreamWrapper) -> Setup:
        stream.skip(b"%%BeginSetup")
        return cast(Setup, FileParser.read_section(stream, b"%%BeginSetup"))

    @staticmethod
    def read_document_trailer(stream: StreamWrapper):
        return FileParser.read_many_objects(stream, b"%%EOF")

    @staticmethod
    def read_one_header_comment(
        stream: StreamWrapper, token: bytes
    ) -> Tuple[str, List[str]]:
        """Reads one header comment. These are always in a single line"""
        # eg. %%RGBProcessColor: 0 0 0 ([Registration])
        # eg. %AI5_File:
        values = []
        try:
            while (
                not stream.peek_token().startswith(b"%")
            ) or stream.peek_token().startswith(b"%%+"):
                t = stream.read_token()
                if t.startswith(b"%%+"):
                    t = t[3:]
                values.append(dec(t))
        except StopIteration:
            pass

        # values = [dec(t) for t in stream.yield_tokens(stop_on_newline=True)]
        key = dec(token.strip(b"%:"))

        if ":" in key:
            # This is a weird special case where a : can also initiate a new token.
            # Let's see if this comes up more often. TODO
            key, extraval = key.split(":", 1)
            values = [extraval] + values
        if "Begin" in key or "End" in key:
            # this should not happen
            warnings.warn(f"Encountered {key}, but parsed as header comment.")
        return (key, values)

    @staticmethod
    def read_one_object(
        stream: StreamWrapper,
        token: bytes,
        end_section: Optional[Union[bytes, List[bytes]]],
    ) -> Optional[Any]:
        """Reads one object and returns."""
        if token in sections:
            return sections[token].parse_method(stream, token)
        if token.startswith(b"%") and not token.startswith(b"%_"):
            return FileParser.read_one_header_comment(stream, token)
        if token.startswith(b"/"):
            container = DataHierarchyParser.read_container(stream, token)
            if container is not None:
                return container
        if (
            isinstance(end_section, list) and token in end_section
        ) or token == end_section:
            return None
        return FileParser.read_operator(stream, end_section, token)

    @staticmethod
    def read_many_objects(
        stream: StreamWrapper, end_section: Optional[Union[bytes, List[bytes]]] = None
    ) -> List[Any]:
        """Reads many objects and returns a list of them.
        if  end_section or EOF is encountered, return the list"""
        result: List[Any] = []
        for token in stream.yield_tokens():
            if (
                isinstance(end_section, list) and token in end_section
            ) or token == end_section:
                return result
            else:
                a = FileParser.read_one_object(stream, token, end_section)
                if a is not None:
                    result.append(a)
        return result

    @staticmethod
    def read_section(stream: StreamWrapper, section_start: bytes) -> Section:
        """Reads a section. The start token (section_start) has been read,
        but not more -> read parameters first, then contents"""
        type = (
            sections[section_start].datatype if section_start in sections else Section
        )
        return type(
            section_start,
            # Arguments are the remainder of the line, if any.
            list(stream.yield_tokens(True))
            if not stream.last_was_line_terminator
            else [],
            FileParser.read_many_objects(
                stream,
                sections[section_start].end_section
                if section_start in sections
                else b"",
            ),
        )

    @staticmethod
    def read_operator(
        stream: StreamWrapper,
        terminator: Optional[Union[bytes, List[bytes]]] = None,
        first_token=Optional[bytes],
    ) -> Optional[Operator]:
        """Read one operator from stream. If we encounter terminator, return."""
        if first_token in operatorlist:
            return operator_factory(first_token, [])
        arguments: List[Union[bytes, Data]] = [first_token] if first_token else []
        for word in stream.yield_tokens():
            if word in operatorlist:
                # special case, additional data after operator
                if word == b"XP":
                    data = b""
                    # Consume following tokens starting with %
                    while stream.peek_token().startswith(b"%"):
                        data += stream.read_token()[1:]
                    arguments.append(data)

                return operator_factory(word, arguments)
            if (
                isinstance(terminator, list) and word in terminator
            ) or word == terminator:
                stream.seek(stream.tell() - len(word))
                warnings.warn(
                    "Tried to parse operator, but we didn't find any known operator. Skipped remainder of the section. Bad contents: "
                    + str(b" ".join(i for i in arguments if isinstance(i, bytes)))
                )
                return None
            if word == b"%%BeginData:":
                # The operator XI can be hidden inside BeginData, see p. 46 of Postscript DSC
                res = FileParser.read_data(stream)
                header, data = res.raw.strip().split(maxsplit=1)
                res.raw = data
                arguments.append(res)
                return operator_factory(header, arguments)

            arguments.append(word)
        return None

    @staticmethod
    def read_data(stream: StreamWrapper, section_start=b"%%BeginData:") -> Data:
        # How many bytes?
        arguments = list(stream.yield_tokens(True))
        length = int(arguments[0])
        # Hex, ASCII or Binary
        type = dec(arguments[1]) if len(arguments) > 1 else "Binary"
        # Bytes or lines
        is_bytes = dec(arguments[2]) == "Bytes" if len(arguments) == 3 else True
        if is_bytes:
            # -1 as we've read the space/newline after the length
            if length > 0:
                data = stream.read(length - 1)
        else:
            tmp = bytearray()
            for _ in range(length):
                tmp.extend(stream.read_line())
            data = bytes(tmp)
        # Next we should read %%EndData, but there might be \xFF bytes in between
        # Continue reading until we encounter a token with %%EndData.
        enddata_pattern = re.compile(b"%%EndData")
        pos = enddata_pattern.search(stream.data, stream.tell())
        if not pos:
            raise ValueError("EOF while reading %%BeginData")
        additional = stream.read(pos.start() - stream.tell())
        stream.skip(b"%%EndData")

        return Data(length, type, data, additional)

    @staticmethod
    def skip_section(stream: StreamWrapper, section_start=b""):
        # We just read until we read this section's end token, keeping track of
        # indentation level
        indent = 1
        for token in stream.yield_tokens():
            if token == sections[section_start].end_section:
                indent -= 1
            elif token == section_start:
                indent += 1
            if indent == 0:
                return Section(section_start)

    @staticmethod
    def read_gradient_fallback(stream: StreamWrapper, section_start=b"["):
        for token in stream.yield_tokens():
            if token == b"[":
                # TODO
                return GradientFallback()

    @staticmethod
    def read_versioned_content(
        stream: StreamWrapper, section_start=b"%AI17_Begin_Content_if_version_gt:24"
    ):
        (major, minor) = 24, int(next(stream.yield_tokens(True)))  # read until eol
        data = FileParser.read_many_objects(stream, b"%AI17_Alternate_Content")
        old_data = FileParser.read_many_objects(stream, b"%AI17_End_Versioned_Content")
        return data if (major, minor) > (24, 0) else old_data

    @staticmethod
    def read_list_section(
        stream: StreamWrapper, section_start=b"%AI24_BeginSymbolList"
    ):
        symbols: List[bytes] = []
        for token in stream.yield_tokens():
            if token == sections[section_start].end_section:
                return Section(section_start, symbols)
            symbols.append(token)

    @staticmethod
    def read_encoding(stream: StreamWrapper, section_start=b"%AI3_BeginEncoding"):
        result = cast(Encoding, FileParser.read_section(stream, section_start))
        result.fonttype = stream.read_token()
        return result


@dataclass
class SectionDesc:
    end_section: bytes
    datatype: type = Section
    parse_method: Callable = FileParser.read_section


sections = {
    b"%%BeginProlog": SectionDesc(b"%%EndProlog"),
    b"%%BeginSetup": SectionDesc(b"%%EndSetup", Setup),
    b"%%BeginProcSet:": SectionDesc(
        b"%%EndProcSet", parse_method=FileParser.skip_section
    ),
    b"%%BeginResource:": SectionDesc(
        b"%%EndResource", parse_method=FileParser.skip_section
    ),
    b"%%BeginData:": SectionDesc(b"%%EndData", parse_method=FileParser.read_data),
    b"%%PageTrailer": SectionDesc(
        b"%%Trailer", parse_method=FileParser.read_list_section
    ),
    # b"[": SectionDesc(b"[", parse_method=FileParser.skip_section),
    b"%AI3_BeginEncoding:": SectionDesc(
        b"%AI3_EndEncoding", Encoding, parse_method=FileParser.read_encoding
    ),
    b"%AI3_BeginPattern:": SectionDesc(b"%AI3_EndPattern"),
    b"%AI5_BeginLayer": SectionDesc(b"%AI5_EndLayer--"),
    b"%AI5_BeginRaster": SectionDesc(b"%AI5_EndRaster"),
    b"%AI5_BeginPalette": SectionDesc(b"%AI5_EndPalette"),
    b"%AI5_BeginGradient:": SectionDesc(b"%AI5_EndGradient"),
    b"%AI5_Begin_NonPrinting": SectionDesc(b"%AI5_End_NonPrinting--"),
    b"%AI8_BeginBrushPattern": SectionDesc(b"%AI8_EndBrushPattern"),
    b"%AI8_BeginPluginObject": SectionDesc(
        b"%AI8_EndPluginObject", parse_method=FileParser.read_list_section
    ),
    b"%AI9_BeginArtStyleList": SectionDesc(
        b"%AI9_EndArtStyleList", parse_method=FileParser.read_list_section
    ),
    b"%AI9_BeginArtStyles": SectionDesc(b"%AI9_EndArtStyles"),
    b"%AI9_BeginDocumentData": SectionDesc(b"%AI9_EndDocumentData"),
    b"%AI10_BeginSVGFilter": SectionDesc(b"%AI10_EndSVGFilter"),
    b"%AI10_BeginSymbol": SectionDesc(b"%AI10_EndSymbol"),
    b"%AI10_BeginSymbolList": SectionDesc(
        b"%AI10_EndSymbolList", parse_method=FileParser.read_list_section
    ),
    b"%AI10_BeginSymbolList\x00": SectionDesc(
        b"%AI10_EndSymbolList\x00", parse_method=FileParser.read_list_section
    ),
    b"%AI11_BeginTextDocument": SectionDesc(b"%AI11_EndTextDocument"),
    # this really ends with AI10, not AI14
    b"%AI14_BeginSymbol": SectionDesc(b"%AI10_EndSymbol"),
    b"%AI17_Begin_Content_if_version_gt:24": SectionDesc(
        b"%AI17_End_Versioned_Content", parse_method=FileParser.read_versioned_content
    ),
    b"%AI24_BeginSymbolList": SectionDesc(
        b"%AI24_EndSymbolList", parse_method=FileParser.read_list_section
    ),
    # b"u": SectionDesc(b"U"), # we leave this part to the state machine
    # b"*u": SectionDesc(b"*U"),
}


class DataHierarchyParser:
    """Parse the document data."""

    @staticmethod
    def read_container(
        stream: StreamWrapper, token: bytes
    ) -> Optional[Union[HierarchyObject, Dict[str, Any], List, Tuple[str, str]]]:
        token = token[1:]
        if token in ObjectContainers or token in (
            b"Dictionary",
            b"ArtDictionary",
            b"XMLNode",
            b"Array",
            b"Art",
            b"SimpleStyle",
            b"Binary",
            b"AI11TextDocument",
            b"AI11UndoFreeTextDocument",
            b"XMLUID",
            b"Mask",
        ):
            stream.skip(b":")
        else:
            return None
        if token in ObjectContainers:
            return DataHierarchyParser.read_object(stream, ObjectContainers[token]())
        if token in [b"Dictionary", b"ArtDictionary"]:
            return DataHierarchyParser.read_dictionary(stream, token)
        if token == b"XMLNode":
            return DataHierarchyParser.read_xmlnode(stream)
        if token == b"Array":
            return DataHierarchyParser.read_array(stream)
        if token == b"Art":
            return DataHierarchyParser.read_art(stream)
        if token == b"SimpleStyle":
            return DataHierarchyParser.read_simple_style(stream)
        if token == b"Mask":
            return DataHierarchyParser.read_mask(stream)
        if token in [b"Binary", b"AI11TextDocument", b"AI11UndoFreeTextDocument"]:
            return DataHierarchyParser.read_binary(stream, token)
        if token == b"XMLUID":
            return DataHierarchyParser.read_xmluid(stream)
        return None

    @staticmethod
    def read_object(stream: StreamWrapper, result: HierarchyObject) -> HierarchyObject:
        """Reads the content of a HierarchyObject, including the terminating ;"""
        for token in stream.yield_tokens():
            if token == b",":
                continue  # skip
            if token == b";":
                # Container finished.
                return result
            if token.startswith(b"/"):
                # sub-container
                value = DataHierarchyParser.read_container(stream, token)
                key = stream.read_token()
                assert key.startswith(b"/"), (key, value)
            else:
                # primitive value, read tokens until "/"
                value, key = DataHierarchyParser._read_until_slash(stream, token)

            setattr(result, dec(key)[1:], value)
        raise ValueError("Container end not found")

    @staticmethod
    def read_mask(stream: StreamWrapper) -> Mask:
        result = Mask()
        while True:
            # This is super hacky, and works around the problem that
            # within a mask it's not always clear whether we are
            # currently parsing "free form" AI or dictionary entries,
            # and the order is also not always the same.
            if b"/Bool" in stream.peek(10):
                value = DataHierarchyParser._dict_array_processor(stream)
                if value is None:
                    return result
                key = stream.read_token()
                setattr(result, dec(key), value)
                stream.skip_optional(b",")
            elif stream.peek_token() == b";":
                stream.skip(b";")
                return result
            else:
                setattr(result, "Art", FileParser.read_many_objects(stream, b"Art"))
                stream.skip(b",")

    @staticmethod
    def read_simple_style(stream: StreamWrapper) -> SimpleStyle:
        result: List[Operator] = []
        for token in stream.yield_tokens():
            # We read an operator, unless token is a semicolon. In that case, return.
            if token == b"/Paint":
                stream.skip(b";")
                ret = SimpleStyle()
                ret.Paint = result
                return ret
            else:
                cmd = FileParser.read_operator(stream, first_token=token)
                if cmd is not None:
                    result.append(cmd)
        raise ValueError("EOF while reading SimpleStyle")

    typemap: Dict[
        bytes,
        Callable[
            [
                List[str],
            ],
            Any,
        ],
    ] = {
        b"/Bool": lambda x: bool(int(x[0])),
        b"/Int": lambda x: int(x[0]),
        b"/Real": lambda x: float(x[0]),
        b"/RealMatrix": lambda x: tuple(float(i) for i in x),
        b"/RealPoint": lambda x: Point.from_tokens(*x),
        b"/RealPointRelToROrigin": lambda x: PointRelToROrigin.from_tokens(*x),
        b"/String": lambda x: "".join(x),
        b"/UnicodeString": lambda x: "".join(x),
    }

    @staticmethod
    def _read_until_slash(stream: StreamWrapper, first_token):
        def it():
            yield first_token
            yield from stream.yield_tokens()

        values = []
        for tok in it():
            if tok.startswith(b"/") and not stream.last_was_string:
                type = tok
                break
            values.append(tok)
        return values, type

    @staticmethod
    def _dict_array_processor(stream: StreamWrapper) -> Optional[Any]:
        token = stream.read_token()
        if token == b";":
            return None
        if token.startswith(b"/"):
            # This is a special case, very large ints are stored with an initial /
            if (
                len(token) > 1
                and token[2:].isdigit()
                and (token[1:2].isdigit() or token[1:2] == b"-")
            ):
                stream.skip(b"/Int")
                return int(token[1:])
            if (a := DataHierarchyParser.read_container(stream, token)) is not None:
                return a
        if token == b"X=":
            # Simply read until X+
            return DataHierarchyParser.read_art(stream, b"X+")
        values, type = DataHierarchyParser._read_until_slash(stream, token)
        return DataHierarchyParser.typemap[type]([dec(i) for i in values])

    @staticmethod
    def read_dictionary(stream: StreamWrapper, type: bytes) -> Dict[str, Any]:
        """Reads the content of a dictionary, including the terminating ;"""
        result: Dict[str, Any] = {}
        result["_type"] = dec(type)
        if stream.skip_optional(b"/NotRecorded"):
            result["Recorded"] = False
            stream.skip(b",")
        if stream.skip_optional(b"/Recorded"):
            result["Recorded"] = True
            stream.skip(b",")
        while True:
            value = DataHierarchyParser._dict_array_processor(stream)
            if value is None:
                return result
            key = stream.read_token()
            result[dec(key)] = value
            stream.skip_optional(b",")

    @staticmethod
    def read_array(stream: StreamWrapper) -> List[Any]:
        """Reads the content of an array, including the terminating ;"""
        result: List[Any] = []
        while True:
            value = DataHierarchyParser._dict_array_processor(stream)
            if value is None:
                return result
            result.append(value)
            stream.skip_optional(b",")

    @staticmethod
    def read_xmlnode(stream: StreamWrapper) -> Union[Dict[str, Any], Tuple[str, str]]:
        """Reads the content of an /XMLNode, including the terminating ;

        Simplifies attribute nodes into dictionary entries."""

        dict = DataHierarchyParser.read_dictionary(stream, b"XMLNode")
        if "xmlnode-nodetype" in dict:
            type = dict["xmlnode-nodetype"]
        else:
            type = (
                2 if ("xmlnode-nodename" in dict and "xmlnode-nodevalue" in dict) else 1
            )

        if type in (2, 3):  # Attribute or text
            return (dict["xmlnode-nodename"], dict["xmlnode-nodevalue"])
        elif type in (1, 9):  # node or root node
            return dict
            # TODO: Convert this into something more easy to work with.
            # Can't use etree.element since we might not know the ns prefixes.
            result = etree.Element(dict["xmlnode-nodename"])
            for key in dict["xmlnode-attributes"]:
                k, value = dict["xmlnode-attributes"][key]
                assert key == k
                result.set(key, value)
            for element in dict["xmlnode-children"]:
                if isinstance(element, etree.Element):
                    result.append(element)
                if isinstance(element, tuple):
                    if element[0] == "#text":
                        result.text = element[1]
            return result
        raise ValueError("Unknown xmlnode-nodetype")

    binary_typemap = {
        b"Binary": Binary,
        b"AI11TextDocument": AI11TextDocument,
        b"AI11UndoFreeTextDocument": AI11UndoFreeTextDocument,
    }

    @staticmethod
    def read_binary(stream: StreamWrapper, type):
        """Read /Binary including the terminating ;"""
        stream.skip(b"/ASCII85Decode")
        stream.skip(b",")
        data = []
        while True:
            token = stream.read_line().strip()[1:]
            data.append(token)
            if token[-2:] == b"~>":
                # read %_ if it occurs
                if stream.peek(2) == b"%_":
                    stream.read(2)
                break

        # Continue reading as if this was a normal object.
        result = DataHierarchyParser.binary_typemap[type](data)
        result._type = type
        return DataHierarchyParser.read_object(stream, result)

    @staticmethod
    def read_art(stream: StreamWrapper, end_section=b";"):
        """Read art. Simply read until the next semicolon on the same level of %_ indentation"""
        commands: List[Operator] = []
        for token in stream.yield_tokens():
            # We read an operator, unless token is a semicolon. In that case, return.
            if token == end_section:
                return Art(commands)
            else:
                cmd = FileParser.read_one_object(stream, token, end_section)
                if cmd is not None:
                    commands.append(cmd)

    @staticmethod
    def read_xmluid(stream: StreamWrapper):
        result = stream.read_token()
        stream.skip(b";")
        return result.decode("latin-1")


def parse_bytes(data: bytes, method):
    wrapper = StreamWrapper(BytesIO(data))
    return method(wrapper)
