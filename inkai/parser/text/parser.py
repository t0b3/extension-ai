# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Parser for AI 11 text elements."""
from typing import Dict, List, Union
from inkai.parser.streamreader import StreamWrapper, dec

ObjectType = Dict[str, "ValueType"]
ValueType = Union[str, bytes, bool, int, float, List["ValueType"], ObjectType]


class TextDocumentParser:
    """Parses an AI11TextDocument without external libraries for
    performance comparison.

    The grammar of this parser is described in
    docs/specification_amendments/text_documents.md.
    """

    @staticmethod
    def read_text_document(stream) -> Dict[str, ValueType]:
        return TextDocumentParser.read_object(stream)

    @staticmethod
    def read_object(stream: StreamWrapper):
        """Parse an object. Note that the opening tag has already been
        consumed by read_value (or is not present on the root object)"""
        result: ObjectType = {}
        for token in stream.yield_tokens():
            if token == b">>":
                return result
            if token.startswith(b"/"):
                result[dec(token)] = TextDocumentParser.read_value(
                    stream, stream.read_token()
                )
        return result

    @staticmethod
    def read_list(stream: StreamWrapper):
        """Parse a list. Note that the opening tag has already been
        consumed by read_value"""
        result: List[ValueType] = []
        for token in stream.yield_tokens():
            if token == b"]":
                return result
            result.append(TextDocumentParser.read_value(stream, token))

    @staticmethod
    def read_value(stream: StreamWrapper, token: bytes) -> ValueType:
        """Parse a value. Primitive values are parsed in this method;
        lists, strings and subobjects are referred to their own methods"""
        if token == b"<<":
            return TextDocumentParser.read_object(stream)
        if token == b"[":
            return TextDocumentParser.read_list(stream)
        else:
            # Is the previous token a string?
            if stream.last_was_string:
                if token.startswith(b"\xfe\xff"):
                    token = token[2:]
                    if b"\x00" in token:
                        try:
                            return token.decode("utf-16-be")
                        except:  # Encoding error
                            pass
                return token
            if token.startswith(b"/"):
                return dec(token)
            if token in (b"true", b"false"):
                return token == b"true"
            else:
                try:
                    return float(token) if b"." in token else int(token)
                except:
                    raise ValueError("Unexpected value")


__all__ = ["AI11BasicTextDocumentParser"]
