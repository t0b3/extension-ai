# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
from __future__ import annotations
from typing import Optional, Any, List, Union, Tuple, TYPE_CHECKING
from typing import Dict as TypingDict
import base64
from collections import namedtuple

from .streamreader import dec

if TYPE_CHECKING:
    from .fileparser import Operator

CollectedSection = type
from lxml import etree

PointTuple = namedtuple("PointTuple", ("x", "y"))


class Point(PointTuple):
    """Used for /RealPoint."""

    @classmethod
    def from_tokens(cls, x: str, y: str, type: str = "/Point"):
        """Create a point from tokens."""
        return cls(float(x), float(y))


class PointRelToROrigin(Point):
    """Used for /RealPointRelToROrigin.

    This is a point relative to the ruler origin.
    """

    def as_point(self, ruler_origin: PointTuple) -> Point:
        """Convert this to a point using the ruler origin."""
        return Point(ruler_origin.x + self.x, ruler_origin.y + self.y)


class HierarchyObject:
    """Base class for a hierarchy object"""

    def __eq__(self, __value: object) -> bool:
        if isinstance(__value, dict):
            return self.asdict() == __value
        return super().__eq__(__value)

    def asdict(self):
        result = {"_type": self.__class__.__name__}
        for key in dir(self):
            if not hasattr(self.__class__, key) or not isinstance(
                getattr(self.__class__, key), property
            ):
                continue
            if key[0].islower():
                continue
            val = getattr(self, key)
            if isinstance(val, HierarchyObject):
                result[key] = val.asdict()
            else:
                result[key] = val
        return result

    def __repr__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, key):
        return getattr(self, key)


class Document(HierarchyObject):
    def __init__(self):
        self._recorded: TypingDict[str, Any] = {}
        self._not_recorded: TypingDict[str, Any] = {}
        super().__init__()

    @property
    def Recorded(self) -> TypingDict[str, Any]:
        return self._recorded

    @Recorded.setter
    def Recorded(self, value: TypingDict[str, Any]):
        self._recorded = value

    @property
    def NotRecorded(self) -> TypingDict[str, Any]:
        return self._not_recorded

    @NotRecorded.setter
    def NotRecorded(self, value: TypingDict[str, Any]):
        self._not_recorded = value


class Art(HierarchyObject):
    def __init__(self, data: List[Operator]):
        self._data = data
        super().__init__()

    @property
    def Data(self) -> List[Operator]:
        return self._data


class AI11Text(HierarchyObject):
    def __init__(self):
        self._free_undo: Optional[int] = None
        self._frame_index: Optional[int] = None
        self._story_index: Optional[int] = None
        self._confining_path: Optional[Art] = None
        self._text_antialiasing: Optional[int] = None
        super().__init__()

    @property
    def FreeUndo(self) -> Optional[int]:
        return self._free_undo

    @FreeUndo.setter
    def FreeUndo(self, value: List[bytes]):
        self._free_undo = int(value[0])

    @property
    def FrameIndex(self) -> Optional[int]:
        return self._frame_index

    @FrameIndex.setter
    def FrameIndex(self, value: List[bytes]):
        self._frame_index = int(value[0])

    @property
    def StoryIndex(self) -> Optional[int]:
        return self._story_index

    @StoryIndex.setter
    def StoryIndex(self, value: List[bytes]):
        self._story_index = int(value[0])

    @property
    def ConfiningPath(self) -> Optional[Art]:
        return self._confining_path

    @ConfiningPath.setter
    def ConfiningPath(self, value):
        assert value is None or isinstance(value, Art)
        self._confining_path = value

    @property
    def TextAntialiasing(self) -> Optional[int]:
        return self._text_antialiasing

    @TextAntialiasing.setter
    def TextAntialiasing(self, value: List[bytes]):
        self._text_antialiasing = int(value[0])


class FilterBase(HierarchyObject):
    def __init__(self):
        self._filter: Optional[Tuple[str, int, int]] = None
        self._visible: Optional[int] = None
        super().__init__()

    @property
    def Filter(self) -> Optional[Tuple[str, int, int]]:
        return self._filter

    @Filter.setter
    def Filter(self, value: List[bytes]):
        assert len(value) == 3
        self._filter = (value[0].decode("latin-1"), int(value[1]), int(value[2]))

    @property
    def Visible(self) -> Optional[int]:
        return self._visible

    @Visible.setter
    def Visible(self, value: List[bytes]):
        self._visible = int(value[0])


class BasicFilter(FilterBase):
    def __init__(self):
        self._dict: Optional[TypingDict[str, Any]] = None
        self._plugin_file_name: Optional[str] = None
        self._title: Optional[str] = None
        self._fill_or_stroke: Optional[int] = None
        super().__init__()

    @property
    def Dict(self) -> Optional[TypingDict[str, int]]:
        return self._dict

    @Dict.setter
    def Dict(self, value: Optional[TypingDict[str, int]]):
        assert isinstance(value, dict)
        self._dict = value

    @property
    def PluginFileName(self) -> Optional[str]:
        return self._plugin_file_name

    @PluginFileName.setter
    def PluginFileName(self, value: List[bytes]):
        self._plugin_file_name = value[0].decode("latin-1")

    @property
    def Title(self) -> Optional[str]:
        return self._title

    @Title.setter
    def Title(self, value: List[bytes]):
        self._title = value[0].decode("latin-1")

    @property
    def FillOrStroke(self) -> Optional[int]:
        return self._fill_or_stroke

    @FillOrStroke.setter
    def FillOrStroke(self, value: List[bytes]):
        self._fill_or_stroke = int(value[0])


class CompoundFilter(FilterBase):
    def __init__(self):
        self._parts: List[FilterBase] = []
        super().__init__()

    def __setattr__(self, name, value):
        if name == "Part":
            assert isinstance(value, FilterBase), value.__class__.__name__
            self._parts.append(value)
        else:
            super().__setattr__(name, value)

    @property
    def Part(self) -> List[FilterBase]:
        return self._parts


class Style(HierarchyObject):
    """Style information.

    See
    - KnownStyle
    - ActiveStyle
    - SimpleStyle
    """


class SimpleStyle(Style):
    def __init__(self):
        self._paint: Optional[List[Operator]] = None
        super().__init__()

    @property
    def Paint(self) -> Optional[List[Operator]]:
        return self._paint

    @Paint.setter
    def Paint(self, value: List[Operator]):
        self._paint = value


class ActiveStyle(Style):
    def __init__(self):
        self._execution: Optional[CompoundFilter] = None

    @property
    def Execution(self) -> Optional[CompoundFilter]:
        return self._execution

    @Execution.setter
    def Execution(self, value: Optional[CompoundFilter]):
        assert isinstance(value, (CompoundFilter))
        self._execution = value


class KnownStyle(Style):
    def __init__(self):
        self._def = None
        self._name: Optional[str] = None
        super().__init__()

    @property
    def Name(self) -> Optional[str]:
        return self._name

    @Name.setter
    def Name(self, value: List[bytes]):
        self._name = value[0].decode("latin-1")

    @property
    def Def(self) -> Optional[Union[SimpleStyle, ActiveStyle]]:
        return self._def

    @Def.setter
    def Def(self, value):
        assert isinstance(value, (SimpleStyle, ActiveStyle))
        self._def = value


class BFSStyle(HierarchyObject):
    def __init__(self):
        self._def: Optional[List[bytes]] = None
        super().__init__()

    @property
    def Def(self) -> Optional[List[bytes]]:
        return self._def

    @Def.setter
    def Def(self, value: List[bytes]):
        self._def = value

    # @property
    # def ddef(self) -> CollectedSection:
    #    """The parsed style definitions in the Paint attribute."""
    #    if self.Def is None:
    #        raise ValueError("Def not set.")
    #    from .objects.object_section import ObjectSection
    #
    #    return ObjectSection.collect_from_string(self.Def)


class BlendStyle(BFSStyle):
    pass


class FillStyle(BFSStyle):
    pass


class StrokeStyle(BFSStyle):
    pass


class SVGFilter(HierarchyObject):
    def __init__(self):
        self._def: Optional[etree.Element] = None
        super().__init__()

    @property
    def Def(self) -> Optional[etree.Element]:
        return self._def

    @Def.setter
    def Def(self, value: etree.Element):
        # assert isinstance(value, etree.Element)
        self._def = value


class Binary(HierarchyObject):
    """A /Binary object."""

    def __init__(self, data: List[bytes]):
        """Create a new binary object with some data."""
        self._data = data
        self._type = "Binary"
        super().__init__()

    def decode(self) -> bytes:
        """Return the binary data."""
        return base64.a85decode(
            b"\n".join(self._data).decode("latin-1"), adobe=True, foldspaces=True
        )

    @property
    def Data(self) -> bytes:
        return self.decode()


class AI11TextDocument(Binary):
    def __init__(self, data: List[bytes]):
        self._ruler_origin: Optional[Point] = None
        super().__init__(data)

    @property
    def RulerOrigin(self) -> Optional[Point]:
        return self._ruler_origin

    @RulerOrigin.setter
    def RulerOrigin(self, value: List[bytes]):
        assert len(value) == 2
        self._ruler_origin = Point(*map(float, value))


class GObjRef(HierarchyObject):
    def __init__(self):
        self._plugin_obj: Optional[Tuple[str, str]] = None

    @property
    def PluginObj(self) -> Optional[Tuple[str, str]]:
        return self._plugin_obj

    @PluginObj.setter
    def PluginObj(self, value: List[bytes]):
        self._plugin_obj = (dec(value[0]), dec(value[1]))


class Mask(HierarchyObject):
    def __init__(self):
        self._art: Optional[List[Operator]] = None
        super().__init__()

    @property
    def Art(self) -> Optional[List[Operator]]:
        return self._art

    @Art.setter
    def Art(self, value: List[Operator]):
        self._art = value

    # TODO


class AI11UndoFreeTextDocument(Binary):
    pass
