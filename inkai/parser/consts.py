# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later


from __future__ import annotations
from math import ceil, floor
import re
import base64
from dataclasses import dataclass, field
from io import BytesIO

from inkai.parser.units import header_comments_to_units, to_px
from .streamreader import StreamWrapper, dec, enc
from .text import TextDocumentParser
from .text.hierarchy import TextDocumentObject
from .text.definitions import defs
from typing import Dict, Set, Tuple, Union, cast, Optional
from .hierarchy_objects import *
from .operators import *


ObjectContainers: Dict[bytes, type] = {
    b"KnownStyle": KnownStyle,
    b"ActiveStyle": ActiveStyle,
    b"BlendStyle": BlendStyle,
    b"FillStyle": FillStyle,
    b"StrokeStyle": StrokeStyle,
    b"BasicFilter": BasicFilter,
    b"CompoundFilter": CompoundFilter,
    b"AI11Text": AI11Text,
    b"Document": Document,
    b"SVGFilter": SVGFilter,
    b"GObjRef": GObjRef,
}

_operatorlist: Set[Union[bytes, Tuple[bytes, type]]] = {
    b"A",
    b"Ae",
    b"An",
    b"u",
    b"*u",
    b"q",
    b"AE",
    b"Ap",
    b"As",
    (b"b", PathRenderOperator),
    (b"B", PathRenderOperator),
    b"Bb",
    b"BB",
    b"Bc",
    b"Bd",
    b"BD",
    b"Bg",
    b"Bh",
    b"Bm",
    b"Bn",
    b"%_Br",
    b"Bs",
    b"%_BS",
    b"%_Bs",
    b"%_Bs",
    b"%_BS",
    b"Bs",
    b"%_BS",
    b"Bs",
    (b"c", curveOperator),
    (b"C", CurveOperator),
    (b"d", StrokeDashOperator),
    (b"D", WindingOrderOperator),
    b"E",
    b"exec",
    (b"f", PathRenderOperator),
    (b"F", PathRenderOperator),
    b"g",
    b"G",
    b"h",
    b"H",
    (b"i", FlatnessOperator),
    (b"j", LineJoinOperator),
    (b"J", LineCapOperator),
    (b"l", lineOperator),
    (b"L", LineOperator),
    (b"Lb", LayerPropertiesOperator),
    (b"Ln", LayerNameOperator),
    b"LB",
    (b"m", MoveOperator),
    (b"M", MiterLimitOperator),
    (b"n", PathRenderOperator),
    (b"N", PathRenderOperator),
    b"Np",
    (b"O", OverprintOperator),
    b"p",
    b"P",
    b"Pb",
    b"PB",
    b"Pc",
    b"Pg",
    b"q",
    b"Q",
    (b"R", OverprintOperator),
    (b"s", PathRenderOperator),
    (b"S", PathRenderOperator),
    b"Tc",
    b"Tf",
    b"Tk" b"Tl",
    b"To",
    b"TO",
    b"Tp",
    b"TP",
    b"Tm",
    b"Tr",
    b"Tt",
    b"Tv",
    b"TV",
    b"Tx",
    b"Tz",
    b"u",
    b"U",
    b"*u",
    b"*U",
    (b"v", Smooth1Operator),
    (b"V", Smooth2Operator),
    (b"w", LineWidthOperator),
    b"W",
    b"X=",
    b"X+",
    b"Xa",
    b"XA",
    b"Xa",
    b"k",
    b"K",
    b"Xd",
    b"XD",
    b"XG",
    b"Xh",
    b"XH",
    b"XI",
    b"Xk",
    b"Xm",
    b"XN",
    b"XP",
    (b"XR", FillRuleOperator),
    b"Xw",
    b"XW",
    b"Xy",
    b"Xz",
    b"XZ",
    (b"y", Smooth3Operator),
    (b"Y", Smooth4Operator),
    b"TE",
    b"TZ",
}

operatorlist: Dict[bytes, type] = {}
for entry in _operatorlist:
    if isinstance(entry, tuple):
        operatorlist[entry[0]] = entry[1]
    else:
        operatorlist[entry] = Operator


def operator_factory(command, arguments):
    return operatorlist[command](command, arguments)


@dataclass
class Page:
    left: float
    bottom: float
    right: float
    top: float

    @property
    def width(self) -> float:
        return self.right - self.left

    @property
    def height(self) -> float:
        return abs(self.top - self.bottom)

    def __iter__(self):
        for attr in (self.left, self.bottom, self.right, self.top):
            yield attr


@dataclass
class AIDocument:
    header: Header
    prolog: Section
    setup: Setup
    trailer: List

    def _get_artboard(self) -> dict:
        """Return the artboard or {}

        Example:

            {'ArtboardUUID': '6cb1f831-1548-49bd-b4d1-54e72acb17a9',
            'DisplayMark': 0,
            'IsArtboardDefaultName': True,
            'IsArtboardSelected': True,
            'Name': 'Zeichenfläche 1',
            'PAR': 1.0,
            'PositionPoint1': PointRelToROrigin(x=0.0, y=128.0),
            'PositionPoint2': PointRelToROrigin(x=128.0, y=0.0),
            'RulerOrigin': Point(x=8127.0, y=8127.110168457),
            '_type': 'Dictionary'}
        """
        if dd := self._document_data:
            return dd["Recorded"].get("ArtboardArray", [{}])[0]  # type: ignore
        return {}

    @property
    def drawing_ruler_origin(self) -> Point:
        """Calculate the ruler origin from all the metadata available.

        This is stable across all AI versions.
            @property

        See docs/specification_amendments/units.md and AI Spec page 30.
        """
        # print(self._document.header_comments.file_format)
        art_size = self.header.art_size
        crop_box = self.header.cropmarks
        if crop_box is not None:
            width = crop_box.right - crop_box.left
            height = crop_box.top - crop_box.bottom
        elif art_size is not None:
            width = art_size.width
            height = art_size.height
        else:
            return Point(0, 0)
        templateBox = self.header.template_box
        x = (width - templateBox.left - templateBox.right) / 2
        y = (height + templateBox.top + templateBox.bottom) / 2
        return Point(
            ceil(x) * self.header.large_canvas_scale,
            ceil(y) * self.header.large_canvas_scale,
        )

    @property
    def canvas_ruler_origin(self) -> Optional[Point]:
        """This is the ruler origin for the canvas coordinate system.

        None if there is no canvas ruler origin.
        """
        if self.setup.text_document is not None:
            # The ruler origin (relative to the canvas) is always
            # initialized at floor(canvassize - artboardsize). However
            # the artboard size may change, or the artboard position,
            # or the artboard ruler origin. But none of that actually
            # changes the relation between Canvas and Drawing coords.
            # The only origin that stays constant is the TextDocument
            # ruler origin, introduced in CS (2003). Yes, really.
            return self.setup.ruler_origin
        else:
            # For version < CS, it was not possible to move the
            # artboard, or have multiple artboards at all.
            # Trying to save a file with a moved first page as AI10
            # and reopening completely garbles the content.
            # But old files, or those with a single, unmoved
            # first page, should work fine with this code.
            # (Live shapes were introduced in v9)
            # This also appears how AI reconstructs the ruler origin
            # if the text document is deleted
            canvas_size = self.header.canvassize
            if canvas_size is None:
                return None
            page = self.drawing_first_page_position
            drawing_ruler_origin = self.drawing_ruler_origin

            return Point(
                floor(canvas_size / 2 - page.width / 2 + drawing_ruler_origin.x),
                floor(canvas_size / 2 - page.height / 2 + drawing_ruler_origin.y),
            )

    @property
    def canvas_first_page_position(self) -> Optional[Point]:
        """Compute the canvas ruler origin for the first page.

        None if there is no canvas ruler origin.
        """
        result = self._get_artboard().get("RulerOrigin")
        if result is not None:
            return result
        cro = self.canvas_ruler_origin
        if cro is None:
            return None
        page = self.drawing_first_page_position
        return Point(cro.x + page.left, cro.y + page.top)

    @property
    def drawing_first_page_position(self) -> Page:
        """This is information about the first page.

        This is calculated from the document.
        AI Spec page 30:

            All artwork elements, as well as the Bounding Box, Template Box, and Tile
            Box, are written out in coordinates relative to the ruler origin, with y
            increasing up and x increasing to the right, and bounds in the order left,
            bottom, right, top.

                y
                ^
                |<----- right ---->|
                |<- left ->|       |
                |          +-------+---
                |          |       |  ^
                |          | page  |  |
                |          |       |  | top
                |   bottom +-------+  |
                |      |              v
          (0,0) +--------------------------> x

        """
        art_board = self._get_artboard()
        if art_board:
            p1 = art_board["PositionPoint1"]
            p2 = art_board["PositionPoint2"]
            # print("art_board", art_board, self.large_canvas_scale)
            lcs = self.header.large_canvas_scale
            return Page(lcs * p1.x, lcs * (-p2.y), lcs * (p2.x), lcs * -p1.y)
        art_size = self.header.art_size
        template_box = self.header.template_box
        crop_box = self.header.cropmarks
        if art_size is None:  # AI3 doesn't specify art_size
            height = -template_box.top - template_box.bottom
            return Page(
                0,
                0 if height < 0 else height,
                template_box.left + template_box.right,
                height if height < 0 else 0,
            )
        if crop_box is not None:
            # In some older version, the artboard array is sometimes omitted when
            # only one page exists. However CS4 only specifies the crop_box, the
            # art_size is wrong.
            width = crop_box.right - crop_box.left
            height = crop_box.top - crop_box.bottom
            x_origin = crop_box.left
            y_origin = -crop_box.top
        else:
            width = art_size.width
            height = art_size.height
            x_origin = -(width - template_box.left - template_box.right) / 2
            y_origin = -(height + template_box.bottom + template_box.top) / 2

        return Page(
            floor(x_origin),
            height + floor(y_origin),
            width + floor(x_origin),
            floor(y_origin),
        )

    @property
    def _document_data(self) -> Optional[dict]:
        dd: Optional[Section] = self.setup.first_subsection_of_type(
            b"%AI9_BeginDocumentData"
        )
        if dd is None:
            return None
        else:
            return dd.children[0]  # type: ignore

    @property
    def units(self) -> str:
        """The units of this document.

        cm, mm, px, ...

        See docs/specification_amendments/units.md
        """
        return self.header.units

    @property
    def in_px(self) -> float:
        """The scaling for the unit used to convert it to pixels.

        If the units of length are centimeters:
        length_in_px = length * in_px
        """
        return to_px(self.units)


@dataclass
class Data:
    length: int
    type: str
    raw: bytes
    additional: bytes

    HEX_REPLACEMENT = re.compile(b"[^0-9A-Fa-f]")
    HEX_BYTES_REGEX = re.compile(
        rb"\A(?:%(?:[0-9A-Fa-f]{2}\s*)+)?(?:\s+%(?:[0-9A-Fa-f]{2})+)*\s*\Z"
    )

    @property
    def decoded(self) -> bytes:
        """The data of the content."""
        if Data.HEX_BYTES_REGEX.match(self.raw):
            # Hex
            return base64.b16decode(
                Data.HEX_REPLACEMENT.sub(b"", self.raw.replace(b"\r\n%", b"")).upper()
            )
        if self.type in ("Binary", "ASCII"):
            return self.raw.replace(b"\r\n", b"")
        raise ValueError("datatype not in hex, binary or ASCII")


@dataclass
class Section:
    start: bytes
    arguments: List[bytes] = field(default_factory=list)
    children: List[Union[Section, Operator, HierarchyObject, Dict, List]] = field(
        default_factory=list
    )

    def first_subsection_of_type(self, header: bytes) -> Optional[Section]:
        return next(
            (i for i in self.children if isinstance(i, Section) and i.start == header),
            None,
        )


TemplateBox = namedtuple("TemplateBox", ("left", "bottom", "right", "top"))
Cropmarks = namedtuple("Cropmarks", ("left", "bottom", "right", "top"))
ArtSize = namedtuple("ArtSize", ("width", "height"))


class Header(dict):
    @property
    def large_canvas_scale(self) -> float:
        """The LargeCanvasScale comment, defaults to 1.

        See docs/specification_amendments/units.md
        """
        return float(self.get("AI24_LargeCanvasScale", [1])[0])

    @property
    def template_box(self) -> TemplateBox:
        """The template box of the document. (llx, lly, urx, ury)

        See page 21 AI Spec.
        """
        return TemplateBox(*map(float, self["AI3_TemplateBox"]))

    @property
    def cropmarks(self) -> Optional[Cropmarks]:
        """The template box of the document. (llx, lly, urx, ury)

        See page 21 AI Spec.
        """
        if "AI3_Cropmarks" in self:
            return Cropmarks(*map(float, self["AI3_Cropmarks"]))
        return None

    @property
    def art_size(self) -> Optional[ArtSize]:
        """This comment specifies the size of the artboard in points. (width, height)"""
        artsize = self.get("AI5_ArtSize")
        return None if artsize is None else ArtSize(*map(float, artsize))

    @property
    def page_origin(self) -> Optional[Point]:
        """The PageOrigin header comment."""
        page_origin = self.get("PageOrigin")
        return None if page_origin is None else Point(*map(float, page_origin))

    @property
    def tile_box(self) -> Optional[TemplateBox]:
        """The AI3_TileBox header comment."""
        tile_box = self.get("AI3_TileBox")
        return None if tile_box is None else TemplateBox(*map(float, tile_box))

    @property
    def units(self) -> str:
        """The units of this document.

        cm, mm, px, ...

        See docs/specification_amendments/units.md
        """
        ai5 = "".join(self.get("AI5_RulerUnits", ""))
        ai24 = "".join(self.get("AI24_RulerUnitsLarge", ""))
        return header_comments_to_units(ai5, ai24)

    @property
    def canvassize(self) -> float:
        return float(self.get("CanvasSize", 16383))


@dataclass
class Encoding(Section):
    fonttype: bytes = b""


@dataclass
class Setup(Section):
    @property
    def art_styles(self):
        section = self.first_subsection_of_type(b"%AI9_BeginArtStyles")
        if section is None:
            return {}
        return {i.Name: i for i in section.children if isinstance(i, KnownStyle)}

    @property
    def palettes(self):
        section = self.first_subsection_of_type(b"%AI5_BeginPalette")
        if section is None:
            return {}
        result = {}
        iterator = section.children.__iter__()
        assert isinstance(nc := next(iterator), Operator) and nc.command == "Pb"
        for op in iterator:
            assert isinstance(op, Operator)
            if op.command in ("Pc", "PB"):
                continue
            else:
                assert isinstance(nc2 := next(iterator), Operator)
                result[dec(cast(bytes, nc2.tokens[0]))] = op
        return result

    @property
    def text_document(self) -> Optional[TextDocumentObject]:
        section = self.first_subsection_of_type(b"%AI11_BeginTextDocument")
        if section is None:
            return None
        child = next(i for i in section.children if isinstance(i, AI11TextDocument))
        parsed_dict = TextDocumentParser.read_object(StreamWrapper(BytesIO(child.Data)))
        return TextDocumentObject(defs, parsed_dict)

    @property
    def ruler_origin(self) -> Optional[Point]:
        section = self.first_subsection_of_type(b"%AI11_BeginTextDocument")
        if section is None:
            return None
        child = next(i for i in section.children if isinstance(i, AI11TextDocument))
        return child.RulerOrigin


@dataclass
class GradientFallback:
    pass
