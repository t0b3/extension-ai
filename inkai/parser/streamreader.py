# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

"""Hybrid tokenizer for AI files.
At any point in the stream, we can either yield a token (read until the
next whitespace, except for parentheses-delimited strings), yield a line,
or do manual stream processing.

This is the "hot codepath" for parsing, so this has been heavily optimized 
for speed. Three different strategies are applied:
    
1) read_line uses data.search(rb"\n", stream.tell()) to find the end of the next
   line
2) tokens are located using re.compile(br"[ \n\r]").search(data, stream.tell());
   this indicates the position of the next whitespace. If the first character
   of this is an opening bracket, read a string instead
3) strings are a bit more complex and rare-ish, so they are read character by
   character as a sort-of state machine."""

from io import BufferedReader
from typing import Generator
import warnings
import re


def dec(data: bytes) -> str:
    return data.decode("latin-1")


def enc(data: str) -> bytes:
    return data.encode("latin-1")


# When searching whitespace, we're looking for an arbitrary
# number of spaces, possibly followed by exactly one
# line break. Line breaks can be any of \r\n, \r or \n.
# We need to match at least one wsp (space or newline).
wsp_regex = re.compile(rb" *((?:\r\n?|\n)| +)")
newline_regex = re.compile(rb"(\r\n?|\n)")

string_replacement = {
    b"n": b"\n",
    b"r": b"\r",
    b"t": b"\t",
    b"b": b"\b",
    b"f": b"\f",
    b"\\": b"\\",
    b"(": b"(",
    b")": b")",
}


class StreamWrapper(BufferedReader):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.data = self.read()
        self.seek(0)
        self.last_was_line_terminator = True
        self.position_of_next_newline: int

    def peek_line(self) -> bytes:
        """Reads a line without advancing the position.
        Returns the line including the line terminator."""
        current_position = self.tell()  # Get the current position
        nlmatch = newline_regex.search(self.data, self.tell())
        if not nlmatch:
            self.position_of_next_newline = len(self.data)
        else:
            self.position_of_next_newline = nlmatch.end()

        return self.data[current_position : self.position_of_next_newline]

    def go_to_next_line(self):
        """Always use after peek_line"""
        self.seek(self.position_of_next_newline)

    def read_line(self) -> bytes:
        if self.tell() == len(self.data):
            raise ValueError("EOF")
        res = self.peek_line()
        self.go_to_next_line()
        return res

    def peek(self, size: int = 0) -> bytes:
        """Peek, but exactly size (or less if EOF)"""
        res = self.read(size)
        self.seek(self.tell() - len(res))
        return res

    def yield_tokens(
        self, stop_on_newline=False, line_start=b"%_"
    ) -> Generator[bytes, None, None]:
        """This is an iterator to allow the calling method to decide when to stop"""
        yielded_one = False
        while True:
            current = self.tell()

            # Unclear: is : a token separator?
            # in favor: %%PageOrigin:-663 -120,
            # %AI17_Begin_Content_if_version_gt:24 4 (major minor)
            # against: unescaped in %%CreationDate: 3/13/2023 7:57 PM
            next_wsp = wsp_regex.search(self.data, current)

            if not next_wsp:
                if current == len(self.data):
                    return
                self.seek(len(self.data))
                yield self.data[current:]
                return
            nextword = next_wsp.end()
            word = self.data[current : next_wsp.start()]

            if newline_regex.search(next_wsp.group(0)):
                self.last_was_line_terminator = True
                if (
                    self.data[next_wsp.end() : next_wsp.end() + len(line_start)]
                    == line_start
                ):
                    nextword += len(line_start)

            else:
                self.last_was_line_terminator = False

            if not word.isspace() and word:
                if word[0] == 40:
                    self.last_was_string = True
                    self.seek(current + 1)
                    yield self.read_string()
                else:
                    self.last_was_string = False
                    self.seek(nextword)
                    yield word
                yielded_one = True
            else:
                self.seek(nextword)

            if stop_on_newline and self.last_was_line_terminator and yielded_one:
                return

    def peek_token(self):
        pos = self.tell()
        res = next(self.yield_tokens())
        self.seek(pos)
        return res

    def read_token(self):
        try:
            return next(self.yield_tokens())
        except StopIteration:
            raise ValueError("EOF reached, parsing incomplete")

    def skip(self, token: bytes):
        if token != (found := self.read_token()):
            raise ValueError(
                f"Expected token {dec(token)} not found at position {self.tell() - len(token)}, found {dec(found)}"
            )

    def skip_optional(self, token: bytes) -> bool:
        pos = self.tell()
        if token != next(self.yield_tokens()):
            self.seek(pos)
            return False
        return True

    @staticmethod
    def encode_3digit(data: bytearray):
        # TODO: Until 0xff, this seems to be latin-1
        # encoded. But values above that are not defined in
        # latin-1. Might be custom?
        try:
            return chr(int(bytes(data), 8)).encode("latin-1")
        except:
            return chr(int(bytes(data), 8)).encode("utf-8")

    def read_string(self):
        """Read a single string. The opening ( has already been consumed."""
        result = bytearray()
        while True:
            char = self.read(1)
            if not char:
                warnings.warn("EOF while parsing string")
                return bytes(result)
            if char == b"(":  # balanced parentheses
                result.extend(b"(")
                result.extend(self.read_string())
                result.extend(b")")
            elif char == b"\\":
                c2 = self.read(1)
                if c2 in string_replacement:
                    result.extend(string_replacement[c2])
                elif c2 in [b"\r", b"\n"]:
                    # The Scanner ignores both the backslash and the newline
                    if c2 == b"\r" and self.peek(1) == b"\n":
                        self.read(1)
                    pass
                elif c2.isdigit() and int(c2) < 9:
                    current = bytearray(c2)
                    while (a := self.peek(1)).isdigit() and int(a) < 8:
                        current.extend(self.read(1))
                        if len(current) == 3:
                            result.extend(self.encode_3digit(current))
                            current = bytearray()
                    if len(current) != 0:
                        result.extend(self.encode_3digit(current))

                else:
                    # We ignore the backslash, but append the character
                    result.extend(c2)
            elif char == b")":
                return bytes(result)
            else:
                result.extend(char)
