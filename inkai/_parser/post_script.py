# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse text in Post Script syntax."""

import re
from typing import List

ESCAPE_REGEX = re.compile("\\\\[^0-9]|\\\\[0-9][0-9][0-9]")
MAPPING = {"t": "\t", "r": "\r", "n": "\n", "t": "\t", "\n": "", "\r": "", "\r\n": ""}
POST_SCRIPT_STRING_REGEX = re.compile("\\(((?:[^\\\\]|\\\\.)*?)\\)")


def unescape(text: str) -> str:
    """Unescape the inside of a PostScript string.

    The escape mechanism is mentioned on page 36 of the PostScript specification.
    """
    if text.startswith("(") and text.endswith(")"):
        text = text[1:-1]

    def unescape(match):
        """Unescape an escaped character."""
        s = match[0]
        assert s[0] == "\\"
        if s[1] in "0123456789":
            return chr(int(s[1:], 8))
        else:
            return MAPPING.get(s[1:], s[1:])

    return ESCAPE_REGEX.sub(unescape, text)


def find_all(text: str) -> List[str]:
    """Return all postscript strings inside of the string.

    Example:

        '(post script)' -> 'post script'
    """
    return [unescape(s) for s in POST_SCRIPT_STRING_REGEX.findall(text)]


__all__ = ["unescape", "find_all"]
