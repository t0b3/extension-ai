# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Colors for stroke, fill and so on.

See AI Spec page 60+

Color types:
- CMYK
- RGB
- GRAY
- HSB - not in AI Spec
- WEBSAVE - not in AI Spec
"""
from .lazy import AIElement, EndOfLine, CommandParser, TokenParserMatchList
from . import parse
from typing import List, Optional


def is_fill_command(token: str):
    """Determine whether this is a fill or a stroke color."""
    return token[-1] == token[-1].lower()


class Color(AIElement):
    """The base class for all colors."""

    name_index = -1
    tint_index = -1

    def is_fill(self) -> bool:
        """Whether this is fill color."""
        return self.tokens[-1]

    def is_stroke(self) -> bool:
        """Whether this is stroke color."""
        return not self.is_fill()

    @property
    def name(self) -> str:
        """The name of the color.

        If the color does not have a name, this is an empty string.
        """
        return "" if self.name_index == -1 else self.tokens[self.name_index]

    @property
    def alpha(self) -> float:
        """The alpha value for transparency.\
        
        0 is transparent
        1 is the full color
        """
        return 1 if self.tint_index == -1 else 1 - self.tokens[self.tint_index]

    @property
    def rgb(self) -> List[float]:
        """The RGB value of the color or []."""
        return []

    @property
    def rgb_string(self) -> str:
        """Returns #RRGGBB or ''.

        This converts RGB, gray and CMYK colors to an RGB string without transparency (alpha).
        """
        return color_to_rgb_string(self)

    @property
    def gray(self) -> List[float]:
        """The gray value of the color or []."""
        return []

    @property
    def cmyk(self) -> List[float]:
        """The CMYK value of the color or []."""
        return []


class Gray(Color):
    """gray g

    AI Spec page 60
    """

    spec = EndOfLine("g", "G")
    token_parsers = [parse.float, is_fill_command]

    @property
    def gray(self) -> List[float]:
        """The gray value of the color. [gray]"""
        return [self.tokens[0]]


class CMYK(Color):
    """cyan magenta yellow black k

    AI Spec page 60
    """

    spec = EndOfLine("k", "K")
    token_parsers = [
        parse.float,
        parse.float,
        parse.float,
        parse.float,
        is_fill_command,
    ]

    @property
    def cmyk(self) -> List[float]:
        """The cmyk value of the color. [cyan, magenta, yellow, black]"""
        return self.tokens[:4]


class CustomCMYK(Color):
    """cyan magenta yellow black (name) gray x

    AI Spec page 61
    """

    spec = EndOfLine("x", "X", "Xs", "XS")
    token_parsers = [
        parse.float,
        parse.float,
        parse.float,
        parse.float,
        parse.ps,
        parse.float,
        is_fill_command,
    ]
    name_index = 4
    tint_index = 5

    @property
    def cmyk(self) -> List[float]:
        """The cmyk value of the color. [cyan, magenta, yellow, black]"""
        return self.tokens[:4]


class RGB(Color):
    """red green blue Xa | cyan magenta yellow black red green blue Xa

    AI Spec page 61
    """

    spec = EndOfLine("Xa", "XA")
    token_parsers = [
        [parse.float] * 3 + [is_fill_command],
        [parse.float] * 7 + [is_fill_command],
    ]

    @property
    def rgb(self) -> List[float]:
        """The RGB value of the color. [red, green, blue]"""
        return self.tokens[-4:-1]

    @property
    def cmyk(self) -> List[float]:
        """The cmyk value of the color or []. [cyan, magenta, yellow, black]"""
        return self.tokens[:4] if len(self.tokens) == 8 else []


class GenericCustomColor(Color):
    """comp1 ... compn name tint type Xx

    AI Spec page 61
    """

    spec = EndOfLine("Xx", "XX")
    token_parsers: TokenParserMatchList = [
        [parse.float] * 4 + [parse.ps, parse.float, "0", is_fill_command],  # CMYK
        [parse.float] * 3 + [parse.ps, parse.float, "1", is_fill_command],  # RGB
    ]
    name_index = -4
    tint_index = -3

    @property
    def rgb(self) -> List[float]:
        """The RGB value of the color or []. [red, green, blue]"""
        return self.tokens[:3] if self.tokens[-2] == "1" else []

    @property
    def cmyk(self) -> List[float]:
        """The cmyk value of the color or []. [cyan, magenta, yellow, black]"""
        return self.tokens[:4] if self.tokens[-2] == "0" else []


class Xz(Color):
    """0.9 0.7 0.6 0.9 0 0 0 ([Name]) 0 1 Xz

    See docs/specification_amendments/colors_and_palettes.md
    """

    spec = EndOfLine("Xz", "XZ")
    token_parsers: TokenParserMatchList = [
        [parse.float] * 7
        + [
            parse.ps,
            parse.float,
            "1",
            is_fill_command,
        ]
    ]
    name_index = 7
    tint_index = 8

    @property
    def cmyk(self) -> List[float]:
        """The cmyk value of the color or []. [cyan, magenta, yellow, black]"""
        return self.tokens[:4]


def color_to_rgb_string(color: Color) -> str:
    """Convert a color to an #RRGGBB string or "".

    This converts RGB, gray and CMYK colors to an RGB string without transparency (alpha).
    """
    rgb = color.rgb
    if not rgb and color.cmyk:
        C, M, Y, K = color.cmyk
        rgb = [(1 - C) * (1 - K), (1 - M) * (1 - K), (1 - Y) * (1 - K)]
    if not rgb and color.gray:
        rgb = [1 - color.gray[0]] * 3
    if not rgb:
        return ""
    f2s = lambda i: f"{int(round(255*rgb[i])):02X}"
    return f"#{f2s(0)}{f2s(1)}{f2s(2)}"


class Pn(Color):
    """This is the palette color 'none'.

    See AI Spec page 79.
    """

    spec = EndOfLine("Pn")

    @property
    def rgb(self) -> List[float]:
        """black"""
        return [0, 0, 0]

    @property
    def gray(self) -> List[float]:
        """black"""
        return [0]

    @property
    def cmyk(self) -> List[float]:
        """black"""
        return [0, 0, 0, 1]

    def is_fill(self) -> bool:
        """This is a fill color."""
        return True

    def is_stroke(okeself) -> bool:
        """This is a stroke color."""
        return True

    @property
    def alpha(self) -> float:
        """This is 100% transparent."""
        return 0

    @property
    def name(self) -> str:
        """The color is 'none'."""
        return "none"


Color.spec = CommandParser.from_specs(
    Gray, CMYK, CustomCMYK, RGB, GenericCustomColor, Xz, Pn
)
__all__ = [
    "Color",
    "RGB",
    "CMYK",
    "CustomCMYK",
    "GenericCustomColor",
    "Xz",
    "color_to_rgb_string",
]
