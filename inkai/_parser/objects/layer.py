# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""The layer.

See page 71 AI Spec.
"""
from ..lazy import StartOfLineSection, CollectedSection, generate_eol
from .. import parse
from .new import ArtDictionary
from .object_section import ObjectSection

Ln = generate_eol("name Ln", "See page 71 AI Spec.", [parse.ps])
Lb = generate_eol(
    "visible preview enabled printing dimmed has_multi_layer_masks color_index red green blue Lb",
    "See page 71 AI Spec.",
    [
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        int,
        int,
        int,
        int,
    ],
)


class Layer(ObjectSection):
    """A layer.

    Example:

        %AI5_BeginLayer
        ...
        %AI5_EndLayer--
    """

    spec = StartOfLineSection("%AI5_BeginLayer", "%AI5_EndLayer--")

    @property
    def name(self) -> str:
        """The name of the layer."""
        return self.first(Ln).name  # type: ignore

    @property
    def id(self) -> str:
        """The id of the layer."""
        return self.first(ArtDictionary)["AI10_ArtUID"]  # type: ignore


__all__ = ["Layer", "Lb", "Ln"]
