# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Gradient instances.

This is the usage of gradients.
See AI Spec page 44+
"""

from inkai.parser.lazy import CommandParser
from inkai.parser.tokens import TokenList
from ..util import TypeAlias
from typing import Type
from ..lazy import (
    generate_eol,
    CollectedSection,
    StartOfLineSection,
    EndOfLine,
    AIElement,
)
from .. import parse
from .interfaces import EndOfPath
from .path import PathRender


class BB(generate_eol("flag BB", "", [parse.int], base=EndOfPath)):  # type: ignore
    """Gradient Close

    Example:

        1 BB

    See AI Spec page 45.
    """

    @property
    def fill(self) -> bool:
        """False, do not fill the path."""
        return False

    @property
    def close(self) -> bool:
        """Close the path?"""
        return self.flag == 2

    @property
    def stroke(self) -> bool:
        """Stroke the path?"""
        print("BB", self.flag)
        return self.flag in (1, 2)


class Bg(
    generate_eol(  # type: ignore
        "flag name xOrigin yOrigin angle length a b c d tx ty Bg",
        "",
        [parse.int, parse.ps] + [parse.float] * 10,
    )
):
    """Gradient Geometry, see AI Spec page 45."""

    @property
    def token_strings(self) -> TokenList:
        """Add the optional flag."""
        token_string = super().token_strings
        if token_string[0].startswith("("):
            token_string.insert(0, "0")
        return token_string


Xm: Type[AIElement] = generate_eol(
    "a b c d x y Xm", "See AI Spec page 47.", [parse.float] * 6
)


class Gradient(CollectedSection):
    """This is a gradient instance.

    See AI Spec page 45.
    """

    spec = StartOfLineSection(
        "Bb", EndOfLine("BB").until, include_last_tested_line=True
    )

    @classmethod
    def add_to_inner(self, parser: CommandParser) -> None:
        parser.add_spec(BB, Bg, Xm, PathRender, Bm, Bc, Bh)


Bm: Type[AIElement] = generate_eol(
    "a b c d tx ty Bm", "See AI Spec page 49.", [parse.float] * 6
)
Bc: Type[AIElement] = generate_eol(
    "a b c d tx ty Bc", "See AI Spec page 49.", [parse.float] * 6
)
Bh: Type[AIElement] = generate_eol(
    "xHilight yHilight angle length Bh",
    "Gradient Hilights\nSee AI Spec page 49.",
    [parse.float] * 4,
)


__all__ = ["Gradient", "BB", "Bg", "Xm", "Bm", "Bc", "Bh"]
