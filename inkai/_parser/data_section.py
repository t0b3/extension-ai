# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This contains the BeginData PostScript directive.

See PostScript Spec page 45.
"""
from inkai.parser.lines import Lines
from .lazy import CollectedSection, StartOfLineSection
from . import parse
from typing import List
import base64
import re

HEX_REPLACEMENT = re.compile("[^0-9A-Fa-f]")


class Data(CollectedSection):
    """A section of binary data.

    Example:

        %%BeginData: <count> [type [bytes_or_lines]]
        ...
        %%EndData

    See PostScript Spec page 45.
    """

    spec = StartOfLineSection(
        "%%BeginData:", lambda line: line == "%%EndData" or line == "%EndData"
    )

    __lines: List[str] = []
    __content = ""
    __data = b""

    @property
    def _lines(self) -> List[str]:
        """The lines for processing the data."""
        if self.__lines == []:
            self.__lines = []
            while not self.lines.reached_end():
                self.__lines.append(
                    self.lines.read_line()
                )  # the binary data is in one line
                if self.lines.current_line_end:
                    self.__lines.append(self.lines.current_line_end)
        return self.__lines

    @property
    def content(self) -> str:
        """The raw unprocessed multi-line content of the section."""
        if not self.__content:
            self.__content = "".join(self._lines)
        return self.__content

    token_parsers = [parse.ignore, parse.ignore, parse.string, parse.string]

    @property
    def type(self) -> str:
        """The type of the content: Hex | Binary | ASCII"""
        return self.tokens[2] if len(self.tokens) > 2 else "Hex"

    def is_bytes(self) -> bool:
        """Whether the content is given in bytes (True) or lines (False)."""
        return self.tokens[3] == "Bytes" if len(self.tokens) > 3 else True

    def is_ASCII(self) -> bool:
        """Whether you can safely use self.data.decode("ASCII")."""
        return self.type == "ASCII"

    @property
    def data(self) -> bytes:
        """The data of the content."""
        if not self.__data:
            first_line = self._lines[0].split("\n", 1)
            # remove the purpose from the line data
            lines = (
                first_line[1:] + self._lines[1:]
                if first_line[0].strip().isalnum()
                else self._lines
            )
            print("TYPE", self.type)
            # return b"".join(line.encode("latin-1") for line in lines)
            if self.type in ("Binary", "ASCII"):
                self.__data = b"".join(line.encode("latin-1") for line in lines)
            else:
                # Hex
                self.__data = b"".join(
                    base64.b16decode(HEX_REPLACEMENT.sub("", line).upper())
                    for line in lines
                    if line
                )
        return self.__data

    @property
    def purpose(self) -> str:
        """Often the first line can contain a purpose of the data like "image" or "XI".

        This example has "XI" as purpose

            %%BeginData: ...
            XI
            <binary>
            %%EndData

        This example has "" as purpose

            %%BeginData: ...
            %<hex data>
            %%EndData


        """
        if not self._lines:
            return ""
        first_line = self._lines[0].split("\n", 1)
        return first_line[0].strip() if first_line[0].strip().isalnum() else ""


__all__ = ["Data"]
