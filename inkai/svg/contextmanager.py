# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Optional
import warnings
import inkex
from inkai.parser.operators import (
    FillRuleOperator,
    Operator,
    PathOperator,
    LayerNameOperator,
    LayerPropertiesOperator,
    PathRenderOperator,
    WindingOrderOperator,
)
from inkai.parser.consts import Section, AIDocument
from inkai.svg.live_shape.lscontext import LiveShapeContextManager
from .coordinates import SVGTransform
from .icontextmanager import IContextManager, on


class DocumentContextManager(IContextManager):
    def __init__(self, file: AIDocument):
        self.root = self
        super().__init__()
        self.transform = SVGTransform(file)

        self.root_element = self.setup_document(file)

        self.file = file

        self.current_container: inkex.BaseElement = self.root_element

        self.pathcm = PathContextManager(self)
        self.stylecm = StyleContextManager(self)
        self.lscm = LiveShapeContextManager(self)

    def setup_document(self, file: AIDocument) -> inkex.SvgDocumentElement:
        # Setup the page
        page = file.drawing_first_page_position
        width = self.transform.from_canvas_length(page.width)
        height = self.transform.from_canvas_length(page.height)

        result = inkex.base.SvgOutputMixin.get_template(
            width=width, height=height, unit=file.units
        ).getroot()

        # Add the remaining pages
        dd = file._document_data
        if not dd:
            return result
        artboards = dd["Recorded"].get("ArtboardArray", None)
        if not artboards or len(artboards) == 1:
            return result

        for artboard in artboards[1:]:
            x1, y1 = self.transform.from_drawing(*artboard["PositionPoint1"])
            x2, y2 = self.transform.from_drawing(*artboard["PositionPoint2"])

            result.namedview.new_page(
                *map(str, [x1, y1, x2 - x1, y2 - y1]), label=artboard["Name"]
            )

        # Also add the name of the first page
        result.namedview.get_pages()[0].set("inkscape:label", artboards[0]["Name"])

        return result

    # Layer operators

    @on("%AI5_BeginLayer")
    def start_layer(self, layer: Section):
        l = inkex.Layer()
        self.current_container.append(l)
        self.current_container = l
        for child in layer.children:
            self.interpret(child)
        self.current_container = l.getparent()

    @on("Lb")
    def layer_properties(self, op: LayerPropertiesOperator):  # type: ignore
        layer: inkex.Layer = self.current_container
        if not op.visible:  # type: ignore
            layer.style["display"] = "none"
        if not op.enabled:  # type: ignore
            layer.set("sodipodi:insensitive", "true")

    @on("Ln")
    def layer_name(self, op: LayerNameOperator):  # type: ignore
        layer: inkex.Layer = self.current_container
        layer.set("inkscape:label", op.name)  # type: ignore

    # Group operators

    @on("u")
    def start_group(self, _: Operator):
        g = inkex.Group()
        self.current_container.append(g)
        self.current_container = g

    @on("U")
    def end_group(self, _: Operator):
        self.current_container = self.current_container.getparent()

    # Stuff that gets distributed based on content
    @on(dict)
    def entry_art_dictionary(self, element: dict):
        if element["_type"] == "ArtDictionary":
            ls: Optional[dict] = element.get("ai::LiveShape", None)
            if ls is not None:
                self.lscm.convert_live_shape(ls)


class StyleContextManager(IContextManager):
    def __init__(self, root: DocumentContextManager):
        self.func_mapping = root.func_mapping
        self.root: DocumentContextManager = root

    def apply_style(self):
        # Applies the current style to the last created element.
        # The element is possibly duplicated during this.
        pass


class PathContextManager(IContextManager):
    root: DocumentContextManager

    def __init__(self, root: DocumentContextManager):
        self.func_mapping = root.func_mapping
        super().__init__(root)
        # State variables
        self.geometry = inkex.Path()
        self.stroke: bool = False
        self.fill: bool = False
        self.clockwise_winding_order: bool = False
        self.in_compound_path: bool = False
        self.last_point = [0, 0]
        self.fill_rule: str = "nonzero"

    @on(*list("cCvVyY"))
    def curve_like(self, op: PathOperator):
        pts = self.root.transform.from_drawing(*op.to_cubic(self.last_point))
        self.last_point = [op.x3, op.y3]  # type: ignore
        self.geometry.append(inkex.paths.Curve(*pts))
        # TODO node type

    @on("l", "L")
    def line_like(self, op: PathOperator):
        pts = self.root.transform.from_drawing(op.x, op.y)  # type: ignore
        self.last_point = [op.x, op.y]  # type: ignore
        self.geometry.append(inkex.paths.Line(*pts))
        # TODO node type

    @on("m")
    def move(self, op: PathOperator):
        pts = self.root.transform.from_drawing(op.x, op.y)  # type: ignore
        self.last_point = [op.x, op.y]  # type: ignore
        self.geometry.append(inkex.paths.Move(*pts))

    @on("D")
    def winding_order(self, op: WindingOrderOperator):  # type: ignore
        self.clockwise_winding_order = op.clockwise  # type: ignore

    @on("*u", "*U")
    def compound_path(self, op: Operator):
        """Tell the context manager to collect the following paths"""
        self.in_compound_path = op.command == "*u"
        if op.command == "*U":
            # Attach the compound path

            self.attach_path()

    @on("XR")
    def fill_rule(self, op: FillRuleOperator):  # type: ignore
        if self.in_compound_path and len(self.geometry) > 0:
            warnings.warn(
                "Changing the fill rule mid-compound path is not supported in SVG"
            )
        self.fill_rule = "evenodd" if op.rule else "nonzero"  # type: ignore

    @on(*list("NnFfsSBb"))
    def paint_path(self, op: PathRenderOperator):
        if op.close:
            self.geometry.append(inkex.paths.ZoneClose())
        if not self.clockwise_winding_order:
            self.geometry.reverse()
        if self.in_compound_path and len(self.geometry) > 0:
            if self.fill != op.fill or self.stroke != op.stroke:
                warnings.warn(
                    "Changing the fill/stroke properties mid-compound path is not supported in SVG"
                )
        self.fill = op.fill
        self.stroke = op.stroke
        if not self.in_compound_path:
            self.attach_path()

    # TODO process guides here

    def attach_path(self):
        pel = inkex.PathElement(d=str(self.geometry))
        self.root.current_container.append(pel)

        pel.style["fill-rule"] = self.fill_rule
        self.root.stylecm.apply_style()

        self.geometry = inkex.Path()
