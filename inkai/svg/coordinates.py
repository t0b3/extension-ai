# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Transform coordinates from one system to another."""

from __future__ import annotations
from typing import Sequence

from inkai.parser.units import to_px
from ..parser.consts import AIDocument

Coordinates = Sequence[float]


class SVGTransform:
    """Transform the coordinate systems to the one used in the SVG file."""

    def __init__(self, info: AIDocument):
        """Create an SVGTransform."""
        self._large_canvas_scale = info.header.large_canvas_scale
        page = info.drawing_first_page_position
        self._ruler_x = page.left
        self._ruler_y = page.top
        cro = info.canvas_ruler_origin
        if cro is None:
            self._has_canvas = False
            self._canvas_ruler_x = self._canvas_ruler_y = 0
        else:
            self._has_canvas = True
            self._canvas_ruler_x = cro.x
            self._canvas_ruler_y = cro.y
        self.unit = info.units

    def from_drawing(self, *xy: float) -> Coordinates:
        """Drawing coordinates -> SVG coordinates"""
        return [
            (f * (-1 if i % 2 else 1) - (self._ruler_y if i % 2 else self._ruler_x))
            * self._canvas_scale
            for i, f in enumerate(xy)
        ]

    @property
    def _canvas_scale(self) -> float:
        """The scaling of e.g. width and height from canvas coordinates to SVG coordinates."""
        return self._large_canvas_scale / to_px(self.unit)

    def from_canvas(self, *xy: float) -> Coordinates:
        """Canvas coordinates -> SVG coordinates. SVG places the first page at 0,0"""
        if not self._has_canvas:
            raise ValueError("This file does not have a canvas_ruler_origin.")
        return [
            (
                f
                - (
                    self._canvas_ruler_y + self._ruler_y
                    if i % 2
                    else self._canvas_ruler_x + self._ruler_x
                )
            )
            * self._canvas_scale
            for i, f in enumerate(xy)
        ]

    def from_canvas_length(self, length: float) -> float:
        return length * self._canvas_scale

    def from_drawing_length(self, length: float) -> float:
        return length / to_px(self.unit)


__all__ = ["SVGTransform"]
