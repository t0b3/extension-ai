# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from __future__ import annotations
from collections import namedtuple
from dataclasses import dataclass
import dataclasses
from typing import Any


from ...parser.util import Point


class Corner:
    r"""The corner of a rectangle.

    Imagine a rectangle with corners.
    The corners are round:

    right  +-----
          /
         /
        + left
        |            x center of rectangle

    y↑up  x→right


    The position of these corner points left and right are
    relative to the center of the rectangle,
    also they are NOT rotated around the center of the rectangle
    by the Angle of the rectangle.
    """

    def __init__(self, rectangle: Rectangle, index):
        """Create a new corner."""
        self.rectangle = rectangle
        self.index = index

    @property
    def RoundingType(self) -> str:
        """The rounding type of the corner."""
        return self.rectangle.params[f"ai::Rectangle::RoundingType::{self.index}"]

    @property
    def CornerType(self) -> str:
        """The corner type of the corner."""
        return self.rectangle.params[f"ai::Rectangle::CornerType::{self.index}"]

    @property
    def CornerRadius(self) -> float:
        """The radius of the corner."""
        return round(
            self.rectangle.params[f"ai::Rectangle::CornerRadius::{self.index}"], 8
        )

    def __eq__(self, other: Any):
        """a == b"""
        if not isinstance(other, Corner):
            return False
        return (
            self.CornerType == other.CornerType
            and abs(self.CornerRadius - other.CornerRadius) < 0.000001
            and self.RoundingType == other.RoundingType
        )

    def __hash__(self) -> int:
        """hash(self)"""
        return hash(self.RoundingType) ^ hash(self.CornerRadius) ^ hash(self.CornerType)

    @property
    def left(self) -> Point:
        """The left corner position if you look from the middle."""
        return self._corner_point(0)

    @property
    def right(self) -> Point:
        """The right corner position if you look from the middle."""
        return self._corner_point(1)

    def _corner_point(self, i: int):
        """Return the corner point."""
        x = self.rectangle.width / 2
        y = self.rectangle.height / 2
        if (self.index, i) in ((2, 0), (3, 1), (0, 0), (1, 1)):
            y -= self.CornerRadius
        else:
            x -= self.CornerRadius
        if self.index in (2, 1):
            x = -x
        if self.index in (0, 1):
            y = -y
        return Point(x, y)


@dataclass
class Corners:
    top_left: Corner
    top_right: Corner
    bottom_right: Corner
    bottom_left: Corner

    def __iter__(self):
        return (getattr(self, field.name) for field in dataclasses.fields(self))


class BaseLiveShape:
    """Base data structure wiuth some common attributes"""

    prefix = ""

    def __init__(self, artdict: dict):
        self.params = artdict

    @property
    def center(self) -> Point:
        """The center."""
        return Point(
            self.params[f"{self.prefix}::CenterX"],
            self.params[f"{self.prefix}::CenterY"],
        )

    @property
    def angle(self) -> float:
        """The angle in radians."""
        return self.params[f"{self.prefix}::RotationAngle"]

    @property
    def clockwise(self) -> bool:
        return self.params[f"{self.prefix}::Clockwise"]


class Rectangle(BaseLiveShape):
    prefix = "ai::Rectangle"

    @property
    def angle(self) -> float:
        """The angle in radial measure (not degrees!)."""
        return self.params["ai::Rectangle::Angle"]

    @property
    def width(self) -> float:
        return self.params["ai::Rectangle::Width"]

    @property
    def height(self) -> float:
        return self.params["ai::Rectangle::Height"]

    @property
    def corners(self) -> Corners:
        """The four corners of the rectangle.

        => [top-left, top-right, bottom-right, bottom-left]

            0 --- 1
            |     |
            3 --- 2
        """
        return Corners(*(Corner(self, (i + 2) % 4) for i in range(4)))

    def has_equal_corners(self) -> bool:
        """Whether all corners are equal."""
        corners = self.corners
        return (
            corners.top_left
            == corners.top_right
            == corners.bottom_right
            == corners.bottom_left
        )

    def has_relative_corner(self) -> bool:
        """Check if any corner is "relative", we can't represent that
        using live shapes -> bail out"""
        return any(i.RoundingType == "Relative" for i in self.corners)


class Ellipse(BaseLiveShape):
    prefix = "ai::Ellipse"

    @property
    def width(self) -> float:
        return self.params["ai::Ellipse::Width"]

    @property
    def height(self) -> float:
        return self.params["ai::Ellipse::Height"]

    @property
    def pieStartAngle(self) -> float:
        """In radians"""
        return self.params["ai::Ellipse::PieStartAngle"]

    @property
    def pieEndAngle(self) -> float:
        """In radians"""
        return self.params["ai::Ellipse::PieEndAngle"]


class Polygon(BaseLiveShape):
    prefix = "ai::Polygon"

    @property
    def numSides(self) -> int:
        return self.params["ai::Polygon::NumSides"]

    @property
    def radius(self) -> float:
        return self.params["ai::Polygon::Radius"]

    @property
    def roundingType(self) -> int:
        """0: Sharp corners, 1: Any other corner type"""
        return self.params["ai::Polygon::CornerRoundingType"]

    @property
    def cornerRadius(self) -> float:
        return self.params["ai::Polygon::CornerRadius"]

    @property
    def cornerType(self) -> int:
        """0: Sharp corners, 1: Rounded, 2: Inverted, 3: Chamfer"""
        return self.params["ai::Polygon::CornerType"]

    @property
    def isRegular(self) -> bool:
        return self.params["ai::Polygon::IsRegular"]


class Line(BaseLiveShape):
    """Not used intentionally (see live_shapes.md)"""

    prefix = "ai::Line"
