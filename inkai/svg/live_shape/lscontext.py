# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from __future__ import annotations

import math
from typing import (
    TYPE_CHECKING,
    Iterable,
    Optional,
    Type,
    Union,
    Dict,
    Tuple,
    Callable,
    Any,
)
import warnings

import inkex

from inkai.svg.icontextmanager import IContextManager
from inkai.svg.live_shape.types import BaseLiveShape, Line, Polygon, Rectangle, Ellipse

if TYPE_CHECKING:
    from ..contextmanager import DocumentContextManager


class LiveShapeContextManager(IContextManager):
    """Create SVG paths and shapes."""

    root: DocumentContextManager

    def __init__(self, root: DocumentContextManager):
        """Create paths from AIElements."""
        super().__init__(root)

    def convert_live_shape(self, lsdict: dict[str, Any]):
        """Type-augment the dictionary, and pass it to the correct converter"""
        handler: str = lsdict["ai::LiveShape::HandlerName"]
        params = lsdict.get("ai::LiveShape::Params")
        mapping: Dict[str, Tuple[Type, Callable]] = {
            "ai::Rectangle": (Rectangle, self.create_rectangle),
            "ai::Ellipse": (Ellipse, self.create_ellipse),
            "ai::Polygon": (Polygon, self.create_polygon),
            "ai::Line": (Line, lambda _: None),
        }
        if handler in mapping:
            type, func = mapping[handler]
            result = func(type(params))
            if result is not None:
                self.replace_last_child(result)

        else:
            warnings.warn(f"Unknown Liveshape type {handler}")

    def replace_last_child(self, new_el: inkex.BaseElement):
        if len(self.root.current_container) > 1 and isinstance(
            self.root.current_container[-1], inkex.ShapeElement
        ):
            self.root.current_container.replace(self.root.current_container[-1], new_el)
        else:
            self.root.current_container.append(new_el)

    # RECTANGLE CONVERSION
    def create_rectangle(self, rect: Rectangle) -> Optional[inkex.ShapeElement]:
        """Convert this path into a rectangle."""
        # If the rectangle has Cornertype "relative", bail out.
        if rect.has_relative_corner():
            return None

        if rect.has_equal_corners():
            return self.create_regular_rectangle(rect)
        else:
            svg_rect = self.create_irregular_rectangle(rect)
            path_effect = self.create_irregular_rectangle_path_effect(rect)
            self.root.root_element.defs.append(path_effect)
            svg_rect.set("inkscape:path-effect", "#" + path_effect.get_id())
            return svg_rect

    def _set_rect_attributes(self, element: inkex.BaseElement, rect: Rectangle):
        """Set the attributes all results should have."""
        top_left = self.root.transform.from_canvas(
            rect.center.x - rect.width / 2, rect.center.y - rect.height / 2
        )
        element.set("x", top_left[0])
        element.set("y", top_left[1])
        element.set("width", self.root.transform.from_canvas_length(rect.width))
        element.set("height", self.root.transform.from_canvas_length(rect.height))
        if rect.angle != 0:
            center = self.root.transform.from_canvas(*rect.center)
            element.transform = (
                f"rotate({-math.degrees(rect.angle)} {center[0]} {center[1]})"
            )

    def create_regular_rectangle(self, rect: Rectangle) -> inkex.Rectangle:
        """Create a rectangle with all corners the same."""
        result = inkex.Rectangle()
        self._set_rect_attributes(result, rect)
        if rect.corners.top_left.CornerRadius != 0:
            result.set(
                "ry",
                self.root.transform.from_canvas_length(
                    rect.corners.top_left.CornerRadius
                ),
            )
        return result

    def create_irregular_rectangle(self, rect: Rectangle) -> inkex.PathElement:
        r"""Create a rectangle shape where the corners differ."""
        result = inkex.PathElement()
        self._set_rect_attributes(result, rect)
        result.set("sodipodi:type", "rect")
        return result

    def create_irregular_rectangle_path_effect(
        self, rect: Rectangle
    ) -> inkex.PathEffect:
        """Create the path effect for the irregular rectangle.

        This path effect is used to edit the corners in Inkscape.
        """
        radius_and_type = [
            (
                self.root.transform.from_canvas_length(corner.CornerRadius),
                math.pi / 2,
                {"Chamfer": "C", "Normal": "F", "Inverted": "IF"}.get(
                    corner.CornerType, "C"
                ),
            )
            for corner in rect.corners
        ]
        if not rect.clockwise:
            radius_and_type = radius_and_type[::-1]
        return self.create_corners_path_effect(radius_and_type)

    @staticmethod
    def create_corners_path_effect(
        radius_angle_type: Iterable[Tuple[float, float, str]]
    ):
        # The Corners LPE expects as sixth parameter the distance from the old node
        # to the point where the remaining straight line and edge shape (arc, line)
        # meet. AI gives the radius, so we have to convert this here.

        params = " @ ".join(
            f"{type},0,0,1,0,{radius * math.tan(angle/2):.6f},0,1"
            for radius, angle, type in radius_angle_type
        )

        return inkex.PathEffect.new(
            effect="fillet_chamfer",
            is_visible="true",
            lpeversion="1",
            satellites_param=params,  # Inkscape 1.2
            nodesatellites_param=params,  # Inkscape 1.3
            unit="px",
            method="auto",
            mode="F",
            radius="0",
            chamfer_steps="1",
            flexible="false",
            use_knot_distance="true",
            apply_no_radius="true",
            apply_with_radius="true",
            only_selected="false",
            hide_knots="false",
        )

    def create_ellipse(
        self, ellipse: Ellipse
    ) -> Union[inkex.Path, inkex.Circle, inkex.Ellipse]:
        center = self.root.transform.from_canvas(*ellipse.center)
        width = self.root.transform.from_canvas_length(ellipse.width)
        height = self.root.transform.from_canvas_length(ellipse.height)
        if ellipse.pieStartAngle == 0 and ellipse.pieEndAngle == 0:
            # Circle or Ellipse
            if math.isclose(width, height):
                result = inkex.Circle.new(center, width / 2)
            else:
                result = inkex.Ellipse.new(center, (width / 2, height / 2))
        else:
            # For Inkscape, the angles are specified in terms of the
            # standard ellipse parametrisation (a cos(t), b sin(t)). For AI,
            # the angles are specified as actual angles (relative to
            # ellipse.angle).
            angle_transform = lambda angle: -math.atan(
                ellipse.width / ellipse.height * math.tan(angle)
            )
            result = inkex.PathElement.arc(
                center,
                width / 2,
                height / 2,
                "slice",
                start=angle_transform(ellipse.pieEndAngle),
                end=angle_transform(ellipse.pieStartAngle),
            )

        if ellipse.angle != 0:
            result.transform = (
                f"rotate({-math.degrees(ellipse.angle)} {center[0]} {center[1]})"
            )
        return result

    def create_polygon(self, poly: Polygon) -> Optional[inkex.PathElement]:
        r"""Create a rectangle shape where the corners differ."""
        if not poly.isRegular:
            # No matching shape in Inkscape, keep path.
            return None

        result = inkex.PathElement.star(
            self.root.transform.from_canvas(*poly.center),
            radii=(self.root.transform.from_canvas_length(poly.radius), 0),
            # Angle of 0 = bottom side is flat,
            # but radius = length from center to top
            args=(-poly.angle + math.pi * (1 / 2 + 1 / poly.numSides), 0),
            sides=poly.numSides,
            rounded=0,
            flatsided=True,
        )
        if poly.cornerType != 0:
            path_effect = self.create_corners_path_effect(
                [
                    (
                        poly.cornerRadius,
                        math.pi * 2 / poly.numSides,
                        {3: "C", 1: "F", 2: "IF"}.get(poly.cornerType, "C"),
                    )
                    for _ in range(poly.numSides)
                ]
            )
            self.root.root_element.defs.append(path_effect)
            result.set("d", None)
            result.set("inkscape:path-effect", path_effect.get_id(1))

        return result
