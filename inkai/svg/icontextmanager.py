# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from __future__ import annotations
from typing import Dict, Union, Callable

from inkai.parser.hierarchy_objects import HierarchyObject
from inkai.parser.streamreader import dec
from ..parser.consts import Operator, Section, Optional


def on(*args):
    """Tag a function to receive elements."""

    def wrapper(function):
        """Tag a function for interpretation."""
        function.on = args
        return function

    return wrapper


class IContextManager:
    def __init__(self, root: Optional[IContextManager] = None):
        self.root = root or self
        self.func_mapping: Dict[Union[type, str], Callable] = {}
        # Register all handlers.
        handlers = [
            getattr(self, name)
            # get all attributes, including methods, properties, and builtins
            for name in dir(self)
            # but we only want methods
            if callable(getattr(self, name))
            # and we don't need builtins
            and not name.startswith("__")
            # and we only want the decorated methods
            and hasattr(getattr(self, name), "on")
        ]
        for handler in handlers:
            for arg in handler.on:
                if arg in self.func_mapping:
                    raise ValueError(f"Duplicate interpretation routes for {arg}")
                self.root.func_mapping[arg] = handler

    def interpret(self, o: Union[Section, Operator, HierarchyObject, dict, list]):
        self.execute(o)

    def execute(self, op: Union[Section, Operator, HierarchyObject, dict, list]):
        if type(op) in self.func_mapping:
            self.func_mapping[type(op)](op)
        elif isinstance(op, Operator) and op.command in self.func_mapping:
            self.func_mapping[op.command](op)
        elif isinstance(op, Section) and dec(op.start) in self.func_mapping:
            self.func_mapping[dec(op.start)](op)
